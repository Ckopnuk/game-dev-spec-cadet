#include <fstream>
#include <iostream>
#include <memory>

#include "src/engine.h"

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
        Ckom::create_engine(), Ckom::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    Ckom::Texture* textureBug = engine->create_texture("bug.png");
    if (nullptr == textureBug)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    Ckom::Vertex_buffer* vertex_buf_b = nullptr;
    std::ifstream        file("vert_tex_color.txt");
    if (!file)
    {
        std::cerr << "Can't load vert_tex_color.txt" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::array<Ckom::Triangle_pct, 2> triangles;
        file >> triangles[0] >> triangles[1];
        vertex_buf_b =
            engine->create_vertex_buffer(&triangles[0], triangles.size());
        if (vertex_buf_b == 0)
        {
            std::cerr << "can't create bug vertex bufer";
            return EXIT_FAILURE;
        }
    }
    file.close();

    Ckom::Triangle_p triangle;
    Ckom::Color      color = { 1, 1, 1, 1 };

    triangle.v[0].p = { -0.5, 0.5 };
    triangle.v[1].p = { 0.5, 0.5 };
    triangle.v[2].p = { 0.0, -0.5 };

    //    triangle.v[0].c = color;
    //    triangle.v[1].c = color;
    //    triangle.v[2].c = color;

    file.close();

    bool continue_loop = true;

    while (continue_loop)
    {
        Ckom::Event event;
        while (engine->read_event(event))
        {
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continue_loop = false;
                    break;
                default:
                    break;
            }
        }

        Ckom::mat2x3 scale_f = Ckom::mat2x3::scale(1, 1);

        engine->render(triangle, color);

        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}
