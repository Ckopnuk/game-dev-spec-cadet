#include "engine.h"

#include <algorithm>
#include <array>
#undef NDEBUG
#include <cassert>
#include <cmath>
#include <cstddef>
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <tuple>
#include <vector>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#include <SDL_syswm.h>

#include "picopng.h"

#define STB_IMAGE_IMPLEMENTATION
#ifdef __GNUG__
#pragma GCC diagnostic push
// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#endif
#include "imgui.h"
#include "stb_image.h"
#ifdef __GNUG__
#pragma GCC diagnostic pop
#endif

// bool ImGui_ImplSdlGL3_Init(SDL_Window* window);
// void ImGui_ImplSdlGL3_Shutdown();
// void ImGui_ImplSdlGL3_NewFrame(SDL_Window* window);
// bool ImGui_ImplSdlGL3_ProcessEvent(const SDL_Event* event);
// void ImGui_ImplSdlGL3_RenderDrawLists(ImDrawData* draw_data);

// void ImGui_ImplSdlGL3_InvalidateDeviceObjects();
// bool ImGui_ImplSdlGL3_CreateDeviceObjects();

static PFNGLCREATESHADERPROC             glCreateShader             = nullptr;
static PFNGLSHADERSOURCEPROC             glShaderSource             = nullptr;
static PFNGLCOMPILESHADERPROC            glCompileShader            = nullptr;
static PFNGLGETSHADERIVPROC              glGetShaderiv              = nullptr;
static PFNGLGETSHADERINFOLOGPROC         glGetShaderInfoLog         = nullptr;
static PFNGLDELETESHADERPROC             glDeleteShader             = nullptr;
static PFNGLCREATEPROGRAMPROC            glCreateProgram            = nullptr;
static PFNGLATTACHSHADERPROC             glAttachShader             = nullptr;
static PFNGLBINDATTRIBLOCATIONPROC       glBindAttribLocation       = nullptr;
static PFNGLLINKPROGRAMPROC              glLinkProgram              = nullptr;
static PFNGLGETPROGRAMIVPROC             glGetProgramiv             = nullptr;
static PFNGLGETPROGRAMINFOLOGPROC        glGetProgramInfoLog        = nullptr;
static PFNGLDELETEPROGRAMPROC            glDeleteProgram            = nullptr;
static PFNGLUSEPROGRAMPROC               glUseProgram               = nullptr;
static PFNGLVERTEXATTRIBPOINTERPROC      glVertexAttribPointer      = nullptr;
static PFNGLENABLEVERTEXATTRIBARRAYPROC  glEnableVertexAttribArray  = nullptr;
static PFNGLVALIDATEPROGRAMPROC          glValidateProgram          = nullptr;
static PFNGLGETUNIFORMLOCATIONPROC       glGetUniformLocation       = nullptr;
static PFNGLUNIFORM1FPROC                glUniform1f                = nullptr;
static PFNGLUNIFORM1IPROC                glUniform1i                = nullptr;
static PFNGLDELETEBUFFERSPROC            glDeleteBuffers            = nullptr;
static PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = nullptr;
static PFNGLACTIVETEXTUREPROC            glActiveTextureMY          = nullptr;
static PFNGLUNIFORM4FVPROC               glUniform4fv               = nullptr;
static PFNGLUNIFORMMATRIX3FVPROC         glUniformMatrix3fv         = nullptr;
static PFNGLBUFFERSUBDATAPROC            glBufferSubData            = nullptr;
static PFNGLACTIVETEXTUREPROC            glActiveTexture_           = nullptr;
static PFNGLGENERATEMIPMAPPROC           glGenerateMipmap           = nullptr;
static PFNGLUNIFORMMATRIX4FVPROC         glUniformMatrix4fv         = nullptr;
static PFNGLGENERATEMIPMAPPROC           glGenerateMipmaps          = nullptr;
// RENDERDOC
static PFNGLBINDBUFFERPROC      glBindBuffer      = nullptr;
static PFNGLGENBUFFERSPROC      glGenBuffers      = nullptr;
static PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
static PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
static PFNGLBUFFERDATAPROC      glBufferData      = nullptr;
// RENDERDOC

static PFNGLBLENDEQUATIONSEPARATEPROC glBlendEquationSeparate = nullptr;
static PFNGLBLENDFUNCSEPARATEPROC     glBlendFuncSeparate     = nullptr;
static PFNGLGETATTRIBLOCATIONPROC     glGetAttribLocation     = nullptr;
static PFNGLDETACHSHADERPROC          glDetachShader          = nullptr;

template <typename T>
static void load_gl_function(const char* funcName, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(funcName);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(std::string("can't load gl function") +
                                 funcName);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#define CKOM_GL_CHECK()                                                        \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

namespace Ckom
{

vec2::vec2()
    : x(0.f)
    , y(1.f)
{
}

vec2::vec2(float x_, float y_)
    : x(x_)
    , y(y_)
{
}

float vec2::length() const
{
    return std::sqrt(x * x + y * y);
}

vec2 operator+(const vec2& left, const vec2& right)
{
    vec2 result;
    result.x = left.x + right.x;
    result.y = left.y + right.y;
    return result;
}

vec2 operator*(const vec2& v, const float& fl)
{
    vec2 result;
    result.x = v.x * fl;
    result.y = v.x * fl;
    return result;
}
vec2 operator+(const vec2& v, const float& fl)
{
    vec2 result;
    result.x = v.x + fl;
    result.y = v.x + fl;
    return result;
}

mat2x3::mat2x3()
    : colon0(1.0f, 0.f)
    , colon1(0.f, 1.0f)
    , delta(0.f, 0.f)
{
}

mat2x3 mat2x3::indentiry()
{
    return mat2x3::scale(1.f);
}

mat2x3 mat2x3::scale(float scale)
{
    mat2x3 result;
    result.colon0.x = scale;
    result.colon1.y = scale;
    return result;
}

mat2x3 mat2x3::scale(float sx, float sy)
{
    mat2x3 result;
    result.colon0.x = sx;
    result.colon1.y = sy;
    return result;
}

mat2x3 mat2x3::rotation(float thetha)
{
    mat2x3 result;
    result.colon0.x = std::cos(thetha);
    result.colon0.y = std::sin(thetha);

    result.colon1.x = -std::sin(thetha);
    result.colon1.y = std::cos(thetha);

    return result;
}

mat2x3 mat2x3::moove(const vec2& delta)
{
    mat2x3 result  = mat2x3::indentiry();
    result.delta.x = delta.x;
    result.delta.y = delta.y;
    return result;
}

vec2 operator*(const vec2& vec, const mat2x3& matr)
{
    vec2 result;
    result.x = vec.x * matr.colon0.x + vec.y * matr.colon0.y + matr.delta.x;
    result.y = vec.x * matr.colon1.x + vec.y * matr.colon1.y + matr.delta.y;
    return result;
}

mat2x3 operator*(const mat2x3& mLeft, const mat2x3& mRight)
{
    mat2x3 r;
    r.colon0.x =
        mLeft.colon0.x * mRight.colon0.x + mLeft.colon1.x * mRight.colon0.y;
    r.colon1.x =
        mLeft.colon0.x * mRight.colon1.x + mLeft.colon1.x * mRight.colon1.y;
    r.colon0.y =
        mLeft.colon0.y * mRight.colon0.x + mLeft.colon1.y * mRight.colon0.y;
    r.colon1.y =
        mLeft.colon0.y * mRight.colon1.x + mLeft.colon1.y * mRight.colon1.y;

    r.delta.x = mLeft.delta.x * mRight.colon0.x +
                mLeft.delta.y * mRight.colon0.y + mRight.delta.x;
    r.delta.y = mLeft.delta.x * mRight.colon1.x +
                mLeft.delta.y * mRight.colon1.y + mRight.delta.y;

    return r;
}

class Vertex_buffer_impl final : public Vertex_buffer
{
public:
    Vertex_buffer_impl(const Triangle_pct* tri, std::size_t n)
        : count(static_cast<std::uint32_t>(n * 3))
    {
        glGenBuffers(1, &gl_handle);
        CKOM_GL_CHECK()

        bind();

        GLsizeiptr size_in_bytes = n * 3 * sizeof(Vertex_pct);

        glBufferData(GL_ARRAY_BUFFER, size_in_bytes, &tri->v[0],
                     GL_STATIC_DRAW);
        CKOM_GL_CHECK()
    }
    Vertex_buffer_impl(const Vertex_pct* vert, std::size_t n)
        : count(static_cast<std::uint32_t>(n))
    {
        glGenBuffers(1, &gl_handle);
        CKOM_GL_CHECK()

        bind();

        GLsizeiptr size_in_bytes =
            static_cast<GLsizeiptr>(n * sizeof(Vertex_pct));
        glBufferData(GL_ARRAY_BUFFER, size_in_bytes, vert, GL_STATIC_DRAW);
        CKOM_GL_CHECK()
    }

    ~Vertex_buffer_impl() final
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CKOM_GL_CHECK()
        glDeleteBuffers(1, &gl_handle);
        CKOM_GL_CHECK()
    }

    void bind() const override
    {
        glBindBuffer(GL_ARRAY_BUFFER, gl_handle);
        CKOM_GL_CHECK()
    }
    size_t size() const override final { return count; }

private:
    std::uint32_t count{ 0 };
    std::uint32_t gl_handle{ 0 };
};

class Index_buffer_impl final : public Index_buffer
{
public:
    Index_buffer_impl(const std::uint16_t* i, size_t n)
        : count(static_cast<std::uint32_t>(n))
    {
        glGenBuffers(1, &gl_handle);
        CKOM_GL_CHECK()

        bind();

        GLsizeiptr size_in_bytes =
            static_cast<GLsizeiptr>(n * sizeof(std::uint16_t));

        glBufferData(GL_ELEMENT_ARRAY_BUFFER, size_in_bytes, i, GL_STATIC_DRAW);
        CKOM_GL_CHECK()
    }
    ~Index_buffer_impl() override
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CKOM_GL_CHECK()
        glDeleteBuffers(1, &gl_handle);
        CKOM_GL_CHECK()
    }

    void bind() const override
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl_handle);
        CKOM_GL_CHECK()
    }
    std::uint32_t size() const override { return count; }

private:
    std::uint32_t gl_handle{ 0 };
    std::uint32_t count{ 0 };
};

static std::string_view get_spund_format_name(uint16_t format_value)
{
    static const std::map<int, std::string_view> format = {
        { AUDIO_U8, "AUDIO_U8" },         { AUDIO_S8, "AUDIO_S8" },
        { AUDIO_U16LSB, "AUDIO_U16LSB" }, { AUDIO_S16LSB, "AUDIO_S16LSB" },
        { AUDIO_U16MSB, "AUDIO_U16MSB" }, { AUDIO_S16MSB, "AUDIO_S16MSB" },
        { AUDIO_S32LSB, "AUDIO_S32LSB" }, { AUDIO_S32MSB, "AUDIO_S32MSB" },
        { AUDIO_F32LSB, "AUDIO_F32LSB" }, { AUDIO_F32MSB, "AUDIO_F32MSB" },
    };

    auto it = format.find(format_value);
    return it->second;
}

static std::size_t get_sound_format_size(uint16_t format_value)
{
    static const std::map<int, std::size_t> format = {
        { AUDIO_U8, 1 },     { AUDIO_S8, 1 },     { AUDIO_U16LSB, 2 },
        { AUDIO_S16LSB, 2 }, { AUDIO_U16MSB, 2 }, { AUDIO_S16MSB, 2 },
        { AUDIO_S32LSB, 4 }, { AUDIO_S32MSB, 4 }, { AUDIO_F32LSB, 4 },
        { AUDIO_F32MSB, 4 },
    };
    auto it = format.find(format_value);
    return it->second;
}

#pragma pack(push, 1)
class Sound_buffer_impl final : public Sound_buffer
{
public:
    Sound_buffer_impl(std::string_view path, SDL_AudioDeviceID device_,
                      SDL_AudioSpec audio_spec);
    ~Sound_buffer_impl() final;

    void play(const properties prop) override final
    {
        // lock collback
        SDL_LockAudioDevice(device);

        current_index = 0;
        is_playing    = true;
        is_looped     = (prop == properties::looped);

        // unlock callback
        SDL_UnlockAudioDevice(device);
    }
    void setVolume(float value) override;

    std::unique_ptr<uint8_t[]> tmp_buf;
    uint8_t*                   buffer;
    uint32_t                   length;
    uint32_t                   current_index = 0;
    SDL_AudioDeviceID          device;
    bool                       is_playing = false;
    bool                       is_looped  = false;
    float                      volume     = 0;
};
#pragma pack(pop)

Sound_buffer_impl::Sound_buffer_impl(std::string_view  path,
                                     SDL_AudioDeviceID device_,
                                     SDL_AudioSpec     device_audio_spec)
    : buffer(nullptr)
    , length(0)
    , device(device_)
{
    SDL_RWops* file = SDL_RWFromFile(path.data(), "rb");
    if (file == nullptr)
    {
        throw std::runtime_error(std::string("Can't open audio file: ") +
                                 path.data());
    }

    SDL_AudioSpec file_audio_spec;

    if (nullptr == SDL_LoadWAV_RW(file, 1, &file_audio_spec, &buffer, &length))
    {
        throw std::runtime_error(std::string("Can't load wav: ") + path.data());
    }

    std::cout << "--------------------------------------------\n";
    std::cout << "audio format for: " << path << '\n'
              << "format: " << get_spund_format_name(file_audio_spec.format)
              << '\n'
              << "sample_size: "
              << get_sound_format_size(file_audio_spec.format) << '\n'
              << "Chanels: " << static_cast<uint32_t>(file_audio_spec.channels)
              << '\n'
              << "Frequency: " << file_audio_spec.freq << '\n'
              << "length: " << length << '\n'
              << "Time: "
              << static_cast<double>(length) /
                     (file_audio_spec.channels *
                      static_cast<uint32_t>(file_audio_spec.freq) *
                      get_sound_format_size(file_audio_spec.format))
              << "Sec " << std::endl;
    std::cout << "--------------------------------------------\n";

    if (file_audio_spec.channels != device_audio_spec.channels ||
        file_audio_spec.format != device_audio_spec.format ||
        file_audio_spec.freq != device_audio_spec.freq)
    {
        SDL_AudioCVT cvt;
        SDL_BuildAudioCVT(&cvt, file_audio_spec.format,
                          file_audio_spec.channels, file_audio_spec.freq,
                          device_audio_spec.format, device_audio_spec.channels,
                          device_audio_spec.freq);
        if (cvt.needed)
        {
            cvt.len = static_cast<int>(length);
            size_t new_budder_size =
                static_cast<size_t>(cvt.len * cvt.len_mult);
            tmp_buf.reset(new uint8_t[new_budder_size]);
            uint8_t* buff_tmp = tmp_buf.get();
            // copy old buffer to new memory
            std::copy_n(buffer, length, buff_tmp);

            SDL_FreeWAV(buffer);
            cvt.buf = buff_tmp;
            if (0 != SDL_ConvertAudio(&cvt))
            {
                std::cout << "Failed to convert audio from file: " << path
                          << " to audio device format" << std::endl;
            }

            buffer = tmp_buf.get();
            length = static_cast<uint32_t>(cvt.len_cvt);
        }
        else
        {
            // TODO no need to convert buffer, use as is
        }
    }
}

Sound_buffer::~Sound_buffer() {}

Sound_buffer_impl::~Sound_buffer_impl()
{
    if (!tmp_buf)
    {
        SDL_FreeWAV(buffer);
    }
    buffer = nullptr;
    length = 0;
}

void Sound_buffer_impl::setVolume(float value)
{
    volume = value;
}

#pragma pack(push, 4)
class Texture_gl_es20 final : public Texture
{
public:
    explicit Texture_gl_es20(std::string_view path)
        : file_path(path)
    {
        std::vector<unsigned char> png_file_in_memory;
        std::ifstream              ifs(path.data(), std::ios_base::binary);
        if (!ifs)
        {
            throw std::runtime_error(std::string("can't load texture: ") +
                                     path.data());
        }
        ifs.seekg(0, std::ios_base::end);
        std::streamoff pos_in_file = ifs.tellg();
        png_file_in_memory.resize(static_cast<size_t>(pos_in_file));
        ifs.seekg(0, std::ios_base::beg);
        if (!ifs)
        {
            throw std::runtime_error("can't load texture");
        }

        ifs.read(reinterpret_cast<char*>(png_file_in_memory.data()),
                 pos_in_file);
        if (!ifs.good())
        {
            throw std::runtime_error("can't load texture");
        }

        stbi_set_flip_vertically_on_load(true);
        int w          = 0;
        int h          = 0;
        int components = 0;

        unsigned char* decode_img = stbi_load_from_memory(
            reinterpret_cast<unsigned char*>(png_file_in_memory.data()),
            static_cast<int>(png_file_in_memory.size()), &w, &h, &components,
            4);

        if (decode_img == nullptr)
        {
            std::cerr << "error: can't load file:" << path << std::endl;
            throw std::runtime_error("can't  load texture!");
        }

        gen_texture_from_pixels(decode_img, static_cast<size_t>(w),
                                static_cast<size_t>(h));
        free(decode_img);

        this->width  = static_cast<uint32_t>(w);
        this->height = static_cast<uint32_t>(h);
    }
    Texture_gl_es20(const void* pixels, const size_t w, const size_t h)
        : width(w)
        , height(h)
    {
        gen_texture_from_pixels(pixels, w, h);
        if (file_path.empty())
        {
            file_path = "::memory::";
        }
    }
    ~Texture_gl_es20() override
    {
        glDeleteTextures(1, &tex_handl);
        CKOM_GL_CHECK()
    }

    void bind() const override
    {
        GLboolean is_texture = glIsTexture(tex_handl);
        assert(is_texture);
        CKOM_GL_CHECK()
        // it was
        glBindTexture(GL_TEXTURE_2D, tex_handl);
        CKOM_GL_CHECK()
    }

    std::uint32_t get_width() const final { return width; }
    std::uint32_t get_height() const final { return height; }
    std::string   get_name() const override { return file_path; }

    void add_ref() { ++ref_counter; }
    bool remove_ref()
    {
        --ref_counter;
        if (ref_counter == 0)
        {
            delete this;
            return true;
        }
        return false;
    }

private:
    void gen_texture_from_pixels(const void* pixels, const size_t w,
                                 const size_t h)
    {
        glGenTextures(1, &tex_handl);
        CKOM_GL_CHECK();
        glBindTexture(GL_TEXTURE_2D, tex_handl);
        CKOM_GL_CHECK();

        //        GLint   mipmap_level = 0;
        GLint   border  = 0;
        GLsizei width_  = static_cast<GLsizei>(w);
        GLsizei height_ = static_cast<GLsizei>(h);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width_, height_, border,
                     GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        CKOM_GL_CHECK()

        glGenerateMipmap(GL_TEXTURE_2D);
        CKOM_GL_CHECK()

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        CKOM_GL_CHECK()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        CKOM_GL_CHECK()
    }

    std::string   file_path;
    size_t        ref_counter = 1;
    GLuint        tex_handl   = 0;
    std::uint32_t width       = 0;
    std::uint32_t height      = 0;
};
#pragma pack(pop)

class Shader_gl_es20
{
public:
    Shader_gl_es20(std::string_view vertex_src, std::string_view fragment_src,
                   const std::vector<std::tuple<GLuint, const GLchar*>>& atrib)
    {
        vert_shader = compiled_shader(GL_VERTEX_SHADER, vertex_src);
        frag_shader = compiled_shader(GL_FRAGMENT_SHADER, fragment_src);
        if (vert_shader == 0 || frag_shader == 0)
        {
            throw std::runtime_error("can't compile shader");
        }
        program_id = link_program(atrib);
        if (program_id == 0)
        {
            throw std::runtime_error("can't link program id");
        }
    }

    void use() const
    {
        glUseProgram(program_id);
        CKOM_GL_CHECK()
    }
    void set_uniform(std::string_view uniform_name, Texture_gl_es20* texture)
    {
        assert(texture != nullptr);
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        CKOM_GL_CHECK()
        if (location == -1)
        {
            std::cerr << "Can't get uniform location from shader\n";
            std::runtime_error("cant get uniform location");
        }

        unsigned int texture_unit = 0;
        glActiveTextureMY(GL_TEXTURE0 + texture_unit);
        CKOM_GL_CHECK()

        texture->bind();

        glUniform1i(location, static_cast<int>(0 + texture_unit));
        CKOM_GL_CHECK()
    }
    void set_uniform(std::string_view uniform_name, const Color& clr)
    {
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        CKOM_GL_CHECK()
        if (location == -1)
        {
            std::cerr << "Can't get uniform location from shader\n";
            std::runtime_error("cant get uniform location");
        }
        float values[4] = { clr.get_r(), clr.get_g(), clr.get_b(),
                            clr.get_a() };
        glUniform4fv(location, 1, &values[0]);
        CKOM_GL_CHECK()
    }
    void set_uniform(std::string_view uniform_name, const mat2x3& mat)
    {
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        CKOM_GL_CHECK()
        if (location == -1)
        {
            std::cerr << "Can't get uniform location from shader: "
                      << uniform_name.data() << std::endl;
            throw std::runtime_error("can't get uniform location!");
        }
        // clang-format off
        float values[9] = { mat.colon0.x, mat.colon0.y, 0.f,
                            mat.colon1.x, mat.colon1.y, 0.f,
                            mat.delta.x, mat.delta.y, 1.f };

        // clang-format on
        glUniformMatrix3fv(location, 1, GL_FALSE, &values[0]);
        CKOM_GL_CHECK()
    }

    GLuint getProgram_id() const { return program_id; }

private:
    GLuint compiled_shader(GLenum shader_type, std::string_view src)
    {
        GLuint shader_id = glCreateShader(shader_type);
        CKOM_GL_CHECK()
        std::string_view vertex_shader_src = src;
        const char*      source            = vertex_shader_src.data();
        glShaderSource(shader_id, 1, &source, nullptr);
        CKOM_GL_CHECK()

        glCompileShader(shader_id);
        CKOM_GL_CHECK()

        GLint compiled_status = 0;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
        CKOM_GL_CHECK()
        if (compiled_status == 0)
        {
            GLint info_len = 0;
            glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
            CKOM_GL_CHECK()
            std::vector<char> info_chars(static_cast<size_t>(info_len));
            glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
            CKOM_GL_CHECK()
            glDeleteShader(shader_id);
            CKOM_GL_CHECK()

            std::string shader_type_name =
                shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";
            std::cerr << "Error compiling shader(vertex)\n"
                      << vertex_shader_src << "\n"
                      << info_chars.data();
            return 0;
        }
        return shader_id;
    }
    GLuint link_program(
        const std::vector<std::tuple<GLuint, const GLchar*>>& atrib)
    {
        GLuint program_id_ = glCreateProgram();
        CKOM_GL_CHECK()
        if (program_id_ == 0)
        {
            std::cerr << "failed create program ";
            throw std::runtime_error("can't link shader");
        }

        glAttachShader(program_id_, vert_shader);
        CKOM_GL_CHECK()
        glAttachShader(program_id_, frag_shader);
        CKOM_GL_CHECK()

        for (const auto& attr : atrib)
        {
            GLuint        loc  = std::get<0>(attr);
            const GLchar* name = std::get<1>(attr);
            glBindAttribLocation(program_id_, loc, name);
            CKOM_GL_CHECK()
        }

        glLinkProgram(program_id_);
        CKOM_GL_CHECK()

        GLint linked_status = 0;
        glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
        CKOM_GL_CHECK()
        if (linked_status == 0)
        {
            GLint info_len = 0;
            glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &info_len);
            CKOM_GL_CHECK()
            std::vector<char> info_log(static_cast<size_t>(info_len));
            glGetProgramInfoLog(program_id_, info_len, nullptr,
                                info_log.data());
            CKOM_GL_CHECK()
            std::cerr << "Error linked program:\n" << info_log.data();
            glDeleteProgram(program_id_);
            CKOM_GL_CHECK()
            return 0;
        }
        return program_id_;
    }

    GLuint vert_shader = 0;
    GLuint frag_shader = 0;
    GLuint program_id  = 0;
};

Triangle_p::Triangle_p()
    : v{ Vertex_p(), Vertex_p(), Vertex_p() }
{
}

Triangle_pc::Triangle_pc()
    : v{ Vertex_pc(), Vertex_pc(), Vertex_pc() }
{
}

Triangle_pct::Triangle_pct()
    : v{ Vertex_pct(), Vertex_pct(), Vertex_pct() }
{
}

static std::array<std::string_view, 32> eventNames = {
    { "left_pressed",
      "left_released",
      "right_pressed",
      "right_released",
      "up_pressed",
      "up_released",
      "down_pressed",
      "down_released",
      "select_pressed",
      "select_released",
      "start_pressed",
      "start_released",
      "button_triangle_pressed",
      "button_triangle_released",
      "button_square_pressed",
      "button_square_released",
      "button_cross_pressed",
      "button_cross_released",
      "button_circle_pressed",
      "button_circle_released",
      "space_btn_pressed",
      "space_btn_released",
      "mouse_move",

      "left_arrow_pressed",
      "left_arrow_released",
      "up_arrow_pressed",
      "up_arrow_released",
      "right_arrow_pressed",
      "right_arrow_released",
      "down_arrow_pressed",
      "down_arrow_released",

      "turn_off" }
};

std::ostream& operator<<(std::ostream& fout, const Event event)
{
    std::uint32_t value = static_cast<std::uint32_t>(event);
    std::uint32_t min   = static_cast<std::uint32_t>(Event::left_pressed);
    std::uint32_t max   = static_cast<std::uint32_t>(Event::turn_off);
    if (value >= min && value <= max)
    {
        fout << eventNames[value];
        return fout;
    }
    else
    {
        throw std::runtime_error("too big event value");
    }
}

static std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

std::istream& operator>>(std::istream& is, Color& clr)
{
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
    float a = 0.f;

    is >> r;
    is >> g;
    is >> b;
    is >> a;

    clr = Color(r, g, b, a);
    return is;
}

std::istream& operator>>(std::istream& is, Vertex_p& v)
{
    is >> v.p.x;
    is >> v.p.y;

    return is;
}

std::istream& operator>>(std::istream& is, Vertex_pc& v)
{
    is >> v.p.x;
    is >> v.p.y;
    is >> v.c;
    return is;
}

std::istream& operator>>(std::istream& is, Vertex_pct& v)
{
    is >> v.p.x;
    is >> v.p.y;
    is >> v.tx;
    is >> v.c;
    return is;
}

std::istream& operator>>(std::istream& is, Triangle_p& tr)
{
    is >> tr.v[0];
    is >> tr.v[1];
    is >> tr.v[2];
    return is;
}

std::istream& operator>>(std::istream& is, Triangle_pc& tr)
{
    is >> tr.v[0];
    is >> tr.v[1];
    is >> tr.v[2];
    return is;
}

std::istream& operator>>(std::istream& is, Triangle_pct& tr)
{
    is >> tr.v[0];
    is >> tr.v[1];
    is >> tr.v[2];
    return is;
}

#pragma pack(push, 1)
struct myBind
{
    myBind(SDL_KeyCode k, std::string_view str, Event press, Event releas,
           Keys ckom_k)
        : keyCode(k)
        , keyName(str)
        , eventPress(press)
        , eventReleas(releas)
        , ck_key(ckom_k)
    {
    }

    SDL_KeyCode      keyCode;
    std::string_view keyName;
    Event            eventPress;
    Event            eventReleas;
    Ckom::Keys       ck_key;
};
#pragma pack(pop)

const std::array<myBind, 15> keysArr{
    { myBind{ SDLK_w, "up", Event::up_pressed, Event::up_released, Keys::up },
      myBind{ SDLK_a, "left", Event::left_pressed, Event::left_released,
              Keys::left },
      myBind{ SDLK_s, "down", Event::down_pressed, Event::down_released,
              Keys::down },
      myBind{ SDLK_d, "right", Event::right_pressed, Event::right_released,
              Keys::right },
      myBind{ SDLK_i, "triangle", Event::button_triangle_pressed,
              Event::button_triangle_released, Keys::bTriangle },
      myBind{ SDLK_j, "square", Event::button_square_pressed,
              Event::button_square_released, Keys::bSquere },
      myBind{ SDLK_k, "cross", Event::button_cross_pressed,
              Event::button_cross_released, Keys::bCross },
      myBind{ SDLK_l, "circle", Event::button_circle_pressed,
              Event::button_circle_released, Keys::bCircle },
      myBind{ SDLK_p, "statr", Event::start_pressed, Event::start_released,
              Keys::start },
      myBind{ SDLK_ESCAPE, "select", Event::select_pressed,
              Event::select_released, Keys::select },
      myBind{ SDLK_LEFT, "left_arrow", Event::left_arrow_pressed,
              Event::left_arrow_released, Keys::left_a },
      myBind{ SDLK_UP, "up_arrow", Event::up_arrow_pressed,
              Event::up_arrow_released, Keys::up_a },
      myBind{ SDLK_RIGHT, "right_arrow", Event::right_arrow_pressed,
              Event::right_arrow_released, Keys::right_a },
      myBind{ SDLK_i, "down_arrow", Event::down_arrow_pressed,
              Event::down_arrow_released, Keys::down_a },
      myBind{ SDLK_SPACE, "space_btn", Event::space_btn_pressed,
              Event::space_btn_released, Keys::space } }
};

bool check_input(const SDL_Event& event, const myBind*& result)
{
    const auto it = std::find_if(
        std::begin(keysArr), std::end(keysArr),
        [&](const myBind b) { return b.keyCode == event.key.keysym.sym; });
    if (it != std::end(keysArr))
    {
        result = &(*it);
        return true;
    }
    return false;
}

class EngineImpl final : public Engine
{
public:
    std::string initialize(std::string_view /*conf*/) override final;

    Ckom::vec2 screen_size() const override
    {
        int w;
        int h;
        SDL_GetWindowSize(window, &w, &h);
        return Ckom::vec2(static_cast<float>(w), static_cast<float>(h));
    }
    float get_time() override final
    {
        std::uint32_t ms_from_library_initialization = SDL_GetTicks();
        float         seconds = ms_from_library_initialization * 0.001f;
        return seconds;
    }

    bool read_event(Event& ev) override final
    {
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event))
        {
            //            ImGui_ImplSdlGL3_ProcessEvent(&sdl_event);
            const myBind* binding = nullptr;

            if (sdl_event.type == SDL_QUIT)
            {
                ev = Event::turn_off;
                return true;
            }
            else if (sdl_event.type == SDL_KEYDOWN)
            {
                if (check_input(sdl_event, binding))
                {
                    ev = binding->eventPress;
                    return true;
                }
            }
            else if (sdl_event.type == SDL_KEYUP)
            {
                if (check_input(sdl_event, binding))
                {
                    ev = binding->eventReleas;
                    return true;
                }
            }
            else if (sdl_event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (sdl_event.button.button == SDL_BUTTON_LEFT)
                {
                    ev = Event::left_mouse_pressed;
                    return true;
                }
            }
            else if (sdl_event.type == SDL_MOUSEBUTTONUP)
            {
                if (sdl_event.button.button == SDL_BUTTON_LEFT)
                {
                    ev = Event::left_mouse_released;
                    return true;
                }
            }
            //            else if (sdl_event.type == SDL_MOUSEMOTION)
            //            {
            //                ev = Event::mouse_move;
            //                return true;
            //            }
        }
        return false;
    }

    bool is_key_down(const enum Keys key) override final
    {
        const auto it =
            std::find_if(begin(keysArr), end(keysArr),
                         [&](const myBind& b) { return b.ck_key == key; });
        if (it != end(keysArr))
        {
            const std::uint8_t* state = SDL_GetKeyboardState(nullptr);
            int sdl_scan_code         = SDL_GetScancodeFromKey(it->keyCode);
            return state[sdl_scan_code];
        }
        return false;
    }
    vec2 mouse_pos() override
    {
        int x;
        int y;
        SDL_GetMouseState(&x, &y);
        return vec2(static_cast<float>(x), static_cast<float>(y));
    }

    Texture* create_texture(std::string_view path) override final
    {
        //        return new Texture_gl_es20(path);

        std::string key{ path };
        auto        it = texture_cache.find(key);
        if (it == end(texture_cache))
        {
            Texture_gl_es20* t = new Texture_gl_es20(path);
            texture_cache.insert({ key, t });
            return t;
        }
        Texture_gl_es20* t = it->second;
        t->add_ref();
        return t;
    }

    Texture* create_texture_rgba32(const void* pixels, const size_t width,
                                   const size_t height) override
    {
        return new Texture_gl_es20(pixels, width, height);
    }
    void destroy_texture(Texture* texture) override final
    {
        std::string key{ texture->get_name() };
        auto        it = texture_cache.find(key);
        if (it != end(texture_cache))
        {
            Texture_gl_es20* t = it->second;
            if (t->remove_ref())
            {
                texture_cache.erase(key);
            }
        }
    }

    Vertex_buffer* create_vertex_buffer(const Triangle_pct* triangles,
                                        std::size_t         n) override final
    {
        assert(triangles != nullptr);
        return new Vertex_buffer_impl(triangles, n);
    }
    Vertex_buffer* create_vertex_buffer(const Vertex_pct* vert,
                                        std::size_t       count) override
    {
        assert(vert != nullptr);
        return new Vertex_buffer_impl(vert, count);
    }
    void destroy_vertex_buffer(Vertex_buffer* buff) override final
    {
        delete buff;
    }

    Index_buffer* create_index_buffer(const std::uint16_t* index,
                                      std::size_t          count) override
    {
        return new Index_buffer_impl(index, count);
    }
    void destroy_index_buffer(Index_buffer* buffer) override final
    {
        delete buffer;
    }

    void render(const Triangle_p& tr, const Color& col) override final
    {
        shader00->use();
        shader00->set_uniform("u_color", col);
        // vertex coordinates
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_p),
                              &tr.v[0].p.x);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(0);
        CKOM_GL_CHECK()

        glDrawArrays(GL_TRIANGLES, 0, 3);
        CKOM_GL_CHECK()
    }
    void render(const Triangle_pc& tr) override final
    {
        shader01->use();
        // positions
        glEnableVertexAttribArray(0);
        CKOM_GL_CHECK()
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v[0].p);
        CKOM_GL_CHECK()

        // colors
        glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(tr.v[0]),
                              &tr.v[0].c);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(1);
        CKOM_GL_CHECK()

        glDrawArrays(GL_TRIANGLES, 0, 3);
        CKOM_GL_CHECK()

        glDisableVertexAttribArray(1);
        CKOM_GL_CHECK()
    }
    void render(const Triangle_pct& tr, Texture* tx) override final
    {
        shader02->use();
        Texture_gl_es20* texture = static_cast<Texture_gl_es20*>(tx);
        texture->bind();
        shader02->set_uniform("s_texture", texture);
        // positions
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v[0].p);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(0);
        CKOM_GL_CHECK()
        // colors
        glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(tr.v[0]),
                              &tr.v[0].c);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(2);
        CKOM_GL_CHECK()

        // texture coordinates
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v[0].tx);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(1);
        CKOM_GL_CHECK()

        glDrawArrays(GL_TRIANGLES, 0, 3);
        CKOM_GL_CHECK()

        glDisableVertexAttribArray(1);
        CKOM_GL_CHECK()
        glDisableVertexAttribArray(2);
        CKOM_GL_CHECK()
    }
    void render(const Triangle_pct& tr, Texture* tex,
                const mat2x3& matrix) override
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CKOM_GL_CHECK()

        shader03->use();
        Texture_gl_es20* texture = static_cast<Texture_gl_es20*>(tex);
        texture->bind();
        shader03->set_uniform("s_texture", texture);
        shader03->set_uniform("u_matrix", matrix);

        glEnableVertexAttribArray(0); // pos
        CKOM_GL_CHECK()
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v[0].p);
        CKOM_GL_CHECK()

        glEnableVertexAttribArray(1); // color
        CKOM_GL_CHECK()
        glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(tr.v[0]),
                              &tr.v->c);
        CKOM_GL_CHECK()

        glEnableVertexAttribArray(2); // texture coord
        CKOM_GL_CHECK()
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v->tx);
        CKOM_GL_CHECK()

        glDrawArrays(GL_TRIANGLES, 0, 3);
        CKOM_GL_CHECK()

        glDisableVertexAttribArray(1);
        CKOM_GL_CHECK()
        glDisableVertexAttribArray(2);
        CKOM_GL_CHECK()
    }
    void render(const Vertex_buffer& buff, Texture* tex,
                const mat2x3& mat) override final
    {
        shader03->use();
        Texture_gl_es20* texture = static_cast<Texture_gl_es20*>(tex);
        texture->bind();
        shader03->set_uniform("s_texture", texture);
        shader03->set_uniform("u_matrix", mat);

        buff.bind();

        glEnableVertexAttribArray(0); // position
        CKOM_GL_CHECK()
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_pct),
                              nullptr);
        CKOM_GL_CHECK()

        glEnableVertexAttribArray(1); // color
        CKOM_GL_CHECK()
        glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                              sizeof(Vertex_pct),
                              reinterpret_cast<void*>(sizeof(Vertex_pct::p) +
                                                      sizeof(Vertex_pct::tx)));
        CKOM_GL_CHECK()

        glEnableVertexAttribArray(2); // tecture coord
        CKOM_GL_CHECK()
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_pct),
                              reinterpret_cast<void*>(sizeof(Vertex_pct::p)));
        CKOM_GL_CHECK()

        GLsizei num_of_vertex = static_cast<GLsizei>(buff.size());
        glDrawArrays(GL_TRIANGLES, 0, num_of_vertex);
        CKOM_GL_CHECK()

        glDisableVertexAttribArray(1);
        CKOM_GL_CHECK()
        glDisableVertexAttribArray(2);
        CKOM_GL_CHECK()
    }
    void render(const Vertex_buffer* buff, const Index_buffer* indexes,
                const Texture* tex, const std::uint16_t* start_vertex_indx,
                size_t num_vertexes) override
    {
        tex->bind();

        buff->bind();

        indexes->bind();

        glEnableVertexAttribArray(0); // pos
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(1); // texture
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(2); // color
        CKOM_GL_CHECK()

        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_pct),
                              nullptr);
        CKOM_GL_CHECK()
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_pct),
                              reinterpret_cast<void*>(sizeof(Vertex_pct::p)));
        CKOM_GL_CHECK()
        glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE,
                              sizeof(Vertex_pct),
                              reinterpret_cast<void*>(sizeof(Vertex_pct::p) +
                                                      sizeof(Vertex_pct::tx)));
        CKOM_GL_CHECK()

        glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(num_vertexes),
                       GL_UNSIGNED_SHORT, start_vertex_indx);
        CKOM_GL_CHECK()
    }

    Sound_buffer* create_sound_buffer(const std::string_view path) override
    {
        Sound_buffer_impl* sound =
            new Sound_buffer_impl(path, audio_device, audio_device_spec);
        SDL_LockAudioDevice(audio_device); // for push_back only
        sounds.push_back(sound);
        SDL_UnlockAudioDevice(audio_device);
        return sound;
    }
    void destroy_sound_buffer(Sound_buffer* s_b) override
    {
        std::erase(sounds, s_b);
        //        std::cout << "sound destroy" << std::endl;
        delete s_b;
    }

    void swap_buffers() override final
    {

        //        ImGui_ImplSdlGL3_RenderDrawLists(ImGui::GetDrawData());

        SDL_GL_SwapWindow(window);

        //        ImGui_ImplSdlGL3_NewFrame(window);

        glClear(GL_COLOR_BUFFER_BIT);
        CKOM_GL_CHECK()
    }

    void uninitialize() override final
    {
        SDL_GL_DeleteContext(gl_context);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

private:
    static void audio_callback(void*, uint8_t*, int);

    std::unordered_map<std::string, Texture_gl_es20*> texture_cache;
    SDL_Window*                                       window     = nullptr;
    SDL_GLContext                                     gl_context = nullptr;

    Shader_gl_es20* shader00 = nullptr;
    Shader_gl_es20* shader01 = nullptr;
    Shader_gl_es20* shader02 = nullptr;
    Shader_gl_es20* shader03 = nullptr;

    SDL_AudioDeviceID               audio_device;
    SDL_AudioSpec                   audio_device_spec;
    std::vector<Sound_buffer_impl*> sounds;

    uint32_t gl_default_vbo = 0;
};

static bool    alreadyExist = false;
static Engine* g_engine     = nullptr;

Engine* create_engine()
{
    if (alreadyExist)
    {
        throw std::runtime_error("is already exist");
    }
    Engine* result = new EngineImpl;
    g_engine       = result;
    alreadyExist   = true;
    return result;
}
void destroy_engine(Engine* eng)
{
    if (alreadyExist == false)
    {
        throw std::runtime_error("engine not created!!!");
    }
    if (nullptr == eng)
    {
        throw std::runtime_error("eng iss nullptr!!!");
    }
    delete eng;
    alreadyExist = false;
}

Engine::~Engine() {}

std::string EngineImpl::initialize(std::string_view /*conf*/)
{
    std::stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version link     = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(link.major, link.minor, link.patch))
    {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << ' ' << link << std::endl;
    }

    const int initResult = SDL_Init(SDL_INIT_EVERYTHING);
    CKOM_GL_CHECK()
    if (initResult != 0)
    {
        const char* errMsg = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << errMsg << std::endl;
        return serr.str();
    }

    // SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    window = SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, 1024, 760,
                              ::SDL_WINDOW_OPENGL);

    if (window == nullptr)
    {
        const char* errMsg = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << errMsg << std::endl;
        SDL_Quit();
        return serr.str();
    }

    std::cout << "init Ckom::engine" << std::endl;
    if (std::string_view("Windows") == SDL_GetPlatform())
    {
        if (!std::cout)
        {
#ifdef _WIN32
            AllocConsole();
#endif
            FILE* f = std::freopen("CON", "w", stdout);
            if (!f)
            {
                throw std::runtime_error("can't reopen stdout");
            }
            std::cout.clear();
            std::cerr << "test" << std::endl;
            if (!std::cout)
            {
                throw std::runtime_error("can't print with std::cout");
            }
        }
    }

    // for render

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    // for renfer
    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
        gl_context = SDL_GL_CreateContext(window);
        if (gl_context == nullptr)
        {
            std::string msg("Can't create openGL context: ");
            msg += SDL_GetError();
            serr << msg << std::endl;
            return serr.str();
        }
    }

    int glMajor = 0;
    int result  = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &glMajor);
    assert(result == 0);
    int glMinor = 0;
    result      = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &glMinor);
    assert(result == 0);

    std::cout << "GL_context: " << glMajor << '.' << glMinor << std::endl;

    if (glMajor <= 2 && glMinor < 0)
    {
        serr << "Corrent openGL context version: " << glMajor << ':' << glMinor
             << '\n'
             << "Need openGl version at least 2:1\n"
             << std::flush;
        return serr.str();
    }

    try
    {
        load_gl_function("glCreateShader", glCreateShader);
        load_gl_function("glShaderSource", glShaderSource);
        load_gl_function("glCompileShader", glCompileShader);
        load_gl_function("glGetShaderiv", glGetShaderiv);
        load_gl_function("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_function("glDeleteShader", glDeleteShader);
        load_gl_function("glCreateProgram", glCreateProgram);
        load_gl_function("glAttachShader", glAttachShader);
        load_gl_function("glBindAttribLocation", glBindAttribLocation);
        load_gl_function("glLinkProgram", glLinkProgram);
        load_gl_function("glGetProgramiv", glGetProgramiv);
        load_gl_function("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_function("glDeleteProgram", glDeleteProgram);
        load_gl_function("glUseProgram", glUseProgram);
        load_gl_function("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_function("glEnableVertexAttribArray",
                         glEnableVertexAttribArray);
        load_gl_function("glValidateProgram", glValidateProgram);
        load_gl_function("glBindBuffer", glBindBuffer);
        load_gl_function("glGenBuffers", glGenBuffers);
        load_gl_function("glGenVertexArrays", glGenVertexArrays);
        load_gl_function("glBindVertexArray", glBindVertexArray);
        load_gl_function("glBufferData", glBufferData);
        load_gl_function("glGetUniformLocation", glGetUniformLocation);
        load_gl_function("glUniform1f", glUniform1f);
        load_gl_function("glDeleteBuffers", glDeleteBuffers);
        load_gl_function("glDisableVertexAttribArray",
                         glDisableVertexAttribArray);
        load_gl_function("glUniform1i", glUniform1i);
        load_gl_function("glActiveTexture", glActiveTextureMY);
        load_gl_function("glUniform4fv", glUniform4fv);
        load_gl_function("glUniformMatrix3fv", glUniformMatrix3fv);
        load_gl_function("glBufferSubData", glBufferSubData);
        load_gl_function("glUniformMatrix3fv", glUniformMatrix3fv);
        load_gl_function("glActiveTexture", glActiveTexture_);
        load_gl_function("glUniformMatrix4fv", glUniformMatrix4fv);
        load_gl_function("glGenerateMipmap", glGenerateMipmap);
        load_gl_function("glBlendFuncSeparate", glBlendFuncSeparate);
        load_gl_function("glBlendEquationSeparate", glBlendEquationSeparate);
        load_gl_function("glGetAttribLocation", glGetAttribLocation);
        load_gl_function("glDetachShader", glDetachShader);
        load_gl_function("glGenerateMipmaps", glGenerateMipmaps);
    }
    catch (std::exception& ex)
    {
        return ex.what();
    }

    glGenBuffers(1, &gl_default_vbo);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, gl_default_vbo);
    CKOM_GL_CHECK()
    uint32_t data_size_in_bytes = 0;
    glBufferData(GL_ARRAY_BUFFER, data_size_in_bytes, nullptr, GL_STATIC_DRAW);
    CKOM_GL_CHECK()
    glBufferSubData(GL_ARRAY_BUFFER, 0, data_size_in_bytes, nullptr);
    CKOM_GL_CHECK()

    shader00 = new Shader_gl_es20(R"(
                                  attribute vec2 a_position;
                                  void main()
                                  {
                                      gl_Position = vec4(a_position, 1.0, 1.0);
                                  }
                                  )",
                                  R"(
                                  #ifdef GL_ES
                                  precision mediump float;
                                  #endif
                                  uniform vec4 u_color;
                                  void main()
                                  {
                                      gl_FragColor = u_color;
                                  }
                                  )",
                                  { { 0, "a_position" } });

    shader00->use();
    shader00->set_uniform("u_color", Color(1.f, 0.f, 0.f, 1.f));

    shader01 = new Shader_gl_es20(
        R"(
                attribute vec2 a_position;
                attribute vec4 a_color;
                varying vec4 v_color;
                void main()
                {
                v_color = a_color;
                gl_Position = vec4(a_position, 1.0, 1.0);
                }
                )",
        R"(
                #ifdef GL_ES
                precision mediump float;
                #endif
                varying vec4 v_color;
                void main()
                {
                gl_FragColor = v_color;
                }
                )",
        { { 0, "a_position" }, { 1, "a_color" } });

    shader01->use();

    shader02 = new Shader_gl_es20(
        R"(
                attribute vec2 a_position;
                attribute vec2 a_tex_coord;
                attribute vec4 a_color;
                varying vec4 v_color;
                varying vec2 v_tex_coord;
                void main()
                {
                v_tex_coord = a_tex_coord;
                v_color = a_color;
                gl_Position = vec4(a_position, 1.0, 1.0);
                }
                )",
        R"(
                #ifdef GL_ES
                precision mediump float;
                #endif
                varying vec2 v_tex_coord;
                varying vec4 v_color;
                uniform sampler2D s_texture;
                void main()
                {
                gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
                }
                )",
        { { 0, "a_position" }, { 1, "a_color" }, { 2, "a_tex_coord" } });

    // turn on rendering with just created shader program
    shader02->use();

    shader03 = new Shader_gl_es20(
        R"(
         attribute vec2 a_position;
         attribute vec2 a_tex_coord;
         attribute vec4 a_color;

        uniform mat3 u_matrix;

        varying vec4 v_color;
        varying vec2 v_tex_coord;

        void main()
        {
          v_tex_coord = a_tex_coord;
          v_color = a_color;
          vec3 pos = u_matrix * vec3(a_position, 1.0);
          gl_Position = vec4(pos, 1.0);
        }
        )",
        R"(
        #ifdef GL_ES
        precision mediump float;
        #endif
        varying vec2 v_tex_coord;
        varying vec4 v_color;

        uniform sampler2D s_texture;
        void main()
        {
          gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
        }
        )",
        { { 0, "a_position" }, { 2, "a_tex_coord" }, { 1, "a_color" } });

    shader03->use();

    glEnable(GL_BLEND);
    CKOM_GL_CHECK()
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    CKOM_GL_CHECK()

    glClearColor(0.f, 0.0, 0.f, 0.0f);
    CKOM_GL_CHECK()

    Ckom::vec2 window_size = screen_size();
    glViewport(0, 0, window_size.x, window_size.y);
    CKOM_GL_CHECK()

    // init audio
    audio_device_spec.freq     = 48000;
    audio_device_spec.format   = AUDIO_S16LSB;
    audio_device_spec.channels = 2;
    audio_device_spec.samples  = 1024;
    audio_device_spec.callback = EngineImpl::audio_callback;
    audio_device_spec.userdata = this;

    const int num_audio_drivers = SDL_GetNumAudioDrivers();
    for (int i = 0; i < num_audio_drivers; ++i)
    {
        std::cout << "audio_driver #:" << i << ' ' << SDL_GetAudioDriver(i)
                  << '\n';
    }
    std::cout << std::flush;

    // TODO on windows 10 only directsound - works for me
    if (std::string_view("Windows") == SDL_GetPlatform())
    {
        const char* selected_audio_driver = SDL_GetAudioDriver(1);
        std::cout << "selected_audio_driver: " << selected_audio_driver
                  << std::endl;

        if (0 != SDL_AudioInit(selected_audio_driver))
        {
            std::cout << "can't init SDL audio\n" << std::flush;
        }
    }

    const char* default_audio_device_name = nullptr;

    const int num_audio_devices =
        SDL_GetNumAudioDevices(SDL_FALSE); // false returned only output devices
    if (num_audio_devices > 0)
    {
        default_audio_device_name = SDL_GetAudioDeviceName(0, SDL_FALSE);
        for (int i = 0; i < num_audio_devices; ++i)
        {
            std::cout << "audio device #" << i << ": "
                      << SDL_GetAudioDeviceName(i, SDL_FALSE) << '\n';
        }
    }

    std::cout << std::flush;

    audio_device =
        SDL_OpenAudioDevice(default_audio_device_name, 0, &audio_device_spec,
                            nullptr, SDL_AUDIO_ALLOW_ANY_CHANGE);

    if (audio_device == 0)
    {
        std::cerr << "failed open audio device: " << SDL_GetError();
        throw std::runtime_error("audio failed");
    }
    else
    {
        std::cout << "-------------------------------------------------\n";
        std::cout << "audio device selected: " << default_audio_device_name
                  << '\n'
                  << "freq: " << audio_device_spec.freq << '\n'
                  << "format: "
                  << get_spund_format_name(audio_device_spec.format) << '\n'
                  << "channels: "
                  << static_cast<uint32_t>(audio_device_spec.channels) << '\n'
                  << "samples: " << audio_device_spec.samples << '\n'
                  << std::flush;
        SDL_PauseAudioDevice(audio_device, SDL_FALSE);
    }

    //    if (!ImGui_ImplSdlGL3_Init(window))
    //    {
    //        return "Error: failed to init ImGui!!";
    //    }

    //    ImGui_ImplSdlGL3_NewFrame(window);

    return "";
}

void EngineImpl::audio_callback(void* engine_ptr, uint8_t* stream,
                                int stream_size)
{
    std::fill_n(stream, stream_size, '\0');

    EngineImpl* eng = static_cast<EngineImpl*>(engine_ptr);

    for (Sound_buffer_impl* snd_buffer : eng->sounds)
    {
        const auto int_volume = static_cast<int>(
            std::round(snd_buffer->volume * SDL_MIX_MAXVOLUME));

        if (snd_buffer->is_playing)
        {
            uint32_t rest = snd_buffer->length - snd_buffer->current_index;
            uint8_t* current_buff =
                &snd_buffer->buffer[snd_buffer->current_index];

            if (rest <= static_cast<uint32_t>(stream_size))
            {
                // copy rest in buffer
                SDL_MixAudioFormat(stream, current_buff,
                                   eng->audio_device_spec.format, rest,
                                   int_volume);
                snd_buffer->current_index += rest;
            }
            else
            {
                SDL_MixAudioFormat(
                    stream, current_buff, eng->audio_device_spec.format,
                    static_cast<uint32_t>(stream_size), int_volume);
                snd_buffer->current_index += static_cast<uint32_t>(stream_size);
            }

            if (snd_buffer->current_index == snd_buffer->length)
            {
                if (snd_buffer->is_looped)
                {
                    // start from begin
                    snd_buffer->current_index = 0;
                }
                else
                {
                    snd_buffer->is_playing = false;
                }
            }
        }
    }
}

Color::Color(uint32_t rgba_)
    : rgba(rgba_)
{
}

Color::Color(float r, float g, float b, float a)
{
    assert(r <= 1 && r >= 0);
    assert(g <= 1 && g >= 0);
    assert(b <= 1 && b >= 0);
    assert(a <= 1 && a >= 0);

    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);

    rgba = a_ << 24 | b_ << 16 | g_ << 8 | r_;
}

float Color::get_r() const
{
    std::uint32_t r_ = (rgba & 0x000000FF) >> 0;
    return r_ / 255.f;
}
float Color::get_g() const
{
    std::uint32_t g_ = (rgba & 0x0000FF00) >> 8;
    return g_ / 255.f;
}
float Color::get_b() const
{
    std::uint32_t b_ = (rgba & 0x00FF0000) >> 16;
    return b_ / 255.f;
}
float Color::get_a() const
{
    std::uint32_t a_ = (rgba & 0xFF000000) >> 24;
    return a_ / 255.f;
}

void Color::set_r(const float r)
{
    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    rgba &= 0xFFFFFF00;
    rgba |= (r_ << 0);
}
void Color::set_g(const float g)
{
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    rgba &= 0xFFFF00FF;
    rgba |= (g_ << 8);
}
void Color::set_b(const float b)
{
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    rgba &= 0xFF00FFFF;
    rgba |= (b_ << 16);
}
void Color::set_a(const float a)
{
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);
    rgba &= 0x00FFFFFF;
    rgba |= a_ << 24;
}

std::istream& operator>>(std::istream& is, vec2& vec)
{
    is >> vec.x;
    is >> vec.y;

    return is;
}

std::istream& operator>>(std::istream& is, mat2x3& matrix)
{
    is >> matrix.colon0.x;
    is >> matrix.colon1.x;
    is >> matrix.colon0.y;
    is >> matrix.colon1.y;

    return is;
}

bool operator==(const vec2& l, const vec2& r)
{
    vec2 diff = l + vec2(-r.x, -r.y);
    return diff.length() <= 0.000001f;
}

} // end namespace Ckom

static float                 g_Time             = 0.0;
static float                 g_mouse_pressed[3] = { false, false, false };
static float                 g_mouse_wheel      = 0.f;
static Ckom::Shader_gl_es20* g_im_gui_shader    = nullptr;

void ImGui_ImplSdlGL3_RenderDrawLists(ImDrawData* draw_data)
{
    ImGuiIO& io        = ImGui::GetIO();
    int      fb_width  = int(io.DisplaySize.x * io.DisplayFramebufferScale.x);
    int      fb_height = int(io.DisplaySize.y * io.DisplayFramebufferScale.y);
    if (fb_width == 0 || fb_height == 0)
    {
        return;
    }
    draw_data->ScaleClipRects(io.DisplayFramebufferScale);

    Ckom::Texture_gl_es20* texture =
        reinterpret_cast<Ckom::Texture_gl_es20*>(io.Fonts->TexID);
    assert(texture != nullptr);

    Ckom::mat2x3 orto_matrix =
        Ckom::mat2x3::scale(2.0f / io.DisplaySize.x, -2.0f / io.DisplaySize.y) *
        Ckom::mat2x3::moove(Ckom::vec2(-1.0f, 1.0f));

    g_im_gui_shader->use();
    g_im_gui_shader->set_uniform("Texture", texture);
    g_im_gui_shader->set_uniform("ProjMtx", orto_matrix);

    for (int n = 0; n < draw_data->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list          = draw_data->CmdLists[n];
        const ImDrawIdx*  idx_buffer_offset = nullptr;

        static_assert(sizeof(Ckom::Vertex_pct) == sizeof(ImDrawVert), "");
        static_assert(sizeof(Ckom::Vertex_pct::p) == sizeof(ImDrawVert::pos),
                      "");
        static_assert(sizeof(Ckom::Vertex_pct::tx) == sizeof(ImDrawVert::uv),
                      "");
        static_assert(
            offsetof(Ckom::Vertex_pct, p) == offsetof(ImDrawVert, pos), "");
        static_assert(
            offsetof(Ckom::Vertex_pct, tx) == offsetof(ImDrawVert, uv), "");

        const Ckom::Vertex_pct* vertex_data =
            reinterpret_cast<const Ckom::Vertex_pct*>(cmd_list->VtxBuffer.Data);
        size_t vert_count = static_cast<size_t>(cmd_list->VtxBuffer.size());

        Ckom::Vertex_buffer* vertex_buff =
            Ckom::g_engine->create_vertex_buffer(vertex_data, vert_count);

        const std::uint16_t* indexes = cmd_list->IdxBuffer.Data;
        size_t index_count = static_cast<size_t>(cmd_list->IdxBuffer.size());

        Ckom::Index_buffer* index_buff =
            Ckom::g_engine->create_index_buffer(indexes, index_count);

        for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
            assert(pcmd->UserCallback == nullptr);

            Ckom::Texture* tx =
                reinterpret_cast<Ckom::Texture*>(pcmd->TextureId);

            Ckom::g_engine->render(vertex_buff, index_buff, tx,
                                   idx_buffer_offset, pcmd->ElemCount);

            idx_buffer_offset += pcmd->ElemCount;
        } // cmd_i end

        Ckom::g_engine->destroy_vertex_buffer(vertex_buff);
        Ckom::g_engine->destroy_index_buffer(index_buff);
    } // end of n
}

static const char* ImGui_ImplSdlGL3_GetClipboardText(void*)
{
    return SDL_GetClipboardText();
}

static void ImGui_ImplSdlGL3_SetClipboardText(void*, const char* text)
{
    SDL_SetClipboardText(text);
}

bool ImGui_ImplSdlGL3_ProcessEvent(const SDL_Event* event)
{
    ImGuiIO& io = ImGui::GetIO();
    switch (event->type)
    {
        case SDL_MOUSEWHEEL:
        {
            if (event->wheel.y > 0)
                g_mouse_wheel = 1;
            if (event->wheel.y < 0)
                g_mouse_wheel = -1;
            return true;
        }
        case SDL_MOUSEBUTTONDOWN:
        {
            if (event->button.button == SDL_BUTTON_LEFT)
                g_mouse_pressed[0] = true;
            if (event->button.button == SDL_BUTTON_RIGHT)
                g_mouse_pressed[1] = true;
            if (event->button.button == SDL_BUTTON_MIDDLE)
                g_mouse_pressed[2] = true;
            return true;
        }
        case SDL_TEXTINPUT:
        {
            io.AddInputCharactersUTF8(event->text.text);
            return true;
        }
        case SDL_KEYDOWN:
        case SDL_KEYUP:
        {
            int key          = event->key.keysym.sym & ~SDLK_SCANCODE_MASK;
            io.KeysDown[key] = (event->type == SDL_KEYDOWN);
            io.KeyShift      = ((SDL_GetModState() & KMOD_SHIFT) != 0);
            io.KeyCtrl       = ((SDL_GetModState() & KMOD_CTRL) != 0);
            io.KeyAlt        = ((SDL_GetModState() & KMOD_ALT) != 0);
            io.KeySuper      = ((SDL_GetModState() & KMOD_GUI) != 0);
            return true;
        }
    }
    return false;
}

void ImGui_ImplSdlGL3_CreateFontsTexture()
{
    // Build texture atlas
    ImGuiIO&       io     = ImGui::GetIO();
    unsigned char* pixels = nullptr;
    int            width  = 0;
    int            height = 0;
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

    io.Fonts->TexID = Ckom::g_engine->create_texture_rgba32(
        pixels, static_cast<size_t>(width), static_cast<size_t>(height));
}

bool ImGui_ImplSdlGL3_CreateDeviceObjects()
{
    const GLchar* vertex_shader =
        //"#version 150\n"
        "#if defined(GL_ES)\n"
        "precision highp float;\n"
        "#endif //GL_ES\n"
        "uniform mat3 ProjMtx;\n"
        "attribute vec2 Position;\n"
        "attribute vec2 UV;\n"
        "attribute vec4 Color;\n"
        "varying vec2 Frag_UV;\n"
        "varying vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	Frag_UV = UV;\n"
        "	Frag_Color = Color;\n"
        "	gl_Position = vec4(ProjMtx * vec3(Position.xy,1), 1);\n"
        "}\n";

    const GLchar* fragment_shader =
        //"#version 150\n"
        "#if defined(GL_ES)\n"
        "precision highp float;\n"
        "#endif //GL_ES\n"
        "uniform sampler2D Texture;\n"
        "varying vec2 Frag_UV;\n"
        "varying vec4 Frag_Color;\n"
        //"out vec4 Out_Color;\n"
        "void main()\n"
        "{\n"
        "	gl_FragColor = Frag_Color * texture2D( Texture, Frag_UV);\n"
        "}\n";

    g_im_gui_shader = new Ckom::Shader_gl_es20(
        vertex_shader, fragment_shader,
        { { 0, "Position" }, { 1, "UV" }, { 2, "Color" } });

    ImGui_ImplSdlGL3_CreateFontsTexture();

    return true;
}

void ImGui_ImplSdlGL3_InvalidateDeviceObjects()
{
    void*          ptr     = ImGui::GetIO().Fonts->TexID;
    Ckom::Texture* texture = reinterpret_cast<Ckom::Texture*>(ptr);
    Ckom::g_engine->destroy_texture(texture);

    delete g_im_gui_shader;
    g_im_gui_shader = nullptr;
}

bool ImGui_ImplSdlGL3_Init(SDL_Window* window)
{
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;  // Enable Keyboard
    // Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    // ImGui::StyleColorsClassic();

    ImGuiIO& io = ImGui::GetIO();
    // g_Window    = window;

    // Setup back-end capabilities flags
    //    io.BackendFlags |=
    //        ImGuiBackendFlags_HasMouseCursors; // We can honor
    //        GetMouseCursor()
    //                                           // values (optional)
    //    io.BackendFlags |=
    //        ImGuiBackendFlags_HasSetMousePos; // We can honor
    //        io.WantSetMousePos
    //                                          // requests (optional, rarely
    //                                          used)
    io.BackendPlatformName = "imgui_impl_sdl";

    // Keyboard mapping. ImGui will use those indices to peek into the
    // io.KeysDown[] array.
    io.KeyMap[ImGuiKey_Tab]        = SDL_SCANCODE_TAB;
    io.KeyMap[ImGuiKey_LeftArrow]  = SDL_SCANCODE_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow]    = SDL_SCANCODE_UP;
    io.KeyMap[ImGuiKey_DownArrow]  = SDL_SCANCODE_DOWN;
    io.KeyMap[ImGuiKey_PageUp]     = SDL_SCANCODE_PAGEUP;
    io.KeyMap[ImGuiKey_PageDown]   = SDL_SCANCODE_PAGEDOWN;
    io.KeyMap[ImGuiKey_Home]       = SDL_SCANCODE_HOME;
    io.KeyMap[ImGuiKey_End]        = SDL_SCANCODE_END;
    io.KeyMap[ImGuiKey_Insert]     = SDL_SCANCODE_INSERT;
    io.KeyMap[ImGuiKey_Delete]     = SDL_SCANCODE_DELETE;
    io.KeyMap[ImGuiKey_Backspace]  = SDL_SCANCODE_BACKSPACE;
    io.KeyMap[ImGuiKey_Space]      = SDL_SCANCODE_SPACE;
    io.KeyMap[ImGuiKey_Enter]      = SDL_SCANCODE_RETURN;
    io.KeyMap[ImGuiKey_Escape]     = SDL_SCANCODE_ESCAPE;
    io.KeyMap[ImGuiKey_A]          = SDL_SCANCODE_A;
    io.KeyMap[ImGuiKey_C]          = SDL_SCANCODE_C;
    io.KeyMap[ImGuiKey_V]          = SDL_SCANCODE_V;
    io.KeyMap[ImGuiKey_X]          = SDL_SCANCODE_X;
    io.KeyMap[ImGuiKey_Y]          = SDL_SCANCODE_Y;
    io.KeyMap[ImGuiKey_Z]          = SDL_SCANCODE_Z;
    /*
        g_MouseCursors[ImGuiMouseCursor_Arrow] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
        g_MouseCursors[ImGuiMouseCursor_TextInput] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
        g_MouseCursors[ImGuiMouseCursor_ResizeAll] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL);
        g_MouseCursors[ImGuiMouseCursor_ResizeNS] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENS);
        g_MouseCursors[ImGuiMouseCursor_ResizeEW] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEWE);
        g_MouseCursors[ImGuiMouseCursor_ResizeNESW] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW);
        g_MouseCursors[ImGuiMouseCursor_ResizeNWSE] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE);
        g_MouseCursors[ImGuiMouseCursor_Hand] =
            SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);
    */
    //    io.RenderDrawListsFn =
    //        imgui_to_engine_render; // Alternatively you can set this to
    //    // NULL and call ImGui::GetDrawData()
    //    // after ImGui::Render() to get the
    //    // same ImDrawData pointer.
    io.SetClipboardTextFn = ImGui_ImplSdlGL3_SetClipboardText;
    io.GetClipboardTextFn = ImGui_ImplSdlGL3_GetClipboardText;
    io.ClipboardUserData  = nullptr;

#ifdef _WIN32
    SDL_SysWMinfo wmInfo;
    SDL_VERSION(&wmInfo.version);
    SDL_GetWindowWMInfo(window, &wmInfo);
    io.ImeWindowHandle = wmInfo.info.win.window;
#else
    (void)window;
#endif

    g_Time = SDL_GetTicks() / 1000.f;

    return true;
}

void ImGui_ImplSdlGL3_Shutdown()
{
    ImGui_ImplSdlGL3_InvalidateDeviceObjects();
    ImGui::DestroyContext();
}

void ImGui_ImplSdlGL3_NewFrame(SDL_Window* window)
{
    ImGuiIO& io = ImGui::GetIO();

    if (io.Fonts->TexID == nullptr)
    {
        ImGui_ImplSdlGL3_CreateDeviceObjects();
    }

    int w, h;
    int display_w, display_h;
    SDL_GetWindowSize(window, &w, &h);
    SDL_GL_GetDrawableSize(window, &display_w, &display_h);
    io.DisplaySize             = ImVec2(float(w), float(h));
    io.DisplayFramebufferScale = ImVec2(w > 0 ? float(display_w / w) : 0.f,
                                        h > 0 ? float(display_h / h) : 0.f);

    // time step
    Uint32 time         = SDL_GetTicks();
    float  current_time = time / 1000.0f;
    io.DeltaTime        = current_time - g_Time; // (1.0f / 60.f);
    if (io.DeltaTime <= 0)
    {
        io.DeltaTime = 0.00001f;
    }
    g_Time = current_time;

    int      mousex, mousey;
    uint32_t mouse_state = SDL_GetMouseState(&mousex, &mousey);
    if (SDL_GetWindowFlags(window) & SDL_WINDOW_MOUSE_FOCUS)
    {
        io.MousePos = ImVec2(float(mousex), float(mousey));
    }
    else
    {
        io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);
    }

    io.MouseDown[0] =
        g_mouse_pressed[0] || (mouse_state & SDL_BUTTON_LEFT) != 0;
    io.MouseDown[1] = g_mouse_pressed[1];
    io.MouseDown[2] = g_mouse_pressed[2];

    g_mouse_pressed[0] = g_mouse_pressed[1] = g_mouse_pressed[2] = false;

    io.MouseWheel = g_mouse_wheel;
    g_mouse_wheel = 0.f;

    SDL_ShowCursor(io.MouseDrawCursor ? 0 : 1);
}
