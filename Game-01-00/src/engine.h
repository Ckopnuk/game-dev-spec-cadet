#pragma once

#include <iosfwd>
#include <string>
#include <string_view>
#include <variant>

#ifndef CKOM_DECLSPEC
#define CKOM_DECLSPEC
#endif

namespace Ckom
{
struct CKOM_DECLSPEC vec2
{
    vec2();
    vec2(const vec2&) = default;
    vec2(float x_, float y_);
    float length() const;
    float x = 0.f;
    float y = 0.f;
};

CKOM_DECLSPEC bool operator==(const vec2& l, const vec2& r);
CKOM_DECLSPEC vec2 operator+(const vec2& left, const vec2& right);
CKOM_DECLSPEC vec2 operator*(const vec2& v, const float& fl);
CKOM_DECLSPEC vec2 operator+(const vec2& v, const float& fl);

struct CKOM_DECLSPEC mat2x3
{
    mat2x3();
    static mat2x3 indentiry();
    static mat2x3 scale(float scale);
    static mat2x3 scale(float sx, float sy /*, float sz, float sw*/);
    static mat2x3 rotation(float thetha);
    static mat2x3 moove(const vec2& delta);
    vec2          colon0;
    vec2          colon1;
    vec2          delta;
};

CKOM_DECLSPEC vec2   operator*(const vec2& vec, const mat2x3& matr);
CKOM_DECLSPEC mat2x3 operator*(const mat2x3& mLeft, const mat2x3& mRight);

struct CKOM_DECLSPEC mouse_state
{
    float x = 0.f; // mouse x
    float y = 0.f; // mouse y
    float z = 0.f; // mouse scroll
};

enum class Event
{
    left_pressed,
    left_released,
    left_mouse_pressed,
    left_mouse_released,
    right_pressed,
    right_released,
    right_mouse_pressed,
    right_mouse_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button_triangle_pressed,
    button_triangle_released,
    button_square_pressed,
    button_square_released,
    button_cross_pressed,
    button_cross_released,
    button_circle_pressed,
    button_circle_released,
    space_btn_pressed,
    space_btn_released,
    mouse_move,

    left_arrow_pressed,
    left_arrow_released,
    up_arrow_pressed,
    up_arrow_released,
    right_arrow_pressed,
    right_arrow_released,
    down_arrow_pressed,
    down_arrow_released,

    turn_off
};

CKOM_DECLSPEC std::ostream& operator<<(std::ostream& mStream,
                                       const Event   event);

enum class Keys
{
    left,
    right,
    up,
    down,
    select,
    start,
    bCross,
    bSquere,
    bTriangle,
    bCircle,
    left_a,
    up_a,
    right_a,
    down_a,
    space
};

class Engine;

CKOM_DECLSPEC Engine* create_engine();
CKOM_DECLSPEC void    destroy_engine(Engine* eng);

class CKOM_DECLSPEC Color
{
public:
    Color() = default;
    explicit Color(std::uint32_t rgba_);
    Color(float r, float g, float b, float a);

    float get_r() const;
    float get_g() const;
    float get_b() const;
    float get_a() const;

    void set_r(const float r);
    void set_g(const float g);
    void set_b(const float b);
    void set_a(const float a);

private:
    std::uint32_t rgba = 0;
};

struct CKOM_DECLSPEC Vertex_p
{
    vec2 p;
};

struct CKOM_DECLSPEC Vertex_pc
{
    vec2  p;
    Color c;
};

struct CKOM_DECLSPEC Vertex_pct
{
    vec2  p;
    vec2  tx;
    Color c;
};

struct CKOM_DECLSPEC Triangle_p
{
    Triangle_p();
    Vertex_p v[3];
};

struct CKOM_DECLSPEC Triangle_pc
{
    Triangle_pc();
    Vertex_pc v[3];
};

struct CKOM_DECLSPEC Triangle_pct
{
    Triangle_pct();
    Vertex_pct v[3];
};

CKOM_DECLSPEC std::istream& operator>>(std::istream& is, vec2& vec);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, mat2x3& matrix);

CKOM_DECLSPEC std::istream& operator>>(std::istream& is, Vertex_p& ver);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, Triangle_p& tr);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, Vertex_pc& ver);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, Triangle_pc& tr);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, Vertex_pct& ver);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, Triangle_pct& tr);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, vec2& pos);
CKOM_DECLSPEC std::istream& operator>>(std::istream& is, Color& cl);
// std::istream& operator>>(std::istream& is, Texture_position& txp);

class CKOM_DECLSPEC Texture
{
public:
    virtual ~Texture() {}
    virtual void          bind() const       = 0;
    virtual std::uint32_t get_width() const  = 0;
    virtual std::uint32_t get_height() const = 0;
    virtual std::string   get_name() const   = 0;
};

class CKOM_DECLSPEC Vertex_buffer
{
public:
    virtual ~Vertex_buffer() {}
    //    virtual const Vertex_pct* data() const = 0;
    virtual void bind() const = 0;
    // count of vertex
    virtual size_t size() const = 0;
};

class CKOM_DECLSPEC Index_buffer
{
public:
    virtual ~Index_buffer() {}
    virtual void          bind() const = 0;
    virtual std::uint32_t size() const = 0;
};

class CKOM_DECLSPEC Sound_buffer
{
public:
    enum class properties
    {
        once,
        looped
    };

    virtual ~Sound_buffer();
    virtual void play(const properties) = 0;
    virtual void setVolume(float value) = 0;
};

class CKOM_DECLSPEC Engine
{
public:
    virtual ~Engine();
    virtual std::string initialize(std::string_view conf) = 0;
    virtual Ckom::vec2  screen_size() const               = 0;

    virtual float get_time()              = 0;
    virtual bool  read_event(Event& ev)   = 0;
    virtual bool  is_key_down(const Keys) = 0;
    virtual vec2  mouse_pos()             = 0;

    virtual Texture* create_texture(std::string_view path)      = 0;
    virtual Texture* create_texture_rgba32(const void*  pixels,
                                           const size_t width,
                                           const size_t height) = 0;
    virtual void     destroy_texture(Texture* texture)          = 0;

    // vertex buff
    virtual Vertex_buffer* create_vertex_buffer(const Triangle_pct*,
                                                std::size_t)          = 0;
    virtual Vertex_buffer* create_vertex_buffer(const Vertex_pct*,
                                                std::size_t)          = 0;
    virtual void           destroy_vertex_buffer(Vertex_buffer* buff) = 0;

    // index buff
    virtual Index_buffer* create_index_buffer(const std::uint16_t*,
                                              std::size_t)    = 0;
    virtual void          destroy_index_buffer(Index_buffer*) = 0;

    virtual Sound_buffer* create_sound_buffer(const std::string_view path) = 0;
    virtual void          destroy_sound_buffer(Sound_buffer*)              = 0;

    virtual void render(const Triangle_p& tr, const Color& col)        = 0;
    virtual void render(const Triangle_pc& tr)                         = 0;
    virtual void render(const Triangle_pct& tr, Texture* tx)           = 0;
    virtual void render(const Triangle_pct&, Texture*, const mat2x3&)  = 0;
    virtual void render(const Vertex_buffer&, Texture*, const mat2x3&) = 0;
    virtual void render(const Vertex_buffer* buff, const Index_buffer* indexes,
                        const Texture*       tex,
                        const std::uint16_t* start_vertex_indx,
                        size_t               num_vertexes)                           = 0;

    virtual void swap_buffers() = 0;
    virtual void uninitialize() = 0;
};

} // namespace Ckom
