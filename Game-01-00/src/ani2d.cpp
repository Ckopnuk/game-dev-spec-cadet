#include "ani2d.h"

Ani2d::Ani2d() {}

Ani2d::~Ani2d() {}

bool Ani2d::draw_once(Ckom::Engine& eng, float delta_time)
{
    if (sprites_.empty())
    {
        return false;
    }

    current_time_ += delta_time;

    float  one_frame_data = 1.f / fps_;
    size_t how_many_frames_from_start =
        static_cast<size_t>(current_time_ / one_frame_data);

    size_t current_frame_index =
        how_many_frames_from_start /*% sprites_.size();*/;

    if (current_frame_index >= sprites_.size())
    {
        //        current_time_       = 0;
        current_frame_index = 0;
        return false;
    }
    else
    {
        Sprite& spr = sprites_.at(current_frame_index);

        spr.draw(eng);
        return true;
    }
}

void Ani2d::draw_elepsed(Ckom::Engine& eng, float delta_time)
{
    if (sprites_.empty())
    {
        return;
    }

    current_time_ += delta_time;

    float  one_frame_data = 1.f / fps_;
    size_t how_many_frames_from_start =
        static_cast<size_t>(current_time_ / one_frame_data);

    size_t current_frame_index = how_many_frames_from_start % sprites_.size();

    Sprite& spr = sprites_.at(current_frame_index);

    spr.draw(eng);
}
