FROM fedora:33

RUN yes | dnf update &&\
    yes | dnf install make &&\
    yes | dnf install cmake &&\
    yes | dnf install ninja-build &&\
    yes | dnf install g++ &&\
    yes | sudo dnf install SDL2 &&\
    yes | sudo dnf install SDL2.x86_64 &&\
    yes | sudo dnf install SDL2-static.i686 &&\
    yes | sudo dnf install SDL2-static.x86_64 &&\
    yes | sudo dnf install SDL2.i686 &&\
    yes | sudo dnf install SDL2-devel.i686 &&\
    yes | sudo dnf install SDL2_image.x86_64 &&\
    yes | sudo dnf install SDL2-devel.x86_64

LABEL version="v2"\
      maintainer="6316777@ro.ru"

ENV CMAKE_GENERATOR "Ninja"
