#pragma once

#include <iosfwd>
#include <string>
#include <string_view>

namespace Ckom
{
struct vec2
{
    vec2();
    vec2(float x_, float y_ /*float z_, float w_*/);
    float x = 0.f;
    float y = 0.f;
    //    float z = 0.f;
    //    float w = 0.f;
};

vec2 operator+(const vec2& left, const vec2& right);
vec2 operator*(const vec2& v, const float& fl);
vec2 operator+(const vec2& v, const float& fl);

struct mat2x3
{
    mat2x3();
    static mat2x3 indentiry();
    static mat2x3 scale(float scale);
    static mat2x3 scale(float sx, float sy /*, float sz, float sw*/);
    static mat2x3 rotation(float thetha);
    static mat2x3 moove(const vec2& delta);
    vec2          colon0;
    vec2          colon1;
    vec2          delta;
};

vec2   operator*(const vec2& vec, const mat2x3& matr);
mat2x3 operator*(const mat2x3& mLeft, const mat2x3& mRight);

struct mouse_state
{
    float x = 0.f; // mouse x
    float y = 0.f; // mouse y
    float z = 0.f; // mouse scroll
};

enum class Event
{
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button_triangle_pressed,
    button_triangle_released,
    button_square_pressed,
    button_square_released,
    button_cross_pressed,
    button_cross_released,
    button_circle_pressed,
    button_circle_released,
    mouse_move,
    turn_off
};

std::ostream& operator<<(std::ostream& mStream, const Event event);

enum class Keys
{
    left,
    right,
    up,
    down,
    select,
    start,
    bCross,
    bSquere,
    bTriangle,
    bCircle
};

class Engine;

Engine* create_engine();
void    destroy_engine(Engine* eng);

class Color
{
public:
    Color() = default;
    explicit Color(std::uint32_t rgba_);
    Color(float r, float g, float b, float a);

    float get_r() const;
    float get_g() const;
    float get_b() const;
    float get_a() const;

    void set_r(const float r);
    void set_g(const float g);
    void set_b(const float b);
    void set_a(const float a);

private:
    std::uint32_t rgba = 0;
};

// struct Texture_position {
//  float u = 0.f;
//  float v = 0.f;
//};

struct Vertex_p
{
    vec2 p;
};

struct Vertex_pc
{
    vec2  p;
    Color c;
};

struct Vertex_pct
{
    vec2  p;
    vec2  tx;
    Color c;
};

struct Triangle_p
{
    Triangle_p();
    Vertex_p v[3];
};

struct Triangle_pc
{
    Triangle_pc();
    Vertex_pc v[3];
};

struct Triangle_pct
{
    Triangle_pct();
    Vertex_pct v[3];
};

std::istream& operator>>(std::istream& is, vec2& vec);
std::istream& operator>>(std::istream& is, mat2x3& matrix);

std::istream& operator>>(std::istream& is, Vertex_p& ver);
std::istream& operator>>(std::istream& is, Triangle_p& tr);
std::istream& operator>>(std::istream& is, Vertex_pc& ver);
std::istream& operator>>(std::istream& is, Triangle_pc& tr);
std::istream& operator>>(std::istream& is, Vertex_pct& ver);
std::istream& operator>>(std::istream& is, Triangle_pct& tr);
std::istream& operator>>(std::istream& is, vec2& pos);
std::istream& operator>>(std::istream& is, Color& cl);
// std::istream& operator>>(std::istream& is, Texture_position& txp);

class Texture
{
public:
    virtual ~Texture() {}
    virtual void          bind() const       = 0;
    virtual std::uint32_t get_width() const  = 0;
    virtual std::uint32_t get_height() const = 0;
};

class Vertex_buffer
{
public:
    virtual ~Vertex_buffer() {}
    virtual const Vertex_pct* data() const = 0;
    virtual void              bind() const = 0;
    // count of vertex
    virtual size_t size() const = 0;
};

class Index_buffer
{
public:
    virtual ~Index_buffer() {}
    virtual void          bind() const = 0;
    virtual std::uint32_t size() const = 0;
};

class Engine
{
public:
    virtual ~Engine();
    virtual std::string initialize(std::string_view conf) = 0;

    virtual bool  read_event(Event& ev)   = 0;
    virtual bool  is_key_down(const Keys) = 0;
    virtual float get_time()              = 0;

    virtual Texture* create_texture(std::string_view path)      = 0;
    virtual Texture* create_texture_rgba32(const void*  pixels,
                                           const size_t width,
                                           const size_t height) = 0;
    virtual void     destroy_texture(Texture* texture)          = 0;

    virtual Vertex_buffer* create_vertex_buffer(const Triangle_pct*,
                                                std::size_t)          = 0;
    virtual Vertex_buffer* create_vertex_buffer(const Vertex_pct*,
                                                std::size_t)          = 0;
    virtual void           destroy_vertex_buffer(Vertex_buffer* buff) = 0;

    virtual Index_buffer* create_index_buffer(const std::uint16_t*,
                                              std::size_t)    = 0;
    virtual void          destroy_index_buffer(Index_buffer*) = 0;

    virtual void render(const Triangle_p& tr, const Color& col)        = 0;
    virtual void render(const Triangle_pc& tr)                         = 0;
    virtual void render(const Triangle_pct& tr, Texture* tx)           = 0;
    virtual void render(const Triangle_pct&, Texture*, const mat2x3&)  = 0;
    virtual void render(const Vertex_buffer&, Texture*, const mat2x3&) = 0;
    virtual void render(const Vertex_buffer* buff, const Index_buffer* indexes,
                        const Texture*       tex,
                        const std::uint16_t* start_vertex_indx,
                        size_t               num_vertexes)                           = 0;

    virtual void uninitialize() = 0;

    virtual void swap_buffers() = 0;
};

} // namespace Ckom
