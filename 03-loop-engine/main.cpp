#include "engine.h"

#include <memory>

int main()
{
    std::unique_ptr<CkOm::Engine, void (*)(CkOm::Engine*)> engine(CkOm::create_engine(), CkOm::destroy_engine);

    std::string err = engine->initialize("");
    if (!err.empty()) {
        std::cerr << err << std::endl;
        return EXIT_FAILURE;
    }

    bool continueLoop = true;
    while (continueLoop) {
        CkOm::Event event;

        while (engine->readInput(event)) {
            std::cout << event << std::endl;
            switch (event) {
            case CkOm::Event::turn_off:
                continueLoop = false;
                break;
            default:
                break;
            }
        }
    }
    engine->uninitialize();
    return EXIT_SUCCESS;
}
