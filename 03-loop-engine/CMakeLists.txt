cmake_minimum_required(VERSION 3.17)
project(loop_Engine_SDL)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

file(GLOB CPPsource "*.cpp")
file(GLOB Headers "*.h")

add_executable(${PROJECT_NAME} ${CPPsource} ${Headers})

find_package(sdl2 REQUIRED)
target_include_directories(${PROJECT_NAME} PRIVATE ${SDL2_INCLUDE_DIRS})
find_library(SDL2_LIB2 libSDL2.a)

target_link_libraries(${PROJECT_NAME} PRIVATE SDL2::SDL2-static SDL2::SDL2main)
