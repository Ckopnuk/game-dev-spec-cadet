#pragma once

#include <iosfwd>
#include <string_view>

#ifndef CKOM_DECLSPEC
#define CKOM_DECLSPEC
#endif

namespace Ckom {

enum class Event
{
  left_pressed,
  left_released,
  right_pressed,
  right_released,
  up_pressed,
  up_released,
  down_pressed,
  down_released,
  select_pressed,
  select_released,
  start_pressed,
  start_released,
  button_triangle_pressed,
  button_triangle_released,
  button_square_pressed,
  button_square_released,
  button_cross_pressed,
  button_cross_released,
  button_circle_pressed,
  button_circle_released,
  turn_off
};

CKOM_DECLSPEC std::ostream&
operator<<(std::ostream& mStream, const Event event);

class Engine;

CKOM_DECLSPEC Engine*
create_engine();
CKOM_DECLSPEC void
destroy_engine(Engine* eng);

struct CKOM_DECLSPEC Vertex
{
  float x = 0.f;
  float y = 0.f;
  float z = 0.f;
};

struct CKOM_DECLSPEC Triangle
{
  Triangle()
  {
    v[0] = Vertex();
    v[1] = Vertex();
    v[2] = Vertex();
  }
  Vertex v[3];
};

CKOM_DECLSPEC std::istream&
operator>>(std::istream& is, Vertex& ver);
CKOM_DECLSPEC std::istream&
operator>>(std::istream& is, Triangle& tr);

class CKOM_DECLSPEC Engine
{
public:
  virtual ~Engine();
  virtual std::string initialize(std::string_view conf) = 0;
  virtual bool readInput(Event& ev) = 0;
  virtual void uninitialize() = 0;
  virtual void renderTriangle(const Triangle&) = 0;
  virtual void swapBuffer() = 0;
};

} // namespace CkOm
