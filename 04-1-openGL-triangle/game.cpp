#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.h"

int
main(int, char*[])
{
  std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
    Ckom::create_engine(), Ckom::destroy_engine);

  const std::string error = engine->initialize("");
  if (!error.empty()) {
    std::cerr << error << std::endl;
    return EXIT_FAILURE;
  }

  bool continueLoop = true;
  while (continueLoop) {
    Ckom::Event event;

    while (engine->readInput(event)) {

      std::cout << event << std::endl;
      switch (event) {
        case Ckom::Event::turn_off:
          continueLoop = false;
          break;
        default:
          break;
      }
    }

    std::ifstream file("vertexes.txt");
    assert(!!file);

    Ckom::Triangle tr;
    file >> tr;
    engine->renderTriangle(tr);

    file >> tr;
    engine->renderTriangle(tr);

    engine->swapBuffer();
  }

  engine->uninitialize();

  return EXIT_SUCCESS;
}
