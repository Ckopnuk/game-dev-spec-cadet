#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <numbers>

#include <SDL.h>

constexpr int32_t AUDIO_FORMAT = AUDIO_S16LSB;

static void audio_callback(void* userdata, uint8_t* stream, int len);

#pragma pack(push, 1)
struct Audio_buffer
{
    uint8_t* start       = nullptr;
    size_t   size        = 0;
    size_t   current_pos = 0;

    struct
    {
        size_t frequncy = 0;
        double time     = 0.0;
        bool   use_note = false;
    } note;
};
#pragma pack(pop)

std::ostream& operator<<(std::ostream& out, const SDL_AudioSpec& spec);

static auto     start         = std::chrono::high_resolution_clock::now();
static auto     finish        = start;
static uint32_t default_delay = 0;

int main(int /*argc*/, char* /*argv*/[])
{
    using std::endl;
    std::clog << "start sdl init" << std::endl;

    if (SDL_Init(SDL_INIT_AUDIO) < 0)
    {
        std::cerr << "Error: can't init audio: " << SDL_GetError();
        return EXIT_FAILURE;
    }

    const char* file_name = "highlands.wav";

    std::clog << "read file: " << file_name << endl;
    SDL_RWops* file = SDL_RWFromFile(file_name, "rb");
    if (file == nullptr)
    {
        std::cerr << "Error can't open file: " << file_name << endl;
        return EXIT_FAILURE;
    }

    SDL_AudioSpec audio_spec_from_file{};
    const int32_t auto_delete_file            = 1;
    uint8_t*      sample_buffer_from_file     = nullptr;
    uint32_t      sample_buffer_len_from_file = 0;

    std::clog << "loading sample buffer from file: " << file_name << endl;

    SDL_AudioSpec* audio_spec =
        SDL_LoadWAV_RW(file, auto_delete_file, &audio_spec_from_file,
                       &sample_buffer_from_file, &sample_buffer_len_from_file);

    if (audio_spec == nullptr)
    {
        std::cerr << "Error: can't parse and load samples from file\n";
        return EXIT_FAILURE;
    }

    std::clog << "loaded file audio spec:\n" << audio_spec_from_file << endl;

    // clang-format off
    Audio_buffer loaded_audio_buff
    {
        .start = sample_buffer_from_file,
        .size = sample_buffer_len_from_file,
        .current_pos = 0,
        .note = {
            .frequncy = 0,
            .time = 0,
            .use_note = false
        }
    };
    // clang-format on

    std::clog << "audio buffer from file size: " << sample_buffer_len_from_file
              << " B (" << sample_buffer_len_from_file / double(1024 * 1024)
              << ") Mb" << endl;

    const char*   device_name       = nullptr;
    const int32_t is_capture_device = 0;
    SDL_AudioSpec disired{ .freq     = 48000,
                           .format   = AUDIO_FORMAT,
                           .channels = 2,
                           .silence  = 0,
                           .samples  = 4096,
                           .padding  = 0,
                           .size     = 0,
                           .callback = audio_callback,
                           .userdata = &loaded_audio_buff };

    std::clog << "prepere disired audio specs for output device:\n"
              << disired << endl;

    SDL_AudioSpec retuned{};

    const int32_t allow_changes = 0;

    SDL_AudioDeviceID audio_device_ID = SDL_OpenAudioDevice(
        device_name, is_capture_device, &disired, &retuned, allow_changes);
    if (audio_device_ID == 0)
    {
        std::cerr << "Error: failed to open audio device: " << SDL_GetError()
                  << endl;
        return EXIT_FAILURE;
    }

    std::clog << "returned audio spec for output device:\n" << retuned << endl;

    if (disired.format != retuned.format ||
        disired.channels != retuned.channels || disired.freq != retuned.freq)
    {
        std::cerr << "Error: disired != returned audio device settings!";
        return EXIT_FAILURE;
    }

    SDL_PauseAudioDevice(audio_device_ID, SDL_FALSE);

    std::clog << "unpause audio device (start audio thread)" << endl;

    bool continue_loop = true;
    while (continue_loop)
    {
        std::clog << "1. stop and exit\n"
                  << "2. print current music position\n"
                  << "3. print music time (length)\n"
                  << "4. print device buffer play length\n"
                  << "5. experimental device buffer play length(time between "
                     "callbacks)\n"
                  << "6. set default delay for audio callback(current val: "
                  << default_delay << " ms)\n"
                  << "7. play note(current val: "
                  << loaded_audio_buff.note.frequncy << ")\n"
                  << ">" << std::flush;

        int choise = 0;
        std::cin >> choise;
        switch (choise)
        {
            case 1:
                continue_loop = false;
                break;
            case 2:
                std::clog << "current music pos: "
                          << loaded_audio_buff.current_pos /
                                 double(loaded_audio_buff.size) * 100
                          << " %" << endl;
                break;
            case 3:
            {
                size_t format_size =
                    audio_spec_from_file.format == AUDIO_FORMAT ? 2 : 0;

                double time_in_minutes = double(loaded_audio_buff.size) /
                                         audio_spec_from_file.channels /
                                         format_size /
                                         audio_spec_from_file.freq / 60;

                double minutes;
                double rest_minute = modf(time_in_minutes, &minutes);
                double seconds = rest_minute * 60; // 60 seconds in one minute
                std::clog << "music length: " << time_in_minutes
                          << " minutes or (" << minutes << ":" << seconds << ")"
                          << endl;
                break;
            }
            case 4:
            {
                std::clog << "device buffer play length: "
                          << retuned.samples / double(retuned.freq)
                          << " seconds" << endl;
                break;
            }
            case 5:
            {
                double elepsed_seconds =
                    std::chrono::duration_cast<std::chrono::duration<double>>(
                        finish - start)
                        .count();
                std::clog << "Time between last two audio_callbacks: "
                          << elepsed_seconds << " seconds" << endl;
                break;
            }
            case 6:
            {
                std::clog << "input delay in milliseconds:>" << std::flush;
                std::cin >> default_delay;
                break;
            }
            case 7:
            {
                std::clog << "inpute note freq: " << std::flush;
                std::cin >> loaded_audio_buff.note.frequncy;
            }
            break;
            default:
                break;
        }
    }

    std::clog << "pause audio devise (stop audio thread)" << endl;
    SDL_PauseAudioDevice(audio_device_ID, SDL_TRUE);

    std::clog << "close audio device" << endl;

    SDL_CloseAudioDevice(audio_device_ID);

    SDL_FreeWAV(loaded_audio_buff.start);

    SDL_Quit();

    return EXIT_SUCCESS;
}

static void audio_callback(void* userdata, uint8_t* stream, int len)
{
    static bool first_time = true;
    if (first_time)
    {
        std::clog << "start audio callback!" << std::endl;
        first_time = false;
    }

    SDL_Delay(default_delay);

    start  = finish;
    finish = std::chrono::high_resolution_clock::now();

    size_t stream_len = static_cast<size_t>(len);

    std::memset(stream, 0, static_cast<size_t>(len));

    Audio_buffer* audio_buff_data = reinterpret_cast<Audio_buffer*>(userdata);
    assert(audio_buff_data != nullptr);

    const double pi = std::numbers::pi_v<double>;

    if (audio_buff_data->note.frequncy != 0)
    {
        size_t num_samples = stream_len / 2 / 2;
        double dt          = 1.0 / 48000.0;

        int16_t* output = reinterpret_cast<int16_t*>(stream);

        for (size_t sample = 0; sample < num_samples; ++sample)
        {
            double omega_t = audio_buff_data->note.time * 2.0 * pi *
                             audio_buff_data->note.frequncy;
            double curr_sample =
                std::numeric_limits<int16_t>::max() * sin(omega_t);
            int16_t curr_val = static_cast<int16_t>(curr_sample);

            *output = curr_val;
            output++;
            *output = curr_val;
            output++;

            audio_buff_data->note.time += dt;
        }
    }
    else
    {
        while (stream_len > 0)
        {
            // copy data from loader buffer into output stream
            const uint8_t* current_sound_pos =
                audio_buff_data->start + audio_buff_data->current_pos;
            const size_t left_in_buffer =
                audio_buff_data->size - audio_buff_data->current_pos;

            if (left_in_buffer > stream_len)
            {
                SDL_MixAudioFormat(stream, current_sound_pos, AUDIO_FORMAT, len,
                                   128);
                audio_buff_data->current_pos += stream_len;
                break;
            }
            else
            {
                // first copy rest from buffer and repeat sounds from beginning
                SDL_MixAudioFormat(stream, current_sound_pos, AUDIO_FORMAT,
                                   left_in_buffer, 128);
                // start buffer from begining
                audio_buff_data->current_pos = 0;
                stream_len -= left_in_buffer;
            }
        }
    }
}

std::ostream& operator<<(std::ostream& out, const SDL_AudioSpec& spec)
{
    out << "\tFreq: " << spec.freq << "\n"
        << "\tFormat: " << std::hex << spec.format << '\n'
        << "\tChannels: " << std::dec << int(spec.channels) << '\n'
        << "\tSilence: " << int(spec.silence) << '\n'
        << "\tSamples: " << spec.samples << '\n'
        << "\tSize: " << spec.size << '\n'
        << "\tCallback: " << reinterpret_cast<const void*>(spec.callback)
        << '\n'
        << "\tUserdata: " << spec.userdata;
    return out;
}
