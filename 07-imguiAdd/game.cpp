#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <numbers>
#include <string_view>

#include "engine.h"

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
        Ckom::create_engine(), Ckom::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    Ckom::Texture* textureBug = engine->create_texture("bug.png");
    if (nullptr == textureBug)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    Ckom::Texture* fon_texture = engine->create_texture("earth.png");
    if (nullptr == fon_texture)
    {
        std::cerr << "failed load earth texture!\n";
        return EXIT_FAILURE;
    }

    Ckom::Vertex_buffer* vertex_buf_b = nullptr;
    Ckom::Vertex_buffer* vert_buf_fon = nullptr;

    std::ifstream file("vert_tex_color.txt");
    if (!file)
    {
        std::cerr << "Can't load vert_tex_color.txt" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::array<Ckom::Triangle_pct, 2> triangles;
        file >> triangles[0] >> triangles[1];
        vertex_buf_b =
            engine->create_vertex_buffer(&triangles[0], triangles.size());
        if (vertex_buf_b == 0)
        {
            std::cerr << "can't create bug vertex bufer";
            return EXIT_FAILURE;
        }
    }
    file.close();
    file.open("vert_pos_color.txt");

    if (!file)
    {
        std::cerr << "Can't load vert_pos_color.txt!" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::array<Ckom::Triangle_pct, 2> trian;
        file >> trian[0] >> trian[1];
        vert_buf_fon = engine->create_vertex_buffer(&trian[0], trian.size());
        if (vert_buf_fon == 0)
        {
            std::cerr << "can't create fon vertex bufer";
            return EXIT_FAILURE;
        }
    }

    bool continue_loop = true;

    Ckom::vec2  current_bug_pos(0.f, 0.f);
    float       current_bug_dir(0.f);
    const float pi    = std::numbers::pi_v<float>;
    const float speed = 0.05f;

    while (continue_loop)
    {
        Ckom::Event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continue_loop = false;
                    break;
                default:

                    break;
            }
        }

        if (engine->is_key_down(Ckom::Keys::left))
        {
            current_bug_dir += pi * 0.02f;
        }

        if (engine->is_key_down(Ckom::Keys::up))
        {
            Ckom::vec2 res;
            current_bug_pos.x -= speed * -sin(current_bug_dir);
            current_bug_pos.y -= speed * cos(current_bug_dir);
        }

        if (engine->is_key_down(Ckom::Keys::right))
        {
            current_bug_dir -= pi * 0.02f;
        }

        if (engine->is_key_down(Ckom::Keys::down))
        {
            current_bug_pos.x += speed * -sin(current_bug_dir);
            current_bug_pos.y += speed * cos(current_bug_dir);
        }

        //        std::cout << "X: " << current_tank_pos.x << std::endl;

        Ckom::mat2x3 move   = Ckom::mat2x3::moove(current_bug_pos);
        Ckom::mat2x3 aspect = Ckom::mat2x3::scale(0.2, 0.2 * (640.f / 480.f));
        Ckom::mat2x3 rot    = Ckom::mat2x3::rotation(current_bug_dir);
        Ckom::mat2x3 m      = rot * move * aspect;
        //        Ckom::mat2x3 m = aspect * rot * move;

        Ckom::mat2x3 scale_f = Ckom::mat2x3::scale(0.8, 0.8 * (640.f / 480.f));
        Ckom::mat2x3 move_f  = Ckom::mat2x3::moove(Ckom::vec2(0.f, 0.f));
        Ckom::mat2x3 matr    = move_f * scale_f;
        engine->render(*vert_buf_fon, fon_texture, matr);
        engine->render(*vertex_buf_b, textureBug, m);

        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}

// int main(int /*argc*/, char* /*argv*/[])
//{
//    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
//        Ckom::create_engine(), Ckom::destroy_engine);

//    const std::string error = engine->initialize("");
//    if (!error.empty())
//    {
//        std::cerr << error << std::endl;
//        return EXIT_FAILURE;
//    }

//    Ckom::Texture* texture = engine->create_texture("tank.png");
//    if (nullptr == texture)
//    {
//        std::cerr << "failed load texture\n";
//        return EXIT_FAILURE;
//    }

//    Ckom::Vertex_buffer* vertex_buf = nullptr;

//    std::ifstream file("vert_tex_color.txt");
//    if (!file)
//    {
//        std::cerr << "can't load vert_tex_color.txt\n";
//        return EXIT_FAILURE;
//    }
//    else
//    {
//        std::array<Ckom::Triangle_pct, 2> tr;
//        file >> tr[0] >> tr[1];
//        vertex_buf = engine->create_vertex_buffer(&tr[0], tr.size());
//        if (vertex_buf == nullptr)
//        {
//            std::cerr << "can't create vertex buffer\n";
//            return EXIT_FAILURE;
//        }
//    }

//    bool continue_loop = true;

//    Ckom::vec2  current_tank_pos(0.f, 0.f);
//    float       current_tank_direction(0.f);
//    const float pi = 3.1415926f;

//    while (continue_loop)
//    {
//        Ckom::Event event;

//        while (engine->read_event(event))
//        {
//            std::cout << event << std::endl;
//            switch (event)
//            {
//                case Ckom::Event::turn_off:
//                    continue_loop = false;
//                    break;
//                default:

//                    break;
//            }
//        }

//        if (engine->is_key_down(Ckom::Keys::left))
//        {
//            current_tank_pos.x -= 0.01f;
//            current_tank_direction = pi / 2.f;
//        }
//        else if (engine->is_key_down(Ckom::Keys::right))
//        {
//            current_tank_pos.x += 0.01f;
//            current_tank_direction = -pi / 2.f;
//        }
//        else if (engine->is_key_down(Ckom::Keys::up))
//        {
//            current_tank_pos.y += 0.01f;
//            current_tank_direction = 0.f;
//        }
//        else if (engine->is_key_down(Ckom::Keys::down))
//        {
//            current_tank_pos.y -= 0.01f;
//            current_tank_direction = -pi;
//        }

//        Ckom::mat2x3 move   = Ckom::mat2x3::moove(current_tank_pos);
//        Ckom::mat2x3 aspect = Ckom::mat2x3::scale(1, 800.f / 600.f);
//        Ckom::mat2x3 rot    = Ckom::mat2x3::rotation(current_tank_direction);
//        Ckom::mat2x3 m      = rot * move * aspect;

//        engine->render(*vertex_buf, texture, m);

//        engine->swap_buffers();
//    }

//    engine->uninitialize();

//    return EXIT_SUCCESS;
//}
