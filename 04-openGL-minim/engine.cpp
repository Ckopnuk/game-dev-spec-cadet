#include "engine.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <vector>

#include "include/glad/glad.h"
#include <SDL.h>

#define CKOM_GL_CHECK()                                                        \
  {                                                                            \
    const int err = static_cast<int>(glGetError());                            \
    if (err != GL_NO_ERROR) {                                                  \
      switch (err) {                                                           \
        case GL_INVALID_ENUM:                                                  \
          std::cerr << "GL_INVALID_ENUM" << std::endl;                         \
          break;                                                               \
        case GL_INVALID_VALUE:                                                 \
          std::cerr << "GL_INVALID_VALUE" << std::endl;                        \
          break;                                                               \
        case GL_INVALID_OPERATION:                                             \
          std::cerr << "GL_INVALID_OPERATION" << std::endl;                    \
          break;                                                               \
        case GL_INVALID_FRAMEBUFFER_OPERATION:                                 \
          std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;        \
          break;                                                               \
        case GL_OUT_OF_MEMORY:                                                 \
          std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                        \
          break;                                                               \
      }                                                                        \
      assert(false);                                                           \
    }                                                                          \
  }

namespace Ckom {
static void APIENTRY
callbackOpenGLDebug(GLenum source,
                    GLenum type,
                    GLuint id,
                    GLenum severity,
                    GLsizei length,
                    const GLchar* message,
                    [[maybe_unused]] const void* userParam);

static std::array<std::string_view, 21> eventNames = {
  { "left_pressed",
    "left_released",
    "right_pressed",
    "right_released",
    "up_pressed",
    "up_released",
    "down_pressed",
    "down_released",
    "select_pressed",
    "select_released",
    "start_pressed",
    "start_released",
    "button_triangle_pressed",
    "button_triangle_released",
    "button_square_pressed",
    "button_square_released",
    "button_cross_pressed",
    "button_cross_released",
    "button_circle_pressed",
    "button_circle_released",
    "turn_off" }
};

std::ostream&
operator<<(std::ostream& fout, Event event)
{
  std::uint32_t value = static_cast<std::uint32_t>(event);
  std::uint32_t min = static_cast<std::uint32_t>(Event::left_pressed);
  std::uint32_t max = static_cast<std::uint32_t>(Event::turn_off);
  if (value >= min && value <= max) {
    fout << eventNames[value];
    return fout;
  } else {
    throw std::runtime_error("too big event value");
  }
}

static std::ostream&
operator<<(std::ostream& out, const SDL_version& v)
{
  out << static_cast<int>(v.major) << '.';
  out << static_cast<int>(v.minor) << '.';
  out << static_cast<int>(v.patch);
  return out;
}

std::istream&
operator>>(std::istream& is, Vertex& v)
{
  is >> v.x;
  is >> v.y;
  return is;
}

std::istream&
operator>>(std::istream& is, Triangle& tr)
{
  is >> tr.v[0];
  is >> tr.v[1];
  is >> tr.v[2];
  return is;
}

struct myBind
{
  SDL_KeyCode keyCode;
  std::string_view keyName;
  Event eventPress;
  Event eventReleas;
};

const std::array<myBind, 10> keysArr{
  { { SDLK_w, "up", Event::up_pressed, Event::up_released },
    { SDLK_a, "left", Event::left_pressed, Event::left_released },
    { SDLK_s, "down", Event::down_pressed, Event::down_released },
    { SDLK_d, "right", Event::right_pressed, Event::right_released },
    { SDLK_i,
      "triangle",
      Event::button_triangle_pressed,
      Event::button_triangle_released },
    { SDLK_j,
      "square",
      Event::button_square_pressed,
      Event::button_square_released },
    { SDLK_k,
      "cross",
      Event::button_cross_pressed,
      Event::button_cross_released },
    { SDLK_l,
      "circle",
      Event::button_circle_pressed,
      Event::button_circle_released },
    { SDLK_p, "statr", Event::start_pressed, Event::start_released },
    { SDLK_ESCAPE, "select", Event::select_pressed, Event::select_released } }
};

static bool
checkInput(const SDL_Event& ev, const myBind*& result)
{
  const auto it = std::find_if(
    std::begin(keysArr), std::end(keysArr), [&](const myBind bind) {
      return bind.keyCode == ev.key.keysym.sym;
    });

  if (it != end(keysArr)) {
    result = &(*it);
    return true;
  }
  return false;
}

class EngineImpl final : public Engine
{
public:
  std::string initialize(std::string_view /*conf*/) final
  {
    std::stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version link = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(link.major, link.minor, link.patch)) {
      serr << "Warning: SDL2 compiled and linked version mismatch: " << compiled
           << ' ' << link << std::endl;
    }

    const int initResult = SDL_Init(SDL_INIT_EVERYTHING);
    if (initResult != 0) {
      const char* errMsg = SDL_GetError();
      serr << "Error: failed call SDL_Init: " << errMsg << std::endl;
      return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    window = SDL_CreateWindow("Title",
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              640,
                              480,
                              ::SDL_WINDOW_OPENGL);

    if (window == nullptr) {
      const char* errMsg = SDL_GetError();
      serr << "Error: failed call SDL_CreateWindow: " << errMsg << std::endl;
      SDL_Quit();
      return serr.str();
    }
    int glMajor = 3;
    int glMinor = 2;
    int glContextProfile = SDL_GL_CONTEXT_PROFILE_ES;

    std::string_view platform = SDL_GetPlatform();
    using namespace std::string_view_literals;
    using namespace std;
    auto list = { "Windows"sv, "Apple"sv };
    auto it = find(begin(list), end(list), platform);
    if (it != end(list)) {
      glMinor = 3;
      glContextProfile = SDL_GL_CONTEXT_PROFILE_CORE;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, glContextProfile);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, glMajor);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, glMinor);

    SDL_GLContext sdlContext = SDL_GL_CreateContext(window);
    if (sdlContext == nullptr) {
      SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                          SDL_GL_CONTEXT_PROFILE_CORE);
      sdlContext = SDL_GL_CreateContext(window);
    }
    assert(sdlContext != nullptr);

    int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &glMajor);
    assert(result == 0);

    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &glMinor);
    assert(result == 0);

    if (glMajor != 3 || glMinor != 2) {
      std::clog << "current context opengl version: " << glMajor << ' '
                << glMinor << '\n'
                << "need openg ES version at least: 3.2\n"
                << std::flush;
      throw std::runtime_error("error open gl version!!!");
    }

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
      std::clog << "error initialized glad!!!" << std::endl;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(callbackOpenGLDebug, nullptr);
    glDebugMessageControl(
      GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

    return "";
  }

  bool readInput(Event& event) final
  {
    SDL_Event sdlEvent;

    if (SDL_PollEvent(&sdlEvent)) {
      const myBind* binding = nullptr;
      if (sdlEvent.type == SDL_QUIT) {
        event = Event::turn_off;
        return true;
      } else if (sdlEvent.type == SDL_KEYDOWN) {
        if (checkInput(sdlEvent, binding)) {
          event = binding->eventPress;
          return true;
        }
      } else if (sdlEvent.type == SDL_KEYUP) {
        if (checkInput(sdlEvent, binding)) {
          event = binding->eventReleas;
          return true;
        }
      }
    }
    return false;
  }

  void renderTriangle(const Triangle&) override final
  {
    using namespace std;
    chrono::steady_clock clock;
    auto time_point = clock.now();
    auto timeSSinceEpoch = time_point.time_since_epoch();
    auto ns = timeSSinceEpoch.count();
    auto seconds = ns / 1000'000'000.0f;
    auto currentColor = 0.5f * (std::sin(seconds) + 1.0f);

    glClearColor(0.f, currentColor, 1.f - currentColor, 0.0f);
    CKOM_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT);
    CKOM_GL_CHECK()
  }

  void swapBuffer() override { SDL_GL_SwapWindow(window); }

  void uninitialize() final {}

private:
  SDL_Window* window = nullptr;
};

static bool alreadyExist = false;

Engine*
create_engine()
{
  if (alreadyExist) {
    throw std::runtime_error("is already exist");
  }
  Engine* result = new EngineImpl;
  alreadyExist = true;
  return result;
}

void
destroy_engine(Engine* eng)
{
  if (alreadyExist == false) {
    throw std::runtime_error("engine not created!!!");
  }
  if (nullptr == eng) {
    throw std::runtime_error("eng iss nullptr!!!");
  }
  delete eng;
}

Engine::~Engine() {}

static const char*
sourceToStrv(GLenum source)
{
  switch (source) {
    case GL_DEBUG_SOURCE_API:
      return "API";
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
      return "SHADER_COMPILER";
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
      return "WINDOW_SYSTEM";
    case GL_DEBUG_SOURCE_THIRD_PARTY:
      return "THIRD_PARTY";
    case GL_DEBUG_SOURCE_APPLICATION:
      return "APPLICATION";
    case GL_DEBUG_SOURCE_OTHER:
      return "OTHER";
  }
  return "unknown";
}

static const char*
typeToStrv(GLenum type)
{
  switch (type) {
    case GL_DEBUG_TYPE_ERROR:
      return "ERROR";
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
      return "DEPRECATED_BEHAVIOR";
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
      return "UNDEFINED_BEHAVIOR";
    case GL_DEBUG_TYPE_PERFORMANCE:
      return "PERFORMANCE";
    case GL_DEBUG_TYPE_PORTABILITY:
      return "PORTABILITY";
    case GL_DEBUG_TYPE_MARKER:
      return "MARKER";
    case GL_DEBUG_TYPE_PUSH_GROUP:
      return "PUSH_GROUP";
    case GL_DEBUG_TYPE_POP_GROUP:
      return "POP_GROUP";
    case GL_DEBUG_TYPE_OTHER:
      return "OTHER";
  }
  return "unknown";
}

static const char*
severityToStrv(GLenum severity)
{
  switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
      return "HIGH";
    case GL_DEBUG_SEVERITY_MEDIUM:
      return "MEDIUM";
    case GL_DEBUG_SEVERITY_LOW:
      return "LOW";
    case GL_DEBUG_SEVERITY_NOTIFICATION:
      return "NOTIFICATION";
  }
  return "unknown";
}

static std::array<char, GL_MAX_DEBUG_MESSAGE_LENGTH> localLogBug;

static void APIENTRY
callbackOpenGLDebug(GLenum source,
                    GLenum type,
                    GLuint id,
                    GLenum severity,
                    GLsizei length,
                    const GLchar* message,
                    [[maybe_unused]] const void* userParam)
{
  auto& buff{ localLogBug };
  int numChars = std::snprintf(buff.data(),
                               buff.size(),
                               "%s %s %d %s %.*s\n",
                               sourceToStrv(source),
                               typeToStrv(type),
                               id,
                               severityToStrv(severity),
                               length,
                               message);

  if (numChars > 0) {
    std::cerr.write(buff.data(), numChars);
  }
}

} // end namespace Ckom
