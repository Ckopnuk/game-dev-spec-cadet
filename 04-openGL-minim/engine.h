#pragma once

#include <iosfwd>
#include <string_view>

namespace Ckom {

enum class Event
{
  left_pressed,
  left_released,
  right_pressed,
  right_released,
  up_pressed,
  up_released,
  down_pressed,
  down_released,
  select_pressed,
  select_released,
  start_pressed,
  start_released,
  button_triangle_pressed,
  button_triangle_released,
  button_square_pressed,
  button_square_released,
  button_cross_pressed,
  button_cross_released,
  button_circle_pressed,
  button_circle_released,
  turn_off
};

std::ostream&
operator<<(std::ostream& mStream, const Event event);

class Engine;

Engine*
create_engine();
void
destroy_engine(Engine* eng);

struct Vertex
{
  Vertex()
    : x(0.f)
    , y(0.f)
  {}
  float x;
  float y;
};

struct Triangle
{
  Triangle()
  {
    v[0] = Vertex();
    v[1] = Vertex();
    v[2] = Vertex();
  }
  Vertex v[3];
};

std::istream&
operator>>(std::istream& is, Vertex& ver);
std::istream&
operator>>(std::istream& is, Triangle& tr);

class Engine
{
public:
  virtual ~Engine();
  virtual std::string initialize(std::string_view conf) = 0;
  virtual bool readInput(Event& ev) = 0;
  virtual void uninitialize() = 0;
  virtual void renderTriangle(const Triangle&) = 0;
  virtual void swapBuffer() = 0;
};

} // namespace CkOm
