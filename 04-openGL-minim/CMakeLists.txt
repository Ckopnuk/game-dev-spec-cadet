cmake_minimum_required(VERSION 3.17)

project(04-openGLmin)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED true)

include_directories(${PROJECT_SOURCE_DIR}/glad/include/glad)

#file(GLOB CPPS "/glad")

add_library(engine-04-1 SHARED
    engine.cpp
    engine.h
    include/glad/glad.h
    include/KHR/khrplatform.h
    src/glad.c
    )

find_package(sdl2 REQUIRED)
target_include_directories(engine-04-1 PRIVATE ${SDL2_INCLUDE_DIRS} ${CMAKE_CURRENT_LIST_DIR})
target_link_libraries(engine-04-1 PRIVATE ${SDL2_LIBRARIES})

add_executable(game-00.0 game.cpp)

target_link_libraries(game-00.0 engine-04-1)
