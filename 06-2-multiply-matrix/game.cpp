#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.h"

Ckom::Vertex_p blend(const Ckom::Vertex_p& vl, const Ckom::Vertex_p& vr,
                     const float a)
{
    Ckom::Vertex_p r;
    r.p.x = (1.0f - a) * vl.p.x + a * vr.p.x;
    r.p.y = (1.0f - a) * vl.p.y + a * vr.p.y;
    return r;
}

Ckom::Triangle_p blend(const Ckom::Triangle_p& tl, const Ckom::Triangle_p& tr,
                       const float a)
{
    Ckom::Triangle_p r;
    r.v[0] = blend(tl.v[0], tr.v[0], a);
    r.v[1] = blend(tl.v[1], tr.v[1], a);
    r.v[2] = blend(tl.v[2], tr.v[2], a);
    return r;
}

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
        Ckom::create_engine(), Ckom::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    Ckom::Texture* texture = engine->create_texture("tank.png");
    if (nullptr == texture)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    bool  continue_loop  = true;
    bool  stop_rotate    = false;
    float stop_time      = 0.f;
    int   current_shader = 0;
    while (continue_loop)
    {
        Ckom::Event event;

        while (engine->read_input(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continue_loop = false;
                    break;
                case Ckom::Event::up_pressed:
                    ++current_shader;
                    if (current_shader > 2)
                    {
                        current_shader = 0;
                    }
                    break;
                default:

                    break;
            }
        }

        if (current_shader == 0)
        {
            std::ifstream file("vert_pos.txt");
            assert(!!file);

            Ckom::Triangle_p tr1;
            Ckom::Triangle_p tr2;
            Ckom::Triangle_p tr11;
            Ckom::Triangle_p tr22;

            file >> tr1 >> tr2 >> tr11 >> tr22;

            float        time = engine->get_time();
            Ckom::mat2x3 move =
                Ckom::mat2x3::moove(Ckom::Vect2(std::sin(time), 0.f));

            Ckom::mat2x3 aspect;
            aspect.colon0.x = 1;
            aspect.colon0.y = 0.f;
            aspect.colon1.x = 0.f;
            aspect.colon1.y = 640.f / 480.f;

            Ckom::mat2x3 m =
                Ckom::mat2x3::rotation(std::sin(time)) * move * aspect;

            for (auto& v : tr1.v)
            {
                v.p = v.p * m;
            }

            for (auto& v : tr2.v)
            {
                v.p = v.p * m;
            }

            engine->render(tr1, Ckom::Color(1.f, 0.f, 0.f, 1.f));
            engine->render(tr2, Ckom::Color(0.f, 1.f, 0.f, 1.f));
        }

        if (current_shader == 1)
        {
            std::ifstream file("vert_pos_color.txt");
            assert(!!file);

            Ckom::Triangle_pc tr1;
            Ckom::Triangle_pc tr2;

            file >> tr1 >> tr2;

            engine->render(tr1);
            engine->render(tr2);
        }

        if (current_shader == 2)
        {
            std::ifstream file("vert_tex_color.txt");
            assert(!!file);

            Ckom::Triangle_pct tr1;
            Ckom::Triangle_pct tr2;

            file >> tr1 >> tr2;

            float        time = engine->get_time();
            Ckom::mat2x3 move =
                Ckom::mat2x3::moove(Ckom::Vect2(std::sin(time), 0.f));

            Ckom::mat2x3 aspect;
            aspect.colon0.x = 1;
            aspect.colon0.y = 0.f;
            aspect.colon1.x = 0.f;
            aspect.colon1.y = 640.f / 480.f;

            Ckom::mat2x3 m =
                Ckom::mat2x3::rotation(std::sin(time)) * move * aspect;

            engine->render(tr1, texture, m);
            engine->render(tr2, texture, m);
        }

        engine->swap_buffer();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}
