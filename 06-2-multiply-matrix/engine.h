#pragma once

#include <iosfwd>
#include <string>
#include <string_view>

namespace Ckom
{
struct Vect2
{
    Vect2();
    Vect2(float x_, float y_);
    float x = 0.f;
    float y = 0.f;
};

Vect2 operator+(const Vect2& left, const Vect2& right);

struct mat2x3
{
    mat2x3();
    static mat2x3 indentiry();
    static mat2x3 scale(float scale);
    static mat2x3 rotation(float thetha);
    static mat2x3 moove(const Vect2& delta);
    Vect2          colon0;
    Vect2          colon1;
    Vect2          delta;
};

Vect2   operator*(const Vect2& vec, const mat2x3& matr);
mat2x3 operator*(const mat2x3& mLeft, const mat2x3& mRight);

enum class Event
{
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button_triangle_pressed,
    button_triangle_released,
    button_square_pressed,
    button_square_released,
    button_cross_pressed,
    button_cross_released,
    button_circle_pressed,
    button_circle_released,
    turn_off
};

std::ostream& operator<<(std::ostream& mStream, const Event event);

class Engine;

Engine* create_engine();
void    destroy_engine(Engine* eng);

class Color
{
public:
    Color() = default;
    explicit Color(std::uint32_t rgba_);
    Color(float r, float g, float b, float a);

    float get_r() const;
    float get_g() const;
    float get_b() const;
    float get_a() const;

    void set_r(const float r);
    void set_g(const float g);
    void set_b(const float b);
    void set_a(const float a);

private:
    std::uint32_t rgba = 0;
};

struct Texture_position
{
    float u = 0.f;
    float v = 0.f;
};

struct Vertex_p
{
    Vect2 p;
};

struct Vertex_pc
{
    Vect2  p;
    Color c;
};

struct Vertex_pct
{
    Vect2  p;
    Color c;
    Vect2  tx;
};

struct Triangle_p
{
    Triangle_p();
    Vertex_p v[3];
};

struct Triangle_pc
{
    Triangle_pc();
    Vertex_pc v[3];
};

struct Triangle_pct
{
    Triangle_pct();
    Vertex_pct v[3];
};

std::istream& operator>>(std::istream& is, Vect2& vec);
std::istream& operator>>(std::istream& is, mat2x3& matrix);

std::istream& operator>>(std::istream& is, Vertex_p& ver);
std::istream& operator>>(std::istream& is, Triangle_p& tr);
std::istream& operator>>(std::istream& is, Vertex_pc& ver);
std::istream& operator>>(std::istream& is, Triangle_pc& tr);
std::istream& operator>>(std::istream& is, Vertex_pct& ver);
std::istream& operator>>(std::istream& is, Triangle_pct& tr);
std::istream& operator>>(std::istream& is, Vect2& pos);
std::istream& operator>>(std::istream& is, Color& cl);
// std::istream& operator>>(std::istream& is, Texture_position& txp);

class Texture
{
public:
    virtual ~Texture() {}
    virtual std::uint32_t get_width() const  = 0;
    virtual std::uint32_t get_height() const = 0;
};

class Engine
{
public:
    virtual ~Engine();
    virtual std::string initialize(std::string_view conf) = 0;

    virtual bool  read_input(Event& ev) = 0;
    virtual float get_time()            = 0;

    virtual void     uninitialize()                                       = 0;
    virtual Texture* create_texture(std::string_view path)                = 0;
    virtual void     destroy_texture(Texture* texture)                    = 0;
    virtual void     render(const Triangle_p& tr, const Color& col)       = 0;
    virtual void     render(const Triangle_pc& tr)                        = 0;
    virtual void     render(const Triangle_pct& tr, Texture* tx)          = 0;
    virtual void     render(const Triangle_pct&, Texture*, const mat2x3&) = 0;

    virtual void set_uniform(const char*, float) = 0;
    virtual void swap_buffer()                   = 0;
};

} // namespace Ckom
