cmake_minimum_required(VERSION 3.17)

project(06-2-multi-matrix)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED true)

add_library(engine-05-4 STATIC
    engine.cpp
    engine.h
    picopng.h
    )

find_package(sdl2 REQUIRED)
target_link_libraries(engine-05-4 PRIVATE SDL2::SDL2 SDL2::SDL2main)
target_link_libraries(engine-05-4 PRIVATE SDL2
                                            GL)

add_executable(game-00.4 game.cpp)

target_link_libraries(game-00.4 engine-05-4)
