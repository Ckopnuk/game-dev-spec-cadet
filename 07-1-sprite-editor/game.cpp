#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <numbers>
#include <sstream>
#include <string_view>
#include <vector>

#pragma GCC diagnostic push
// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wall"
#include "src/imgui/imgui.h"
#pragma GCC diagnostic pop

#include "engine.h"
#include "sprite.h"
#include "sprite_reader.h"

#include "ani2d.h"

#include <chrono>

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
        Ckom::create_engine(), Ckom::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    Ckom::Texture* textureBug = engine->create_texture("bug.png");
    if (nullptr == textureBug)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }

    Ckom::Texture* fon_texture = engine->create_texture("earth.png");
    if (nullptr == fon_texture)
    {
        std::cerr << "failed load earth texture!\n";
        return EXIT_FAILURE;
    }

    Ckom::Vertex_buffer* vertex_buf_b = nullptr;
    Ckom::Vertex_buffer* vert_buf_fon = nullptr;

    std::array<Ckom::Triangle_pc, 2> triangles;

    std::ifstream file("vert_tex_color.txt");
    if (!file)
    {
        std::cerr << "Can't load vert_tex_color.txt" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {

        file >> triangles[0] >> triangles[1];
        //        vertex_buf_b =
        //            engine->create_vertex_buffer(&triangles[0],
        //            triangles.size());
        //        if (vertex_buf_b == 0)
        //        {
        //            std::cerr << "can't create bug vertex bufer";
        //            return EXIT_FAILURE;
        //        }
    }
    file.close();
    file.open("vert_pos_color.txt");

    if (!file)
    {
        std::cerr << "Can't load vert_pos_color.txt!" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::array<Ckom::Triangle_pct, 2> trian;
        file >> trian[0] >> trian[1];
        vert_buf_fon = engine->create_vertex_buffer(&trian[0], trian.size());
        if (vert_buf_fon == 0)
        {
            std::cerr << "can't create fon vertex bufer";
            return EXIT_FAILURE;
        }
    }

    bool continue_loop = true;

    Ckom::vec2  current_bug_pos(0.f, 0.f);
    float       current_bug_dir(0.f);
    const float pi    = std::numbers::pi_v<float>;
    const float speed = 0.05f;

    while (continue_loop)
    {
        Ckom::Event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continue_loop = false;
                    break;
                default:

                    break;
            }
        }

        if (engine->is_key_down(Ckom::Keys::left))
        {
            current_bug_dir += pi * 0.02f;
        }

        if (engine->is_key_down(Ckom::Keys::up))
        {
            Ckom::vec2 res;
            current_bug_pos.x += speed * -sin(current_bug_dir);
            current_bug_pos.y += speed * cos(current_bug_dir);
        }

        if (engine->is_key_down(Ckom::Keys::right))
        {
            current_bug_dir -= pi * 0.02f;
        }

        if (engine->is_key_down(Ckom::Keys::down))
        {
            current_bug_pos.x -= speed * -sin(current_bug_dir);
            current_bug_pos.y -= speed * cos(current_bug_dir);
        }

        //        std::cout << "X: " << current_tank_pos.x << std::endl;

        Ckom::mat2x3 move   = Ckom::mat2x3::moove(current_bug_pos);
        Ckom::mat2x3 aspect = Ckom::mat2x3::scale(0.2, 0.2 * (800.f / 600.f));
        Ckom::mat2x3 rot    = Ckom::mat2x3::rotation(current_bug_dir);
        Ckom::mat2x3 m      = rot * move * aspect;
        //        Ckom::mat2x3 m = aspect * rot * move;

        Ckom::mat2x3 scale_f = Ckom::mat2x3::scale(
            0.8, 0.8 * (engine->screen_size().x / engine->screen_size().y));
        Ckom::mat2x3 move_f = Ckom::mat2x3::moove(Ckom::vec2(0.f, 0.f));
        Ckom::mat2x3 matr   = move_f * scale_f;
        //        ImGui::NewFrame();
        engine->render(*vert_buf_fon, fon_texture, matr);
        engine->render(triangles.at(0));
        //        engine->render(*vertex_buf_b, textureBug, m);
        //        ImGui::Render();
        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}

// class timer
//{
// public:
//    timer()
//        : m_beg(clock_t::now())
//    {
//    }

//    void reset() { m_beg = clock_t::now(); }

//    float elapsed() const
//    {
//        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg)
//            .count();
//    }

// private:
//    // Type aliases to make accessing nested type easier
//    using clock_t  = std::chrono::high_resolution_clock;
//    using second_t = std::chrono::duration<float, std::ratio<1>>;

//    std::chrono::time_point<clock_t> m_beg;
//};
