#include "ani2d.h"

Ani2d::Ani2d() {}

void Ani2d::draw(Ckom::Engine& eng, float delta_time)
{
    if (sprites_.empty())
    {
        return;
    }

    current_time_ += delta_time;

    float  one_frame_data = 1.f / fps_;
    size_t how_many_frames_from_start =
        static_cast<size_t>(current_time_ / one_frame_data);

    size_t current_frame_index = how_many_frames_from_start % sprites_.size();

    Sprite& spr = sprites_.at(current_frame_index);

    spr.draw(eng);
}
