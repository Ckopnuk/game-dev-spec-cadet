#include <SDL.h>

#include <algorithm>
#include <array>
#include <iostream>
#include <string_view>

std::ostream& operator<<(std::ostream& mout, SDL_version& ver) {
  mout << static_cast<int>(ver.major) << '.';
  mout << static_cast<int>(ver.minor) << '.';
  mout << static_cast<int>(ver.patch);
  return mout;
};

#pragma pack(push, 4)
struct myBind {
  SDL_KeyCode keyCod;
  std::string_view codeName;
};
#pragma pack(pop)

void checkInput(SDL_Event chEvent) {
  using std::cout;
  using std::endl;

  std::array<myBind, 10> keysArr{{{SDLK_w, "up"},
                                  {SDLK_a, "left"},
                                  {SDLK_s, "down"},
                                  {SDLK_d, "right"},
                                  {SDLK_i, "button_triangle"},
                                  {SDLK_j, "button_square"},
                                  {SDLK_k, "button_cross"},
                                  {SDLK_l, "button_circle"},
                                  {SDLK_p, "button_start"},
                                  {SDLK_ESCAPE, "button_select"}}};

  const auto it = std::find_if(std::begin(keysArr), std::end(keysArr),
                               [&](const myBind& bind) {
                                 return bind.keyCod == chEvent.key.keysym.sym;
                               });
  if (it != std::end(keysArr)) {
    cout << it->codeName << ' ';
    if (chEvent.type == SDL_KEYDOWN) {
      cout << "is pressed" << endl;
    } else {
      cout << "is released" << endl;
    }
  }
}

int main() {
  using std::cout;
  using std::endl;

  SDL_version compiled = {0, 0, 0};
  SDL_version link = {0, 0, 0};

  SDL_GetVersion(&compiled);
  SDL_GetVersion(&link);

  cout << "Copmpiled: " << compiled << endl;
  cout << "Linked: " << link << endl;

  if (SDL_COMPILEDVERSION !=
      SDL_VERSIONNUM(link.major, link.minor, link.patch)) {
    std::cerr << "Warning: compiled and lincked version mismatch: " << compiled
              << "and " << link << endl;
  }

  const int initResult = SDL_Init(SDL_INIT_EVERYTHING);
  if (initResult != 0) {
    const char* errorMsg = SDL_GetError();
    std::cerr << "Error: failed call SDL_Init " << errorMsg << endl;
    return EXIT_FAILURE;
  }

  SDL_Window* const window =
      SDL_CreateWindow("litle", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                       640, 480, ::SDL_WINDOW_OPENGL);

  if (window == nullptr) {
    const char* errorMsg = SDL_GetError();
    std::cerr << "Error: failed create SDL_Window: " << errorMsg << endl;
    return EXIT_FAILURE;
  }

  bool continueLooping = true;
  while (continueLooping) {
    SDL_Event sdlEvent;
    while (SDL_PollEvent(&sdlEvent)) {
      switch (sdlEvent.type) {
        case SDL_KEYDOWN:
          checkInput(sdlEvent);
          break;
        case SDL_KEYUP:
          checkInput(sdlEvent);
          break;
        case SDL_QUIT:
          continueLooping = false;
          break;
        default:
          break;
      }
    }
  }
  return cout.fail();
}
