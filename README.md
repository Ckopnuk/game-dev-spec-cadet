#Game_00-01

Installation
Om requires SDL2 v2.0.12+ to run.

Install the dependencies to build engine on linux.

On Fedora linux
$ git clone https://gitlab.com/Ckopnuk/game-dev-spec-cadet.git
$ sudo dnf install SDL2
$ sudo dnf install SDL2-static

hints on Fedora
To search package
# search by package name
$ dnf search SDL2
# search by file in package
$ dnf provides /usr/lib/libSDL2.so
To list package contents (files list)
$ dnf repoquery -l SDL2
$ rpm -ql SDL2
To show compile/link/version of installed version
$ sdl2-config --version
$ sdl2-config --libs
$ sdl2-config --static-libs

Generate Docker image (for bitbucket pipelines)
read complete example in support/docker{Dockerfile|readme.md}
write Dockerfile
call sudo systemctl start docker
call sudo docker build -t leanid/fedora_latest .
call sudo docker push leanid/fedora_latest

Dockerfile content:

FROM fedora:latest

RUN dnf update -y
RUN dnf upgrade -y
RUN dnf install -y gcc-c++ make cmake mingw64-gcc mingw64-gcc-c++ clang wine git SDL2-devel SDL2-static mingw64-SDL2 mingw64-SDL2-static libstdc++-static glibc-static ninja-build




