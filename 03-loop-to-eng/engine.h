#ifndef ENGINE_H
#define ENGINE_H

#include <SDL2/SDL.h>
#include <algorithm>
#include <array>
#include <exception>
#include <iosfwd>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

namespace CkOm {

enum class Event {
    left_pressed,
    left_released,
    right_pressed,
    right_released,
    up_pressed,
    up_released,
    down_pressed,
    down_released,
    select_pressed,
    select_released,
    start_pressed,
    start_released,
    button_triangle_pressed,
    button_triangle_released,
    button_square_pressed,
    button_square_released,
    button_cross_pressed,
    button_cross_released,
    button_circle_pressed,
    button_circle_released,
    turn_off
};

ENGINE_H std::ostream& operator<<(std::ostream& mStream, const Event event);

class Engine;

ENGINE_H Engine* create_engine();
ENGINE_H void destroy_engine(Engine* eng);

class ENGINE_H Engine {
public:
    virtual ~Engine();
    virtual std::string initialize(std::string_view conf) = 0;
    virtual bool readInput(Event& ev) = 0;
    virtual void uninitialize() = 0;
};

} // namespace CkOm

#endif // ENGINE_H
