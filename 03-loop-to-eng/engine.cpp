#include "engine.h"

namespace CkOm {

static std::array<std::string_view, 21> eventNames = {
    { "left_pressed",
        "left_released",
        "right_pressed",
        "right_released",
        "up_pressed",
        "up_released",
        "down_pressed",
        "down_released",
        "select_pressed",
        "select_released",
        "start_pressed",
        "start_released",
        "button_triangle_pressed",
        "button_triangle_released",
        "button_square_pressed",
        "button_square_released",
        "button_cross_pressed",
        "button_cross_released",
        "button_circle_pressed",
        "button_circle_released",
        "turn_off" }
};

std::ostream&
operator<<(std::ostream& mStream, const Event event)
{
    std::uint32_t value = static_cast<std::uint32_t>(event);
    std::uint32_t min = static_cast<std::uint32_t>(Event::left_pressed);
    std::uint32_t max = static_cast<std::uint32_t>(Event::turn_off);
    if (value >= min && value <= max) {
        mStream << eventNames[value];
        return mStream;
    } else {
        throw std::runtime_error("too big event value");
    }
}

static std::ostream&
operator<<(std::ostream& mout, const SDL_version ver)
{
    mout << static_cast<int>(ver.major) << '.';
    mout << static_cast<int>(ver.minor) << '.';
    mout << static_cast<int>(ver.patch);
    return mout;
}

struct myBind {
    myBind(std::string_view str, Event pres, Event releas)
        : keyName(str)
        , eventPress(pres)
        , eventReleas(releas)
    {
    }
    myBind(SDL_KeyCode keyC, std::string_view str, Event pressed, Event released)
        : keyCode(keyC)
        , keyName(str)
        , eventPress(pressed)
        , eventReleas(released)
    {
    }

    SDL_KeyCode keyCode;
    std::string_view keyName;
    Event eventPress;
    Event eventReleas;
};

struct myConBind : public myBind {
    myConBind(SDL_GameControllerButton bt, std::string_view str, Event pr, Event rl)
        : myBind(str, pr, rl)
        , button(bt)
    {
    }

    SDL_GameControllerButton button;
};

const std::array<myConBind, 10> buttonsArr {
    { { SDL_CONTROLLER_BUTTON_DPAD_UP, "up", Event::up_pressed, Event::up_released },
        { SDL_CONTROLLER_BUTTON_DPAD_DOWN, "down", Event::down_pressed, Event::down_released },
        { SDL_CONTROLLER_BUTTON_DPAD_RIGHT, "right", Event::right_pressed, Event::right_released },
        { SDL_CONTROLLER_BUTTON_DPAD_LEFT, "left", Event::left_pressed, Event::left_released },
        { SDL_CONTROLLER_BUTTON_A, "cross", Event::button_cross_pressed, Event::button_cross_released },
        { SDL_CONTROLLER_BUTTON_B, "circle", Event::button_circle_pressed, Event::button_circle_released },
        { SDL_CONTROLLER_BUTTON_X, "square", Event::button_square_pressed, Event::button_square_released },
        { SDL_CONTROLLER_BUTTON_Y, "triangle", Event::button_triangle_pressed, Event::button_triangle_released },
        { SDL_CONTROLLER_BUTTON_START, "start", Event::start_pressed, Event::start_released },
        { SDL_CONTROLLER_BUTTON_BACK, "select", Event::select_pressed, Event::select_released } }
};

const std::array<myBind, 10> keysArr {
    { { SDLK_w, "up", Event::up_pressed, Event::up_released },
        { SDLK_a, "left", Event::left_pressed, Event::left_released },
        { SDLK_s, "down", Event::down_pressed, Event::down_released },
        { SDLK_d, "right", Event::right_pressed, Event::right_released },
        { SDLK_i,
            "triangle",
            Event::button_triangle_pressed,
            Event::button_triangle_released },
        { SDLK_j,
            "square",
            Event::button_square_pressed,
            Event::button_square_released },
        { SDLK_k,
            "cross",
            Event::button_cross_pressed,
            Event::button_cross_released },
        { SDLK_l,
            "circle",
            Event::button_circle_pressed,
            Event::button_circle_released },
        { SDLK_p, "statr", Event::start_pressed, Event::start_released },
        { SDLK_ESCAPE, "select", Event::select_pressed, Event::select_released } }
};

static bool checkInput(const SDL_Event& ev, const myBind*& result)
{
    const auto it = std::find_if(std::begin(keysArr), std::end(keysArr), [&](const myBind bind) {
        return bind.keyCode == ev.key.keysym.sym;
    });

    if (it != end(keysArr)) {
        result = &(*it);
        return true;
    }
    return false;
}

static bool checkInput(const SDL_Event& ev, const myConBind*& result)
{
    const auto it = std::find_if(std::begin(buttonsArr), std::end(buttonsArr), [&](const myConBind bind) {
        return bind.button == ev.cbutton.button;
    });

    if (it != end(keysArr)) {
        result = &(*it);
        return true;
    }
    return false;
}

class EngineImpl final : public Engine {
public:
    std::string initialize(std::string_view /*conf*/) final
    {
        std::stringstream serr;

        SDL_version compiled = { 0, 0, 0 };
        SDL_version link = { 0, 0, 0 };

        SDL_VERSION(&compiled);
        SDL_GetVersion(&link);

        if (SDL_COMPILEDVERSION != SDL_VERSIONNUM(link.major, link.minor, link.patch)) {
            serr << "Warning: SDL2 compiled and linked version mismatch: " << compiled << ' ' << link << std::endl;
        }

        const int initResult = SDL_Init(SDL_INIT_EVERYTHING);
        if (initResult != 0) {
            const char* errMsg = SDL_GetError();
            serr << "Error: failed call SDL_Init: " << errMsg << std::endl;
            return serr.str();
        }

        SDL_Window* const window = SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED, 640, 480, ::SDL_WINDOW_OPENGL);

        if (window == nullptr) {
            const char* errMsg = SDL_GetError();
            serr << "Error: failed call SDL_CreateWindow: " << errMsg << std::endl;
            SDL_Quit();
            return serr.str();
        }

        SDL_GameController* controller = nullptr;
        for (int i = 0; i < SDL_NumJoysticks(); ++i) {
            controller = SDL_GameControllerOpen(i);
            if (controller) {
                break;
            } else {
                serr << "Could not open gamecontroller " << i << '\n'
                     << SDL_GetError() << std::endl;
            }
        }
        return "";
    }

    bool readInput(Event& event) final
    {
        SDL_Event sdlEvent;

        if (SDL_PollEvent(&sdlEvent)) {
            const myBind* binding = nullptr;
            const myConBind* conBind = nullptr;
            if (sdlEvent.type == SDL_QUIT) {
                event = Event::turn_off;
                return true;
            } else if (sdlEvent.type == SDL_KEYDOWN) {
                if (checkInput(sdlEvent, binding)) {
                    event = binding->eventPress;
                    return true;
                }
            } else if (sdlEvent.type == SDL_KEYUP) {
                if (checkInput(sdlEvent, binding)) {
                    event = binding->eventReleas;
                    return true;
                }
            } else if (sdlEvent.type == SDL_CONTROLLERDEVICEADDED) {
                std::cerr << "Controller added" << std::endl;
            } else if (sdlEvent.type == SDL_CONTROLLERDEVICEREMOVED) {
                std::cerr << "Controlle removed" << std::endl;
            } else if (sdlEvent.type == SDL_CONTROLLERBUTTONDOWN) {
                if (checkInput(sdlEvent, conBind)) {
                    event = conBind->eventPress;
                    return true;
                }
            } else if (sdlEvent.type == SDL_CONTROLLERBUTTONUP) {
                if (checkInput(sdlEvent, conBind)) {
                    event = conBind->eventReleas;
                    return true;
                }
                return true;
            }
        }
        return false;
    }
    void uninitialize() final {}
};

static bool alreadyExist = false;

Engine* create_engine()
{
    if (alreadyExist) {
        throw std::runtime_error("Engine already exist");
    }
    Engine* result = new EngineImpl();
    alreadyExist = true;
    return result;
}

void destroy_engine(Engine* eng)
{
    if (alreadyExist == false) {
        throw std::runtime_error("Engine not created");
    }
    if (eng == nullptr) {
        throw std::runtime_error("Eng is nullptr");
    }
    delete eng;
}

Engine::~Engine() {}

} // namespace CkOm
