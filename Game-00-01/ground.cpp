#include "ground.h"

Ground::Ground(Ckom::Engine* eng)
{
    engine = eng;
    if (engine == nullptr)
    {
        std::cerr << "can't create ground: engine == nullptr!" << std::endl;
    }

    setTexture("/home/ckopnuk/projects/game-dev-spec-cadet/Game-00-01/src/texture/background.png", *engine);
    setVertex_buf("../vert_tex_color.txt", *engine);
}

Ground::~Ground() {}

void Ground::render()
{
    engine->render(*vertex_buf, texture, matrix());
}

Ckom::mat2x3 Ground::matrix()
{
    const float  pi       = std::numbers::pi_v<float>;
    Ckom::mat2x3 scale    = Ckom::mat2x3::scale(0.2, 0.2 * (1024 / 760));
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(pi);
    Ckom::mat2x3 position = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res      = scale * rotation * position;
    return res;
}

float Ground::get_radius() const
{
    return radius;
}

/////////////////////// GROUND_IN_FIRE CLASS //////////////////////

Ground_in_fire::Ground_in_fire(Ckom::Engine* eng)
    : Ground(eng)
{
    if (engine == nullptr)
    {
        std::cerr << "nullptr Engine in ground_in_fire!!!" << std::endl;
    }
    setTexture("texture/Explosion_F.png", *engine);
}

Ground_in_fire::~Ground_in_fire() {}

////////////////////// GROUND_SPACE IN_FULL_SCREEN /////////////////////

Ground_space::Ground_space(Ckom::Engine* eng)
    : Ground(eng)
{
    setVertex_buf("vert_space.txt", *engine);
    setTexture("src/texture/space.jpg", *engine);
}

Ground_space::~Ground_space() {}

Ckom::mat2x3 Ground_space::matrix()
{
    Ckom::mat2x3 scale    = Ckom::mat2x3::scale(1, 1);
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(0);
    Ckom::mat2x3 position = Ckom::mat2x3::moove({ 0.0, 0.0 });
    Ckom::mat2x3 res      = scale * rotation * position;
    return res;
}
