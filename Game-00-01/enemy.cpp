#include "enemy.h"
#include <chrono>
#include <math.h>
#include <numbers>

static std::vector<Bullet>              bullets;
static std::vector<Enemy_from_big_bos*> enemys;

const float pi = std::numbers::pi_v<float>;

Enemy_litle_bug::Enemy_litle_bug(Ckom::Engine& eng)
    : Enemy("src/texture/Ship2.png", "vert_tex_color.txt", eng)
{
    speed  = 0.4;
    radius = 0.03;
    dmg    = 1;
    hp     = 1;
}

Enemy_litle_bug::~Enemy_litle_bug() {}

Ckom::mat2x3 Enemy_litle_bug::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.2, 0.3 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(pi * 0.5);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

///////////////////////// ENEMY ////////////////////////

Enemy::Enemy(std::string_view path_to_texture, std::string_view path_to_vertex,
             Ckom::Engine& eng)
{
    setTexture(path_to_texture, eng);
    setVertex_buf(path_to_vertex, eng);
    engine            = &eng;
    sound_fire_enemys = engine->create_sound_buffer("src/sound/fire_hit.wav");
    sound_fire_enemys->setVolume(0.1);
    int num = std::rand() % 10;

    switch (num)
    {
        case 0:
            pos = { -1, -0.9 };
            break;
        case 1:
            pos = { -1, -0.7 };
            break;
        case 2:
            pos = { -1, -0.5 };
            break;
        case 3:
            pos = { -1, -0.3 };
            break;
        case 4:
            pos = { -1, -0.1 };
            break;
        case 5:
            pos = { -1, 0.1 };
            break;
        case 6:
            pos = { -1, 0.3 };
            break;
        case 7:
            pos = { -1, 0.5 };
            break;
        case 8:
            pos = { -1, 0.7 };
            break;
        case 9:
            pos = { -1, 0.9 };
            break;
    }
}

Enemy::~Enemy()
{
    engine->destroy_sound_buffer(sound_fire_enemys);
    engine->destroy_vertex_buffer(vertex_buf);
    engine->destroy_texture(texture);
    //    delete sound_fire_enemys;
}

void Enemy::set_move(float x)
{
    this->pos.x += x;
}

float Enemy::get_speed()
{
    return speed;
}

float Enemy::get_radius()
{
    return radius;
}

int Enemy::get_dmg()
{
    return dmg;
}

void Enemy::render()
{
    engine->render(*this->getVertex_buf(), this->get_texture(), matrix());
}

void Enemy::render_bullets()
{
    if (!bullets.empty())
    {
        for (size_t i = 0; i < bullets.size(); ++i)
        {
            bullets.at(i).render();
            bullets.at(i).set_move(0.02, 0);
            if (bullets.at(i).get_pos().x >= 1)
            {
                bullets.erase(bullets.begin() + i);
            }
        }
    }
}

std::vector<Bullet>& Enemy::get_bullets()
{
    return bullets;
}

bool Enemy::render_litle_bugs(Tank&                 tank,
                              std::vector<Bullet*>& bullets_from_tank,
                              std::vector<Sprite>& sprites, Ani2d& animation)
{
    if (!enemys.empty())
    {
        for (size_t i = 0; i < enemys.size(); ++i)
        {
            float x = tank.get_pos().x - enemys.at(i)->get_pos().x;
            float y = tank.get_pos().y - enemys.at(i)->get_pos().y;

            float modul = sqrt(x * x + y * y);

            float cosinus_a = x / modul;
            float angle     = acos(cosinus_a);
            if (y < 0)
            {
                angle = -acos(cosinus_a);
            }

            enemys.at(i)->set_directiom(angle);

            enemys.at(i)->render();
            float m_x = 0.01 * cos(enemys.at(i)->get_directiom());
            float m_y = 0.01 * sin(enemys.at(i)->get_directiom());
            enemys.at(i)->set_move_on(m_x, m_y);

            float radius_of_tank = 0.02;
            float radius_sum     = radius_of_tank + enemys.at(i)->radius;

            float x_a = tank.get_pos().x;
            float y_a = tank.get_pos().y;
            float x_b = enemys.at(i)->get_pos().x;
            float y_b = enemys.at(i)->get_pos().y;

            float distance = sqrt(pow(x_b - x_a, 2) + pow(y_a - y_b, 2));

            if (radius_sum >= distance)
            {
                for (size_t n = 0; n < sprites.size(); ++n)
                {
                    sprites.at(n).set_world_pos(enemys.at(i)->get_pos());
                    sprites.at(n).rotation(enemys.at(i)->get_directiom());
                }
                animation.sprites(sprites);
                animation.fps(sprites.size());
                animation.restart();
                enemys.at(i)->sound_explosion_minion->play(
                    Ckom::Sound_buffer::properties::once);
                enemys.erase(enemys.begin() + i);
                tank.main_hp -= 25;
                return true;
            }
            else if (!bullets_from_tank.empty())
            {
                for (size_t j = 0; j < bullets_from_tank.size(); ++j)
                {
                    float radius_sum_2 =
                        enemys[i]->radius + bullets_from_tank.at(j)->radius;

                    float xa = bullets_from_tank.at(j)->get_pos().x;
                    float ya = bullets_from_tank.at(j)->get_pos().y;

                    float distance_fire =
                        sqrt(pow(x_b - xa, 2) + pow(y_b - ya, 2));

                    if (radius_sum_2 >= distance_fire)
                    {
                        for (size_t n = 0; n < sprites.size(); ++n)
                        {
                            sprites.at(n).set_world_pos(
                                enemys.at(i)->get_pos());
                            sprites.at(n).rotation(
                                enemys.at(i)->get_directiom());
                        }
                        animation.sprites(sprites);
                        animation.fps(sprites.size());
                        animation.restart();
                        enemys.at(i)->sound_explosion_minion->play(
                            Ckom::Sound_buffer::properties::once);
                        enemys.erase(enemys.begin() + i);
                        return true;
                    }
                }
            }
            return false;
        }
    }
    return false;
}

void Enemy::fire()
{
    sound_fire_enemys->play(Ckom::Sound_buffer::properties::once);

    Bullet bullet("src/texture/Exhaust_Fire.png", "vert_tex_color.txt", engine,
                  pos);
    timer_gun.reset();
    bullet.set_pos(pos.x + 0.1, pos.y);
    bullets.push_back(bullet);
}

void Enemy::production()
{
    if (engine == nullptr)
    {
        std::cerr << "can't producte engine = nullptr" << std::endl;
    }
    else
    {
        timer_producte.reset();
        Enemy_from_big_bos* bug = new Enemy_from_big_bos(*engine);

        //    Enemy* enemy = new Enemy_litle_bug(*engine)
        if (this->pos.y >= -0.7)
        {
            bug->set_pos(this->pos.x, this->pos.y - 0.2);
        }
        else
        {
            bug->set_pos(this->pos.x, this->pos.y + 0.2);
        }
        enemys.push_back(bug);
    }
}

void Enemy::dead_animation(const float /*delta_time*/) {}

// void Enemy::dead_anim()
//{
//    //    explosion_animation.draw(*engine, timer.elapsed());
//    //    timer.reset();
//}

int Enemy::getHp() const
{
    return hp;
}

void Enemy::setHp(int value)
{
    hp = value;
}

///////////////////////// avarge /////////////////////////////////

Enemy_avarge_bug::Enemy_avarge_bug(Ckom::Engine& eng)
    : Enemy("src/texture/Ship4.png", "vert_tex_color.txt", eng)
{
    speed  = 0.2;
    radius = 0.1;
    dmg    = 2;
    hp     = 2;
}

Enemy_avarge_bug::~Enemy_avarge_bug()
{
    //    std::cerr << "avarge destroy" << std::endl;
}

void Enemy_avarge_bug::fire()
{
    if (!in_fire)
    {
        Enemy::fire();
        in_fire = true;
    }
}

Ckom::mat2x3 Enemy_avarge_bug::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.25, 0.35 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(-pi * 0.5f);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

/////////////////////////////// BIG BOS /////////////////////////////////

Enemy_big_boss_bug::Enemy_big_boss_bug(Ckom::Engine& en)
    : Enemy("src/texture/Ship6.png", "vert_tex_color.txt", en)
{
    speed  = 0.11;
    radius = 0.1;
    dmg    = 3;
    hp     = 5;
}

Enemy_big_boss_bug::~Enemy_big_boss_bug()
{
    std::cerr << "BIG BOS DESTRUCTOR" << std::endl;
}

Ckom::mat2x3 Enemy_big_boss_bug::matrix()
{
    Ckom::mat2x3 scale    = Ckom::mat2x3::scale(0.3, 0.35 * (1024 / 760));
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(-pi * 0.5f);
    Ckom::mat2x3 position = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res      = scale * rotation * position;
    return res;
}

/////////////////////////// ENEMY FROM BIG BOS //////////////////////////

Enemy_from_big_bos::Enemy_from_big_bos(Ckom::Engine& eng)
    : Enemy("src/texture/Ship1.png", "vert_tex_color.txt", eng)
{
    speed  = 0.5;
    radius = 0.06;
    dmg    = 2;
    hp     = 1;
    sound_explosion_minion =
        engine->create_sound_buffer("src/sound/explosion3.wav");
    sound_explosion_minion->setVolume(0.15);
}

Enemy_from_big_bos::~Enemy_from_big_bos()
{
    engine->destroy_sound_buffer(sound_explosion_minion);
}

Ckom::mat2x3 Enemy_from_big_bos::matrix()
{
    Ckom::mat2x3 scale    = Ckom::mat2x3::scale(0.15);
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(directiom);
    Ckom::mat2x3 position = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res      = scale * rotation * position;
    return res;
}

float Enemy_from_big_bos::get_directiom() const
{
    return directiom;
}

void Enemy_from_big_bos::set_directiom(float value)
{
    directiom = value;
}

void Enemy_from_big_bos::set_move_on(float x, float y)
{
    this->pos.x += x;
    this->pos.y += y;
}
