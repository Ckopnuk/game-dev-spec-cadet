#ifndef ENEMY_H
#define ENEMY_H

#include "Timer.h"
#include "ani2d.h"
#include "bullet.h"
#include "engine.h"
#include "game_obj.h"
#include "sprite.h"
#include "sprite_reader.h"
#include "tank.h"

#include <cstdlib>
#include <iostream>
#include <string_view>
#include <vector>

class Enemy_from_big_bos;

class Enemy : public Game_obj
{
public:
    Enemy(std::string_view path_to_texture, std::string_view path_to_vertex,
          Ckom::Engine& en);
    virtual ~Enemy();

    virtual void  set_move(float x);
    virtual float get_speed();
    virtual float get_radius();
    virtual int   get_dmg();

    virtual void render() override;

    static void                 render_bullets();
    static std::vector<Bullet>& get_bullets();
    static bool                 render_litle_bugs(Tank&                 tank,
                                                  std::vector<Bullet*>& bullets_from_tank,
                                                  std::vector<Sprite>&  sprites,
                                                  Ani2d&                animation);

    virtual void fire();
    virtual void production();
    virtual void dead_animation(const float delta_time);
    //    virtual void dead_anim();

    Timer timer_gun;
    Timer timer_producte;
    Timer timer;

    virtual int  getHp() const;
    virtual void setHp(int value);

    int hp;

    //    static

protected:
    Sprite_reader       loader_of_sprites;
    Ani2d               explosion_animation;
    std::vector<Sprite> explosion_sprites;
    float               speed;
    float               radius;
    int                 dmg;
    Ckom::Sound_buffer* sound_fire_enemys;
    //    Ckom::Sound_buffer* sound_explosion;
};

class Enemy_litle_bug final : public Enemy
{
public:
    Enemy_litle_bug(Ckom::Engine& eng);
    ~Enemy_litle_bug() override;

    //    void dead_animation(const float delta_time);

    void fire() override
    { /*do nothing*/
    }
    void production() override
    { /*do nothing*/
    }

    Ckom::mat2x3 matrix() override final;

    //    static std::vector<Sprite> sprites_sexplosion;
};

class Enemy_avarge_bug : public Enemy
{
public:
    Enemy_avarge_bug(Ckom::Engine& eng);
    ~Enemy_avarge_bug();

    void fire() override;
    void production() override
    { /*do nothing*/
    }

    Ckom::mat2x3 matrix() override;

private:
    bool in_fire = false;
};

class Enemy_big_boss_bug final : public Enemy
{
public:
    Enemy_big_boss_bug(Ckom::Engine& en);
    ~Enemy_big_boss_bug();

    Ckom::mat2x3 matrix() override;
};

class Enemy_from_big_bos final : public Enemy
{
public:
    Enemy_from_big_bos(Ckom::Engine& eng);
    ~Enemy_from_big_bos();

    Ckom::mat2x3 matrix() override;

    float get_directiom() const;
    void  set_directiom(float value);

    void set_move_on(float x, float y);

    Ckom::Sound_buffer* sound_explosion_minion;

protected:
    float directiom;
};

#endif // ENEMY_H
