#ifndef TANK_H
#define TANK_H

#include "Timer.h"
#include "bullet.h"
#include "game_obj.h"

#include <math.h>
#include <vector>

class Tank_tower;

class Tank : public Game_obj
{
public:
    Tank(Ckom::Engine* eng);
    virtual ~Tank();

    virtual void         render() override;
    virtual Ckom::mat2x3 matrix() override;

    float get_direction() const;
    void  set_direction(float value);

    void set_move(float speed);

    Tank_tower* tower;

    Timer timer;

    float get_radius() const;

    int main_hp;

protected:
    float               direction = 0;
    float               radius    = 0.05;
    Ckom::Sound_buffer* sound_truck;
};

class Gun;

class Tank_tower : public Game_obj
{
public:
    Tank_tower(Ckom::Engine* eng);
    ~Tank_tower();

    void         render() override;
    Ckom::mat2x3 matrix() override;

    float getDirection() const;
    void  setDirection(float value);

    void set_pos_on_tank(float dir, const Ckom::vec2 tank_pos);

    Gun* gun;

protected:
    float direction = 0;
};

class Gun : public Game_obj
{
public:
    Gun(Ckom::Engine* eng);
    ~Gun();

    void         render() override;
    Ckom::mat2x3 matrix() override;

    void set_pos_on_tower(float dir, const Ckom::vec2 tower_pos);
    void fire();

    std::vector<Bullet*> bullets;

    float getDir() const;
    void  setDir(float value);

protected:
    float dir = 0;
};

#endif // TANK_H
