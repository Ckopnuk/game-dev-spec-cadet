#ifndef ANI2D_H
#define ANI2D_H

#include "sprite.h"

#include <vector>

#include "engine.h"

class Ani2d final
{
public:
    Ani2d();
    ~Ani2d();

    void sprites(const std::vector<Sprite>& sprites_value)
    {
        sprites_ = sprites_value;
    }

    float fps() const { return fps_; }
    void  fps(float fps_value) { fps_ = fps_value; }

    void restart() { current_time_ = 0.f; }

    bool draw_once(Ckom::Engine& eng, float delta_time);
    void draw_elepsed(Ckom::Engine& eng, float delta_time);

private:
    std::vector<Sprite> sprites_;
    float               fps_;
    float               current_time_ = 0.f;
};

#endif // ANI2D_H
