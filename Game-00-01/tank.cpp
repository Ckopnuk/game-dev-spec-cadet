#include "tank.h"

Tank::Tank(Ckom::Engine* eng)
{
    engine = eng;
    setTexture("src/texture/Hull_04.png", *engine);
    setVertex_buf("vert_tex_color.txt", *engine);
    sound_truck = engine->create_sound_buffer("src/sound/truck.wav");
    sound_truck->setVolume(0.3);

    tower = new Tank_tower(engine);

    main_hp = 100;
}

Tank::~Tank()
{
    std::cout << "destroy tank" << std::endl;
    engine->destroy_vertex_buffer(vertex_buf);
    engine->destroy_texture(texture);
    delete tower;
}

void Tank::render()
{
    engine->render(*vertex_buf, texture, matrix());
    tower->set_pos_on_tank(this->direction, this->pos);
    tower->render();
}

Ckom::mat2x3 Tank::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.3);
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(direction);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

float Tank::get_direction() const
{
    return direction;
}

void Tank::set_direction(float value)
{
    direction = value;
}

void Tank::set_move(float speed)
{
    pos.x += speed * -sin(direction);
    pos.y += speed * cos(direction);
    //    if (timer.elapsed() >= 1)
    //    {
    //        sound_truck->play(Ckom::Sound_buffer::properties::looped);
    //        timer.reset();
    //    }
}

float Tank::get_radius() const
{
    return radius;
}

////////////////////////// Tank Tower ////////////////////////////////

Tank_tower::Tank_tower(Ckom::Engine* eng)
{
    engine = eng;
    setTexture("src/texture/Gun_06_B.png", *engine);
    setVertex_buf("vert_tex_color.txt", *engine);
    gun = new Gun(engine);
}

Tank_tower::~Tank_tower()
{
    std::cout << "destroy tower" << std::endl;
    delete gun;
}

void Tank_tower::render()
{
    engine->render(*this->vertex_buf, this->texture, this->matrix());
    gun->set_pos_on_tower(this->direction, this->pos);
    gun->render();
}

Ckom::mat2x3 Tank_tower::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.1, 0.1 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(direction);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

float Tank_tower::getDirection() const
{
    return direction;
}

void Tank_tower::setDirection(float value)
{
    direction = value;
}

void Tank_tower::set_pos_on_tank(float dir, const Ckom::vec2 tank_pos)
{
    this->pos = tank_pos;
    this->pos.x -= 0.05 * -sin(dir);
    this->pos.y -= 0.05 * cos(dir);
}

///////////////////////////////// GUN //////////////////////////////////

Gun::Gun(Ckom::Engine* eng)
{
    engine = eng;
    setTexture("src/texture/Gun_06_A.png", *engine);
    setVertex_buf("tower_vert.txt", *engine);
}

Gun::~Gun()
{
    std::cout << "destroy gun" << std::endl;
}

void Gun::render()
{

    for (size_t i = 0; i < bullets.size(); ++i)
    {
        bullets.at(i)->render();
        float x = 0.025f * -sin(bullets.at(i)->get_direction());
        float y = 0.025f * cos(bullets.at(i)->get_direction());
        bullets.at(i)->set_move(x, y);
    }
    engine->render(*this->vertex_buf, this->texture, matrix());
}

Ckom::mat2x3 Gun::matrix()
{
    Ckom::mat2x3 scale    = Ckom::mat2x3::scale(0.07);
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(dir);
    Ckom::mat2x3 position = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res      = scale * rotation * position;
    return res;
}

void Gun::set_pos_on_tower(float dir, const Ckom::vec2 tower_pos)
{
    this->dir = dir;
    this->pos = tower_pos;
    this->pos.x += 0.08 * -sin(this->dir);
    this->pos.y += 0.08 * cos(this->dir);
}

void Gun::fire()
{
    Ckom::vec2 position = this->pos;
    Bullet* bullet = new Bullet("src/texture/bullet3.png", "vert_tex_color.txt",
                                engine, position);
    bullet->set_direction(this->dir);
    bullet->set_scale_n(0.09);
    bullets.push_back(bullet);

    for (size_t i = 0; i < bullets.size(); ++i)
    {
    }
}

float Gun::getDir() const
{
    return dir;
}

void Gun::setDir(float value)
{
    dir = value;
}
