#ifndef GROUND_H
#define GROUND_H

#include "game_obj.h"

#include <numbers>

class Ground : public Game_obj
{
public:
    Ground(Ckom::Engine* eng);
    virtual ~Ground();

    virtual void         render() override;
    virtual Ckom::mat2x3 matrix() override;

    float get_radius() const;

protected:
    float radius = 0.03f;
};

class Ground_in_fire : public Ground
{
public:
    Ground_in_fire(Ckom::Engine* eng);
    virtual ~Ground_in_fire();
};

class Ground_space : public Ground
{
public:
    Ground_space(Ckom::Engine* eng);
    ~Ground_space();

    Ckom::mat2x3 matrix() override;
};

#endif // GROUND_H
