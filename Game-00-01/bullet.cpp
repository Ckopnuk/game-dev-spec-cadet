#include "bullet.h"

Bullet::Bullet(std::string_view path_to_texture, std::string_view path_to_vb,
               Ckom::Engine* eng, Ckom::vec2& position)
{
    engine = eng;
    setTexture(path_to_texture, *engine);
    setVertex_buf(path_to_vb, *engine);
    this->pos        = position;
    sound_bullet_hit = engine->create_sound_buffer("src/sound/fire2.wav");
    sound_bullet_hit->setVolume(0.4);
}

Bullet::~Bullet() {}

void Bullet::render()
{
    if (vertex_buf == nullptr)
    {
        throw std::runtime_error("not init vert_buf");
    }
    engine->render(*vertex_buf, texture, matrix());
}

Ckom::mat2x3 Bullet::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(scale_n);
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(direction);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(this->pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

float Bullet::get_direction() const
{
    return direction;
}

void Bullet::set_direction(float value)
{
    direction = value;
}

void Bullet::set_move(float x, float y)
{
    pos.x += x;
    pos.y += y;
}

float Bullet::get_scale_n() const
{
    return scale_n;
}

void Bullet::set_scale_n(float value)
{
    scale_n = value;
}
