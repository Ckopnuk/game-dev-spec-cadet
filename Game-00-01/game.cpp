#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <istream>
#include <memory>
#include <numbers>
#include <sstream>
#include <string_view>
#include <vector>

#pragma GCC diagnostic push
// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wall"
#include "src/imgui/imgui.h"
#pragma GCC diagnostic pop

#include "Timer.h"
#include "ani2d.h"
#include "enemy.h"
#include "engine.h"
#include "ground.h"
#include "sprite.h"
#include "sprite_reader.h"
#include "tank.h"

#include <chrono>

using namespace std;

enum enemy_create_name
{
    litle,
    avarge,
    big
};

void draw_hp(std::vector<Sprite>& sprites, Ckom::Engine& engine, int& hp);
void set_sprite_pos_to_anim_from_enemy(std::vector<Sprite>& sprites,
                                       Ani2d& anim, Enemy& enemy);
void create_enemy(enemy_create_name name_enemy, float& time_value, Timer& timer,
                  size_t& num_enemy, std::vector<Enemy*>& vector,
                  Ckom::Engine& engine);

const float pi = std::numbers::pi_v<float>;

struct colums
{
    std::array<int, 10> rows;
};

struct Obj_matrix
{
    Obj_matrix()
    {
        for (size_t i = 0; i < cols.size(); ++i)
        {
            cols[i].rows.fill(1);
        }
    }
    std::array<colums, 10> cols;
};

int main(int /*argc*/, char* /*argv*/[])
{
    Ckom::Engine*     engine = Ckom::create_engine();
    const std::string error  = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    // load sound
    Ckom::Sound_buffer* sound     = engine->create_sound_buffer("road.wav");
    Ckom::Sound_buffer* sound_gun = engine->create_sound_buffer("gun_2.wav");
    Ckom::Sound_buffer* sound_tank_move =
        engine->create_sound_buffer("src/sound/tank_move.wav");
    Ckom::Sound_buffer* sound_explosion_big_bos =
        engine->create_sound_buffer("src/sound/explosion.wav");
    Ckom::Sound_buffer* sound_explosion_avarge =
        engine->create_sound_buffer("src/sound/explosion1.wav");
    Ckom::Sound_buffer* sound_explosion_litle =
        engine->create_sound_buffer("src/sound/explosion2.wav");

    sound->setVolume(0.5);
    sound_gun->setVolume(0.05);
    sound_tank_move->setVolume(0.7);
    sound_explosion_big_bos->setVolume(0.15);
    sound_explosion_avarge->setVolume(0.15);
    sound_explosion_litle->setVolume(0.15);

    // to count number of enemies
    size_t num_litle   = 0;
    size_t num_avarge  = 0;
    size_t num_big_bos = 0;

    std::vector<Enemy*> swarm_enemy;
    Tank                tank(engine);

    bool  continue_loop = true;
    float speed         = 0.01;
    // count of enemies
    size_t litle_bud_numbers = 30;

    Timer timer;
    Timer time_gun_attack;
    Timer time_l;
    Timer time_a;
    Timer time_b;
    Timer timer_bullets;
    Timer timer_sound_move;
    float time_value            = 0.f;
    float time_value_avarge     = 0.f;
    float time_value_big_bos    = 0.f;
    float time_value_bullets    = 0.f;
    float time_gun_attack_value = 0.f;
    float direction_tower       = 0.f;

    std::ifstream file_litle_expl;
    std::ifstream file_avarge_expl;
    std::ifstream file_big_bos_expl;
    std::ifstream file_minion_expl;
    std::ifstream file_bullet_expl;
    std::ifstream file_fire_enemy;

    std::ifstream file_text;
    file_text.exceptions(std::ios::badbit | std::ios::failbit);
    file_text.open("src/anim/text/text.yaml", std::ios::binary);
    std::vector<Sprite> sprites_text;
    Sprite_reader       loader_of_sprites;
    loader_of_sprites.load_sprites(sprites_text, file_text, *engine);
    //    file_text.close();
    Ani2d anim_text;
    anim_text.sprites(sprites_text);
    anim_text.fps(sprites_text.size());

    file_litle_expl.exceptions(std::ios::badbit | std::ios::failbit);
    file_litle_expl.open("src/anim/explosion_litle_enemy/explosion.yaml",
                         std::ios::binary);
    file_avarge_expl.exceptions(std::ios::badbit | std::ios::failbit);
    file_avarge_expl.open("src/anim/explosion_avarge_enemy/explosion.yaml",
                          std::ios::binary);
    file_big_bos_expl.exceptions(std::ios::badbit | std::ios::failbit);
    file_big_bos_expl.open("src/anim/explosion_big_enemy/explosion.yaml",
                           std::ios::binary);
    file_minion_expl.exceptions(std::ios::badbit | std::ios::failbit);
    file_minion_expl.open("src/anim/explosion_minion/explosion.yaml",
                          std::ios::binary);
    file_bullet_expl.exceptions(std::ios::badbit | std::ios::failbit);
    file_bullet_expl.open("src/anim/explosion_bullet/explosion.yaml",
                          std::ios::binary);

    std::vector<Sprite> sprites_lirle_expl;
    std::vector<Sprite> sprites_avarge_expl;
    std::vector<Sprite> sprites_big_bos_expl;
    std::vector<Sprite> sprites_expl_minion;
    std::vector<Sprite> sprites_bullet_expl;

    loader_of_sprites.load_sprites(sprites_lirle_expl, file_litle_expl,
                                   *engine);
    loader_of_sprites.load_sprites(sprites_avarge_expl, file_avarge_expl,
                                   *engine);
    loader_of_sprites.load_sprites(sprites_big_bos_expl, file_big_bos_expl,
                                   *engine);
    loader_of_sprites.load_sprites(sprites_expl_minion, file_minion_expl,
                                   *engine);
    loader_of_sprites.load_sprites(sprites_bullet_expl, file_bullet_expl,
                                   *engine);

    // close file
    file_litle_expl.close();
    file_avarge_expl.close();
    file_big_bos_expl.close();
    file_minion_expl.close();
    file_bullet_expl.close();

    // texture scaling
    for (size_t i = 0; i < sprites_lirle_expl.size(); ++i)
    {
        sprites_lirle_expl.at(i).size({ 0.3, 0.3 });
    }
    for (size_t i = 0; i < sprites_avarge_expl.size(); ++i)
    {
        sprites_avarge_expl.at(i).size({ 0.7, 0.75 * (1024 / 760) });
    }
    for (size_t i = 0; i < sprites_big_bos_expl.size(); ++i)
    {
        sprites_big_bos_expl.at(i).size({ 0.8, 0.7 /** (1024 / 760)*/ });
    }
    for (size_t i = 0; i < sprites_expl_minion.size(); ++i)
    {
        sprites_expl_minion.at(i).size({ 0.4, 0.4 /** (1024 / 760)*/ });
    }
    for (size_t i = 0; i < sprites_bullet_expl.size(); ++i)
    {
        sprites_bullet_expl.at(i).size({ 0.4, 0.4 /** (1024 / 760)*/ });
    }

    Ani2d anim_litle_expl;
    Ani2d anim_avarge_expl;
    Ani2d anim_big_bos_expl;
    Ani2d anim_minion_expl;
    Ani2d anim_bullet_expl;

    anim_litle_expl.sprites(sprites_lirle_expl);
    anim_litle_expl.fps(sprites_lirle_expl.size() * 1);

    anim_avarge_expl.sprites(sprites_avarge_expl);
    anim_avarge_expl.fps(sprites_avarge_expl.size());

    anim_big_bos_expl.sprites(sprites_big_bos_expl);
    anim_big_bos_expl.fps(sprites_big_bos_expl.size());

    anim_minion_expl.sprites(sprites_expl_minion);
    anim_minion_expl.fps(sprites_expl_minion.size());

    anim_bullet_expl.sprites(sprites_bullet_expl);
    anim_bullet_expl.fps(sprites_bullet_expl.size());

    std::vector<Ground*> grounds;
    std::vector<size_t>  num_ground_in_fire;
    Obj_matrix           grounds_matrix;

    bool draw         = false;
    bool draw_minion  = false;
    bool draw_avarge  = false;
    bool draw_big_bos = false;
    bool draw_bullet  = false;
    bool is_play      = false;

    grounds_matrix.cols.at(4).rows.at(6) = 0;

    Ground_space fon_space(engine);

    for (size_t n = 0; n < 100; ++n)
    {
        grounds.push_back(new Ground(engine));
        int i = n % 10;
        int j = (n % 100 - i) / 10;
        grounds.at(n)->set_pos(-0.9 + (j * 0.2), 0.9 - (i * 0.2));
    }

    while (continue_loop)
    {
        if (!is_play)
        {
            sound->play(Ckom::Sound_buffer::properties::looped);
            is_play = true;
        }

        float delta_time = timer.elapsed();
        timer.reset();
        Ckom::Event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continue_loop = false;
                    break;
                default:
                    break;
            }
        }

        if (engine->is_key_down(Ckom::Keys::left) &&
            engine->is_key_down(Ckom::Keys::up))
        {
            tank.set_direction(pi * 0.25);
            tank.set_move(speed);
        }
        else if (engine->is_key_down(Ckom::Keys::right) &&
                 engine->is_key_down(Ckom::Keys::up))
        {
            tank.set_direction(-pi * 0.25);
            tank.set_move(speed);
        }
        else if (engine->is_key_down(Ckom::Keys::right) &&
                 engine->is_key_down(Ckom::Keys::down))
        {
            tank.set_direction(-pi * 0.75);
            tank.set_move(speed);
        }
        else if (engine->is_key_down(Ckom::Keys::left) &&
                 engine->is_key_down(Ckom::Keys::down))
        {
            tank.set_direction(pi * 0.75);
            tank.set_move(speed);
        }
        else if (engine->is_key_down(Ckom::Keys::left))
        {
            tank.set_direction(pi * 0.5);
            tank.set_move(speed);
        }
        else if (engine->is_key_down(Ckom::Keys::right))
        {
            tank.set_direction(-pi * 0.5);
            tank.set_move(speed);
        }
        else if (engine->is_key_down(Ckom::Keys::up))
        {
            tank.set_direction(0);
            tank.set_move(speed);
        }
        else if (engine->is_key_down(Ckom::Keys::down))
        {
            tank.set_direction(-pi);
            tank.set_move(speed);
        }

        if (engine->is_key_down(Ckom::Keys::up_a))
        {

            if (time_gun_attack_value >= 0.1f)
            {

                sound_gun->play(Ckom::Sound_buffer::properties::once);
                tank.tower->gun->setDir(direction_tower);
                time_gun_attack.reset();
                tank.tower->gun->fire();
            }
        }
        if (engine->is_key_down(Ckom::Keys::right_a))
        {
            direction_tower -= pi * 0.015;
            tank.tower->setDirection(direction_tower);
        }
        if (engine->is_key_down(Ckom::Keys::left_a))
        {
            direction_tower += pi * 0.015;
            tank.tower->setDirection(direction_tower);
        }

        if (tank.main_hp <= 0)
        {
            std::cout << "hp  is 0!!! YOU LOST!!!" << std::endl;
            continue_loop = false;
        }

        time_gun_attack_value = time_gun_attack.elapsed();

        time_value         = time_l.elapsed();
        time_value_avarge  = time_a.elapsed();
        time_value_big_bos = time_b.elapsed();
        time_value_bullets = timer_bullets.elapsed();

        ////////////////////// CREATE GROUND ///////////////////////

        //                create grounds
        //        for (size_t i = 0; i < grounds_matrix.cols[0].rows.size();
        //        ++i)
        //        {
        //            for (size_t j = 0; j < grounds_matrix.cols.size(); ++j)
        //            {
        //                //                if in position on matrix have 0 then
        //                //                replace
        //                //                gound to the ground_in_fire
        //                if (grounds_matrix.cols[j].rows[i] == 0)
        //                {
        //                    Ground* name = new Ground_in_fire(engine);
        //                    float   x    = grounds.at(i + (j *
        //                    10))->get_pos().x; float   y    = grounds.at(i +
        //                    (j * 10))->get_pos().y; name->set_pos(x, y);
        //                    delete grounds.at(i + (j * 10));
        //                    grounds.at(i + (j * 10)) = name;

        //                    if (!swarm_enemy.empty())
        //                    {
        //                        // when the bug is on the burning
        //                        // floor then it is born
        //                        // at the begining
        //                        for (size_t n = 0; n < swarm_enemy.size();
        //                        ++n)
        //                        {
        //                            double radius =
        //                                swarm_enemy.at(n)->get_radius() +
        //                                grounds.at(i + (j *
        //                                10))->get_radius();
        //                            double xa =
        //                            swarm_enemy.at(n)->get_pos().x; double ya
        //                            = swarm_enemy.at(n)->get_pos().y;

        //                            double xb = grounds.at(i + (j *
        //                            10))->get_pos().x; double yb =
        //                            grounds.at(i + (j * 10))->get_pos().y;

        //                            double distance =
        //                                sqrt(pow((xb - xa), 2) + pow((yb -
        //                                ya), 2));
        //                            if (radius >= distance)
        //                            {
        //                                if (swarm_enemy.at(n)->get_dmg() == 3)
        //                                {
        //                                    delete swarm_enemy.at(n);
        //                                    swarm_enemy.at(n) =
        //                                        new
        //                                        Enemy_big_boss_bug(*engine);
        //                                    swarm_enemy.at(n)->fire();
        //                                }

        //                                else if (swarm_enemy.at(n)->get_dmg()
        //                                == 2)
        //                                {
        //                                    delete swarm_enemy.at(n);
        //                                    swarm_enemy.at(n) =
        //                                        new Enemy_avarge_bug(*engine);
        //                                    swarm_enemy.at(n)->fire();
        //                                }

        //                                else if (swarm_enemy.at(n)->get_dmg()
        //                                == 1)
        //                                {
        //                                    delete swarm_enemy.at(n);
        //                                    swarm_enemy.at(n) =
        //                                        new Enemy_litle_bug(*engine);
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        // end create grounds

        ////////////////////// CREATE ENEMY ////////////////////////

        create_enemy(enemy_create_name::litle, time_value, time_l, num_litle,
                     swarm_enemy, *engine);

        create_enemy(enemy_create_name::avarge, time_value_avarge, time_a,
                     num_avarge, swarm_enemy, *engine);

        create_enemy(enemy_create_name::big, time_value_big_bos, time_b,
                     num_big_bos, swarm_enemy, *engine);

        /////////////////////// ATTACK //////////////////////

        if (!tank.tower->gun->bullets.empty() && !swarm_enemy.empty())
        {
            for (size_t i = 0; i < tank.tower->gun->bullets.size(); ++i)
            {
                for (size_t j = 0; j < swarm_enemy.size(); ++j)
                {
                    double radius = tank.tower->gun->bullets[i]->radius +
                                    swarm_enemy[j]->get_radius();
                    double xa = tank.tower->gun->bullets[i]->get_pos().x;
                    double ya = tank.tower->gun->bullets[i]->get_pos().y;
                    double xb = swarm_enemy[j]->get_pos().x;
                    double yb = swarm_enemy[j]->get_pos().y;

                    //                    double b_x = Enemy::
                    double distance =
                        sqrt(pow((xb - xa), 2) + pow((yb - ya), 2));
                    if (radius >= distance)
                    {
                        swarm_enemy.at(j)->hp -= 1;
                        if (swarm_enemy.at(j)->get_dmg() == 1 &&
                            swarm_enemy.at(j)->hp <= 0)
                        {
                            --num_litle;

                            set_sprite_pos_to_anim_from_enemy(
                                sprites_lirle_expl, anim_litle_expl,
                                *swarm_enemy.at(j));

                            draw = true;
                            anim_litle_expl.restart();

                            delete swarm_enemy[j];
                            sound_explosion_litle->play(
                                Ckom::Sound_buffer::properties::once);
                            swarm_enemy.erase(swarm_enemy.begin() + j);
                        }
                        else if (swarm_enemy.at(j)->get_dmg() == 2 &&
                                 swarm_enemy.at(j)->hp <= 0)
                        {
                            set_sprite_pos_to_anim_from_enemy(
                                sprites_avarge_expl, anim_avarge_expl,
                                *swarm_enemy.at(j));

                            draw_avarge = true;
                            anim_avarge_expl.restart();

                            --num_avarge;
                            delete swarm_enemy[j];
                            sound_explosion_avarge->play(
                                Ckom::Sound_buffer::properties::once);
                            swarm_enemy.erase(swarm_enemy.begin() + j);
                        }
                        else if (swarm_enemy.at(j)->get_dmg() == 3 &&
                                 swarm_enemy.at(j)->hp <= 0)
                        {
                            set_sprite_pos_to_anim_from_enemy(
                                sprites_big_bos_expl, anim_big_bos_expl,
                                *swarm_enemy.at(j));

                            draw_big_bos = true;
                            anim_big_bos_expl.restart();

                            --num_big_bos;
                            delete swarm_enemy[j];
                            sound_explosion_big_bos->play(
                                Ckom::Sound_buffer::properties::once);
                            swarm_enemy.erase(swarm_enemy.begin() + j);
                        }

                        tank.tower->gun->bullets.erase(
                            tank.tower->gun->bullets.begin() + i);
                    }
                }
            }
        }

        /////////////////////// GET DMG /////////////////////

        for (size_t i = 0; i < swarm_enemy.size(); ++i)
        {
            if (swarm_enemy.at(i)->get_pos().x >= 1)
            {
                tank.main_hp -= swarm_enemy.at(i)->get_dmg();
            }
        }

        std::vector<Bullet>& enemys_bullets = Enemy::get_bullets();

        if (!enemys_bullets.empty())
        {
            float x_a = tank.get_pos().x;
            float y_a = tank.get_pos().y;

            for (size_t i = 0; i < enemys_bullets.size(); ++i)
            {
                float x_b = enemys_bullets.at(i).get_pos().x;
                float y_b = enemys_bullets.at(i).get_pos().y;

                float radius_sum =
                    tank.get_radius() + enemys_bullets.at(i).radius;
                float distance = sqrt(pow(x_b - x_a, 2) + pow(y_b - y_a, 2));

                if (distance <= radius_sum)
                {
                    for (size_t num = 0; num < sprites_bullet_expl.size();
                         ++num)
                    {
                        sprites_bullet_expl.at(num).set_world_pos(
                            enemys_bullets.at(i).get_pos());
                    }
                    anim_bullet_expl.sprites(sprites_bullet_expl);
                    anim_bullet_expl.fps(sprites_bullet_expl.size());
                    draw_bullet = true;
                    anim_bullet_expl.restart();
                    enemys_bullets.at(i).sound_bullet_hit->play(
                        Ckom::Sound_buffer::properties::once);
                    enemys_bullets.erase(enemys_bullets.begin() + i);
                    tank.main_hp -= 20;
                }
            }
        }

        /////////////////////// RENDER //////////////////////

        // render grounds
        for (size_t i = 0; i < grounds.size(); ++i)
        {
            grounds.at(i)->render();
        }

        // end render grounds

        tank.render();

        // render enemy
        for (size_t i = 0; i < swarm_enemy.size(); ++i)
        {
            swarm_enemy.at(i)->render();
            swarm_enemy.at(i)->set_move(speed * swarm_enemy.at(i)->get_speed());
            // big bos attack every 1.5 seconds
            if (swarm_enemy.at(i)->timer_gun.elapsed() >= 1.5)
            {
                swarm_enemy.at(i)->fire();
            }
            // big bos producte litle enemy every 2 seconds
            if (swarm_enemy.at(i)->timer_producte.elapsed() >= 2 &&
                num_litle < litle_bud_numbers)
            {
                swarm_enemy.at(i)->production();
            }
            //            when he crossed the border created new one
            //            on the
            //            start line
            if (swarm_enemy.at(i)->get_pos().x >= 1)
            {
                tank.main_hp -= swarm_enemy.at(i)->get_dmg();

                if (swarm_enemy.at(i)->get_dmg() == 3)
                {
                    for (size_t num = 0; num < sprites_big_bos_expl.size();
                         ++num)
                    {
                        sprites_big_bos_expl.at(num).set_world_pos(
                            swarm_enemy.at(i)->get_pos());
                    }
                    anim_big_bos_expl.sprites(sprites_big_bos_expl);
                    anim_big_bos_expl.fps(sprites_big_bos_expl.size());
                    draw_big_bos = true;
                    anim_big_bos_expl.restart();

                    delete swarm_enemy.at(i);
                    sound_explosion_big_bos->play(
                        Ckom::Sound_buffer::properties::once);
                    swarm_enemy.at(i) = new Enemy_big_boss_bug(*engine);
                    swarm_enemy.at(i)->fire();
                }
                else if (swarm_enemy.at(i)->get_dmg() == 2)
                {
                    for (size_t num = 0; num < sprites_avarge_expl.size();
                         ++num)
                    {
                        sprites_avarge_expl.at(num).set_world_pos(
                            swarm_enemy.at(i)->get_pos());
                        anim_avarge_expl.sprites(sprites_avarge_expl);
                        anim_avarge_expl.fps(sprites_avarge_expl.size());
                    }
                    draw_avarge = true;
                    anim_avarge_expl.restart();

                    delete swarm_enemy.at(i);
                    sound_explosion_avarge->play(
                        Ckom::Sound_buffer::properties::once);
                    swarm_enemy.at(i) = new Enemy_avarge_bug(*engine);
                    // he attack one times when he created
                    swarm_enemy.at(i)->fire();
                }
                else if (swarm_enemy.at(i)->get_dmg() == 1)
                {

                    for (size_t q = 0; q < sprites_lirle_expl.size(); ++q)
                    {
                        sprites_lirle_expl.at(q).set_world_pos(
                            swarm_enemy.at(i)->get_pos());
                        anim_litle_expl.sprites(sprites_lirle_expl);
                        anim_litle_expl.fps(sprites_lirle_expl.size());
                    }

                    draw = true;
                    anim_litle_expl.restart();

                    delete swarm_enemy.at(i);
                    sound_explosion_litle->play(
                        Ckom::Sound_buffer::properties::once);
                    swarm_enemy.at(i) = new Enemy_litle_bug(*engine);
                }
            }
        }

        draw_hp(sprites_text, *engine, tank.main_hp);

        if (draw)
        {
            anim_litle_expl.draw_once(*engine, delta_time);
        }
        if (draw_avarge)
        {
            anim_avarge_expl.draw_once(*engine, delta_time);
        }
        if (draw_big_bos)
        {
            anim_big_bos_expl.draw_once(*engine, delta_time);
        }
        if (draw_minion)
        {
            anim_minion_expl.draw_once(*engine, delta_time);
        }
        if (draw_bullet)
        {
            anim_bullet_expl.draw_once(*engine, delta_time);
        }

        // end render enemy

        // render bullets
        for (size_t i = 0; i < tank.tower->gun->bullets.size(); ++i)
        {
            if (tank.tower->gun->bullets.at(i)->get_pos().x >= 1 ||
                tank.tower->gun->bullets.at(i)->get_pos().x <= -1 ||
                tank.tower->gun->bullets.at(i)->get_pos().y >= 1 ||
                tank.tower->gun->bullets.at(i)->get_pos().y <= -1)
            {
                tank.tower->gun->bullets.erase(
                    tank.tower->gun->bullets.begin() + i);
            }
        }
        // end render bullets

        Enemy::render_bullets();

        if (!swarm_enemy.empty())
        {
            if (Enemy::render_litle_bugs(tank, tank.tower->gun->bullets,
                                         sprites_expl_minion, anim_minion_expl))
            {
                draw_minion = true;
            }
        }

        engine->swap_buffers();
    }

    swarm_enemy.clear();
    engine->uninitialize();
    Ckom::destroy_engine(engine);

    return EXIT_SUCCESS;
}

void draw_hp(std::vector<Sprite>& sprites, Ckom::Engine& engine, int& hp)
{

    switch (hp % 10)
    {
        case 0:
            sprites.at(0).set_world_pos({ 0.9, 0.9 });
            sprites.at(0).size({ 0.1, 0.1 });
            sprites.at(0).draw(engine);
            break;
        case 1:
            sprites.at(1).set_world_pos({ 0.9, 0.9 });
            sprites.at(1).size({ 0.1, 0.1 });
            sprites.at(1).draw(engine);
            break;
        case 2:
            sprites.at(2).set_world_pos({ 0.9, 0.9 });
            sprites.at(2).size({ 0.1, 0.1 });
            sprites.at(2).draw(engine);
            break;
        case 3:
            sprites.at(3).set_world_pos({ 0.9, 0.9 });
            sprites.at(3).size({ 0.1, 0.1 });
            sprites.at(3).draw(engine);
            break;
        case 4:
            sprites.at(4).set_world_pos({ 0.9, 0.9 });
            sprites.at(4).size({ 0.1, 0.1 });
            sprites.at(4).draw(engine);
            break;
        case 5:
            sprites.at(5).set_world_pos({ 0.9, 0.9 });
            sprites.at(5).size({ 0.1, 0.1 });
            sprites.at(5).draw(engine);
            break;
        case 6:
            sprites.at(6).set_world_pos({ 0.9, 0.9 });
            sprites.at(6).size({ 0.1, 0.1 });
            sprites.at(6).draw(engine);
            break;
        case 7:
            sprites.at(7).set_world_pos({ 0.9, 0.9 });
            sprites.at(7).size({ 0.1, 0.1 });
            sprites.at(7).draw(engine);
            break;
        case 8:
            sprites.at(8).set_world_pos({ 0.9, 0.9 });
            sprites.at(8).size({ 0.1, 0.1 });
            sprites.at(8).draw(engine);
            break;
        case 9:
            sprites.at(9).set_world_pos({ 0.9, 0.9 });
            sprites.at(9).size({ 0.1, 0.1 });
            sprites.at(9).draw(engine);
            break;
    }

    switch ((hp % 100) / 10)
    {
        case 0:
            sprites.at(0).set_world_pos({ 0.8, 0.9 });
            sprites.at(0).size({ 0.1, 0.1 });
            sprites.at(0).draw(engine);
            break;
        case 1:
            sprites.at(1).set_world_pos({ 0.8, 0.9 });
            sprites.at(1).size({ 0.1, 0.1 });
            sprites.at(1).draw(engine);
            break;
        case 2:
            sprites.at(2).set_world_pos({ 0.8, 0.9 });
            sprites.at(2).size({ 0.1, 0.1 });
            sprites.at(2).draw(engine);
            break;
        case 3:
            sprites.at(3).set_world_pos({ 0.8, 0.9 });
            sprites.at(3).size({ 0.1, 0.1 });
            sprites.at(3).draw(engine);
            break;
        case 4:
            sprites.at(4).set_world_pos({ 0.8, 0.9 });
            sprites.at(4).size({ 0.1, 0.1 });
            sprites.at(4).draw(engine);
            break;
        case 5:
            sprites.at(5).set_world_pos({ 0.8, 0.9 });
            sprites.at(5).size({ 0.1, 0.1 });
            sprites.at(5).draw(engine);
            break;
        case 6:
            sprites.at(6).set_world_pos({ 0.8, 0.9 });
            sprites.at(6).size({ 0.1, 0.1 });
            sprites.at(6).draw(engine);
            break;
        case 7:
            sprites.at(7).set_world_pos({ 0.8, 0.9 });
            sprites.at(7).size({ 0.1, 0.1 });
            sprites.at(7).draw(engine);
            break;
        case 8:
            sprites.at(8).set_world_pos({ 0.8, 0.9 });
            sprites.at(8).size({ 0.1, 0.1 });
            sprites.at(8).draw(engine);
            break;
        case 9:
            sprites.at(9).set_world_pos({ 0.8, 0.9 });
            sprites.at(9).size({ 0.1, 0.1 });
            sprites.at(9).draw(engine);
            break;
    }

    if (hp == 100)
    {
        sprites.at(1).set_world_pos({ 0.7, 0.9 });
        sprites.at(1).size({ 0.1, 0.1 });
        sprites.at(1).draw(engine);
    }
}

void create_enemy(enemy_create_name name_enemy, float& time_value, Timer& timer,
                  size_t& num_enemy, std::vector<Enemy*>& vector,
                  Ckom::Engine& engine)
{
    float  born_time     = 1.f;
    size_t max_enemy_num = 10;
    switch (name_enemy)
    {
        case enemy_create_name::litle:
            born_time     = 1.f;
            max_enemy_num = 25;
            if (time_value >= born_time && num_enemy < max_enemy_num)
            {
                time_value = 0.0f;
                timer.reset();
                ++num_enemy;

                Enemy* enemy = new Enemy_litle_bug(engine);
                vector.push_back(enemy);
            }
            break;
        case enemy_create_name::avarge:
            born_time     = 1.5f;
            max_enemy_num = 10;
            if (time_value >= born_time && num_enemy < max_enemy_num)
            {
                time_value = 0.0f;
                timer.reset();
                ++num_enemy;

                Enemy* enemy = new Enemy_avarge_bug(engine);
                vector.push_back(enemy);
            }
            break;
        case enemy_create_name::big:
            born_time     = 6;
            max_enemy_num = 3;
            if (time_value >= born_time && num_enemy < max_enemy_num)
            {
                time_value = 0.0f;
                timer.reset();
                ++num_enemy;

                Enemy* enemy = new Enemy_big_boss_bug(engine);
                vector.push_back(enemy);
            }
            break;
        default:
            born_time     = 1.f;
            max_enemy_num = 10;
            break;
    }
}

void set_sprite_pos_to_anim_from_enemy(std::vector<Sprite>& sprites,
                                       Ani2d& anim, Enemy& enemy)
{
    std::for_each(sprites.begin(), sprites.end(),
                  [&enemy](Sprite& s) { s.set_world_pos(enemy.get_pos()); });
    anim.sprites(sprites);
    anim.fps(sprites.size());
}
