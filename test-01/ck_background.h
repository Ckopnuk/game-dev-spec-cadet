#ifndef CK_BACKGROUND_H
#define CK_BACKGROUND_H

#include <numbers>

#include "game_obj.h"


class Ck_Background : public Game_obj
{
public:
    Ck_Background(Ckom::Engine* eng);
    virtual ~Ck_Background();

    virtual void render() override;
    virtual Ckom::mat2x3 matrix() override;
};

#endif // CK_BACKGROUND_H
