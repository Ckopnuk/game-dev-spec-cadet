#include <iostream>

#include "engine.h"
#include "ck_background.h"


int main(int /*argc*/, char* /*argv*/[]){
    Ckom::Engine* engine = Ckom::create_engine();
    const std::string error = engine->initialize("");
    if(!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    bool continue_loop = true;
    Ck_Background* bgrnd = new Ck_Background(engine);

    while (continue_loop)
    {
        Ckom::Event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event) {
            case Ckom::Event::turn_off:
                continue_loop = false;
                break;
            default:
                break;
            }
        }

        if(engine->is_key_down(Ckom::Keys::select))
        {
            Ckom::Event::turn_off;
            continue_loop = false;
           // delete  background;
        }
        bgrnd->render();

    }

    return EXIT_SUCCESS;
}
