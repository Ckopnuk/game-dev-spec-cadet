#ifndef GAME_OBJ_H
#define GAME_OBJ_H

#include <array>
#include <fstream>
#include <iostream>
#include <string_view>

#include "engine.h"


class Game_obj
{
public:
    Game_obj();
    virtual ~Game_obj();

    virtual Ckom::mat2x3 matrix() = 0;
    virtual void render() = 0;

    virtual void set_vertex_buf(std::string_view path, Ckom::Engine& eng);
    virtual void set_texture(std::string_view path, Ckom::Engine& eng);
    virtual void set_pos(const float x, const float y);

    virtual Ckom::Vertex_buffer* get_vertex_buffer() const;
    virtual Ckom::Texture* get_texture() const;
    virtual Ckom::vec2 get_pos() const;

protected:
    Ckom::Vertex_buffer* vertex_buffer = nullptr;
    Ckom::Texture* texture = nullptr;
    Ckom::Engine* engine = nullptr;
    Ckom::vec2 pos{0.f, 0.f};
};

#endif // GAME_OBJ_H
