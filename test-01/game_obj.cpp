#include "game_obj.h"

Game_obj::Game_obj()
{

}

Game_obj::~Game_obj(){}

Ckom::Vertex_buffer* Game_obj::get_vertex_buffer() const
{
    return vertex_buffer;
}

Ckom::Texture* Game_obj::get_texture() const
{
    return texture;
}

Ckom::vec2 Game_obj::get_pos() const
{
    return pos;
}

void Game_obj::set_pos(const float x, const float y)
{
    pos.x = x;
    pos.y = y;
}

void Game_obj::set_texture(std::string_view path, Ckom::Engine &eng)
{
    texture = eng.create_texture(path.data());
    if(nullptr == texture)
    {
        std::cerr << "fail load texture: " << path.data();
    }
}

void Game_obj::set_vertex_buf(std::string_view path, Ckom::Engine &eng)
{
    std::ifstream file(path.data());
    if(!file)
    {
        std::cerr << "CAN'T LOAD " << path.data() << std::endl;
    }
    else {
        std::array<Ckom::Triangle_pct, 2> triangles;
        file >> triangles[0] >> triangles[1];
        vertex_buffer = eng.create_vertex_buffer(&triangles[0], triangles.size());
        if(vertex_buffer == 0)
        {
            std::cerr << "Can't create vertex buffer: " << path.data() << std::endl;
        }
    }
    file.close();
}
