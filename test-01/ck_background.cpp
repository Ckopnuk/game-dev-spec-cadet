#include "ck_background.h"

Ck_Background::Ck_Background(Ckom::Engine* eng)
{
    engine = eng;
    if (engine == nullptr)
    {
        std::cerr << "can't create background: engine == nullptr" << std::endl;
    }

    set_vertex_buf("../vert_tex_color.txt", *eng);
    set_texture("/home/ckopnuk/projects/game-dev-spec-cadet/test-01/src/texture/background.png", *engine);
}

Ck_Background::~Ck_Background()
{

}



void Ck_Background::render()
{
    engine->render(*vertex_buffer, texture, matrix());
}

Ckom::mat2x3 Ck_Background::matrix()
{
    const float pi = std::numbers::pi_v<float>;
    Ckom::mat2x3 scale = Ckom::mat2x3::scale(1, 1 * (1024 / 760));
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(pi);
    Ckom::mat2x3 position = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res = scale * rotation * position;
    return res;
}
