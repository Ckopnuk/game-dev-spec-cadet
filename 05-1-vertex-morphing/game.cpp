#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.h"

int main(int, char*[])
{
    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
        Ckom::create_engine(), Ckom::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    std::ifstream file("vertexes.txt", std::ios_base::binary);
    //    assert(!!file);

    if (!file)
    {
        std::cerr << "Can't open file vertex.txt" << std::endl;
    }

    Ckom::Triangle trLeft1;
    Ckom::Triangle trRight1;

    Ckom::Triangle trLeft2;
    Ckom::Triangle trRight2;

    file >> trLeft1 >> trRight1 >> trLeft2 >> trRight2;

    float speed        = 0.5;
    bool  continueLoop = true;
    while (continueLoop)
    {
        Ckom::Event event;

        while (engine->readInput(event))
        {

            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continueLoop = false;
                    break;
                default:
                    break;
            }
        }

        float alpha = (std::sin(engine->getTime() * 0.001f) * 0.5f) + 0.5f;

        //        engine->morphing(trLeft1, trLeft2, alpha);

        //        engine->morphing(trRight1, trRight2, alpha);

        engine->renderTriangle(trLeft2);

        engine->swapBuffer();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}
