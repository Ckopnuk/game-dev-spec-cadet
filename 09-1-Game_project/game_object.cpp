#include "game_object.h"
#include <numbers>

std::istream& operator>>(std::istream& stream, Game_object& obj)
{
    std::string start_word;
    stream >> start_word;
    if (start_word != "object")
    {
        throw std::runtime_error("can't parse game object got: " + start_word);
    }

    stream >> obj.name;
    stream >> obj.type;

    float direction_in_grad = 0.0f;
    stream >> direction_in_grad;
    const float pi = std::numbers::pi_v<float>;
    obj.rotation   = direction_in_grad * (pi / 180.f);

    stream >> obj.position;
    stream >> obj.size;

    std::string key_word;
    stream >> key_word;
    if (key_word != "mesh")
    {
        throw std::runtime_error("can't parse game object mesh got: " +
                                 key_word);
    }

    stream >> obj.path_mesh;

    Ckom::Vertex_buffer* mesh =
        load_mesh_from_file_with_scale(obj.path_mesh, obj.size);
    assert(mesh != nullptr);
    obj.mesh = mesh;

    stream >> key_word;
    if (key_word != "texture")
    {
        throw std::runtime_error("can't parse game object texture got: " +
                                 key_word);
    }

    stream >> obj.path_texture;

    // Ckom::Texture* tex = Ckom::Engine::create_texture(obj.path_texture);
    // assert(tex != nullptr);

    return stream;
}

std::stringstream filter_coments(std::string_view file)
{
    std::stringstream out;
    std::string       line;
    std::ifstream     in(file.data(), std::ios_base::binary);

    if (!in)
    {
        throw std::runtime_error(std::string("can't open file: ") +
                                 file.data());
    }

    while (std::getline(in, line))
    {
        size_t comment_pos = line.find("//");
        if (comment_pos != std::string::npos)
        {
            line = line.substr(0, comment_pos);
        }
        if (!line.empty())
        {
            out << line << '\n';
        }
    }

    return out;
}

Ckom::Vertex_buffer* load_mesh_from_file_with_scale(const std::string_view path,
                                                    const Ckom::vec2& scale)
{
    std::stringstream file = filter_coments(path);
    if (!file)
    {
        throw std::runtime_error("can't load file in load mesh from file!!!");
    }

    size_t      num_of_vertexes = 0;
    std::string num_of_ver;

    file >> num_of_ver;

    if (num_of_ver != "num_of_vertexes")
    {
        throw std::runtime_error("no key word: num_of_vertexes");
    }

    file >> num_of_vertexes;

    std::vector<Ckom::Vertex_pct> vertexes;

    vertexes.reserve(num_of_vertexes);

    std::copy_n(std::istream_iterator<Ckom::Vertex_pct>(file), num_of_vertexes,
                std::back_inserter(vertexes));

    Ckom::mat2x3 scale_matrix = Ckom::mat2x3::scale(scale.x, scale.y);

    std::transform(vertexes.begin(), vertexes.end(), vertexes.begin(),
                   [&scale_matrix](Ckom::Vertex_pct vert) {
                       vert.p = vert.p * scale_matrix;
                       return vert;
                   });

    Ckom::Vertex_buffer = Ckom::
}
