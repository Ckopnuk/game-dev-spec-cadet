#ifndef ENEMY_H
#define ENEMY_H

#include "engine.h"

#include <cstdlib>
#include <iostream>
#include <string_view>
#include <vector>

struct Bullet
{
    float          speed     = 1.5f;
    float          direction = 0.f;
    Ckom::vec2     pos;
    Ckom::Texture* texture;
    float          radius = 0.04;
};

class Enemy
{
public:
    Enemy();
    virtual ~Enemy();

    virtual Ckom::Texture* get_texture()     = 0;
    virtual void           set_move(float x) = 0;
    virtual Ckom::vec2     get_pos()         = 0;
    virtual Ckom::mat2x3   matrix()          = 0;
    virtual float          get_speed()       = 0;
    virtual float          get_radius()      = 0;
    virtual int            get_dmg()         = 0;

    virtual void fire(Ckom::Texture* tex, const Ckom::vec2 position,
                      const float dir) = 0;

    std::vector<Bullet> bullets;
    //    virtual Enemy* production()          = 0;
protected:
};

class Enemy_litle_bug final : public Enemy
{
public:
    Enemy_litle_bug(Ckom::Texture* tex);
    ~Enemy_litle_bug();

    Ckom::Texture* get_texture() override final;
    void           set_move(float x) override final;
    Ckom::vec2     get_pos() override final;
    Ckom::mat2x3   matrix() override final;
    float          get_speed() override final;
    float          get_radius() override final;
    int            get_dmg() override final;

    void fire(Ckom::Texture* tex, const Ckom::vec2 position,
              const float dir) override final;

    //    Enemy* production() override;

private:
    //    std::vector<Bullet> bullets;
    Ckom::vec2     pos;
    Ckom::Texture* texture;
    float          speed  = 1;
    float          radius = 0.08;
    int            dmg    = 1;
};

class Enemy_avarge_bug final : public Enemy
{
public:
    Enemy_avarge_bug(Ckom::Texture* tex);
    ~Enemy_avarge_bug();

    Ckom::Texture* get_texture() override final;
    Ckom::vec2     get_pos() override final;
    Ckom::mat2x3   matrix() override final;
    float          get_speed() override final;
    float          get_radius() override final;
    int            get_dmg() override final;

    void set_move(float x) override;

    void fire(Ckom::Texture* tex, const Ckom::vec2 position,
              const float dir) override final;

    //    Enemy* production() override final;

private:
    //    std::vector<Bullet> bullets;
    Ckom::vec2     pos;
    Ckom::Texture* texture;
    float          speed  = 0.75;
    float          radius = 0.1;
    int            dmg    = 2;
};

class Enemy_big_boss_bug final : public Enemy
{
public:
    Enemy_big_boss_bug(Ckom::Texture* tex);
    ~Enemy_big_boss_bug();

    Ckom::Texture* get_texture() override final;
    Ckom::vec2     get_pos() override final;
    Ckom::mat2x3   matrix() override final;
    float          get_speed() override final;
    float          get_radius() override final;
    int            get_dmg() override final;

    void set_move(float x) override final;

    void fire(Ckom::Texture* tex, const Ckom::vec2 position,
              const float dir) override final;

    //    Enemy* production() override final;

private:
    //    std::vector<Bullet> bullets;
    Ckom::vec2     pos;
    Ckom::Texture* texture;
    float          speed  = 0.5;
    float          radius = 0.12;
    int            dmg    = 3;
};

#endif // ENEMY_H
