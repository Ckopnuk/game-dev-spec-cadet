#ifndef SPRITE_H
#define SPRITE_H

#include "engine.h"

struct Rect
{
    Ckom::vec2 pos;
    Ckom::vec2 size;
};

class Sprite
{
public:
    Sprite();
    Sprite(const Sprite&) = default;
    Sprite(const std::string_view id, Ckom::Texture* tex,
           const Rect& rect_on_texture, const Ckom::vec2& pos,
           const Ckom::vec2& size, const float angle);

    void draw(Ckom::Engine& render);

    Ckom::Texture* texture() const;
    void           texture(Ckom::Texture* t);

    const Rect& uv_rect() const;
    void        uv_rect(const Rect& r);

    /// center of sprite in world coordinates
    Ckom::vec2 pos() const;
    void       pos(const Ckom::vec2& p);

    /// width of sprite in world coordinates
    Ckom::vec2 size() const;
    void       size(const Ckom::vec2& s);

    /// angle of sprite in degrees
    float rotation() const;
    void  rotation(const float r);

    const std::string& id() const;
    void               id(std::string_view name);

private:
    std::string    id_;
    Ckom::Texture* texture_ = nullptr;
    Rect           uv_rect_;
    Ckom::vec2     pos_;
    Ckom::vec2     size_;
    float          rotation_ = 0; // degrees
};

inline bool operator==(const Rect& l, const Rect& r)
{
    return l.pos == r.pos && l.size == r.size;
}

inline bool operator==(const Sprite& l, const Sprite& r)
{
    return l.id() == r.id() && l.texture() == r.texture() &&
           l.uv_rect() == r.uv_rect() && l.pos() == r.pos() &&
           l.size() == r.size() &&
           std::abs(l.rotation() - r.rotation()) <= 0.000001f;
}

#endif // SPRITE_H
