#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <algorithm>
#include <cassert>
#include <fstream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>

#include "engine.h"

enum class Object_type
{
    level,
    enemy_litle,
    enemy_avarge,
    enemy_big,
    bonus,
    flor
};

struct Game_object
{
    std::string      name;
    enum Object_type type;
    float            rotation;
    Ckom::vec2       position;
    Ckom::vec2       size;
    std::string      path_mesh;
    std::string      path_texture;

    Ckom::Vertex_buffer* mesh    = nullptr;
    Ckom::Texture*       texture = nullptr;
};

inline std::istream& operator>>(std::istream& stream, Object_type& type);

Ckom::Vertex_buffer* load_mesh_from_file_with_scale(const std::string_view path,
                                                    const Ckom::vec2& scale);

std::istream& operator>>(std::istream& stream, Game_object& obj);

std::istream& operator>>(std::istream& stream, Object_type& type)
{
    static const std::map<std::string, Object_type> types = {
        { "level", Object_type::level },
        { "enemy_litle", Object_type::enemy_litle },
        { "enemy_avarge", Object_type::enemy_avarge },
        { "enemy_big", Object_type::enemy_big },
        { "bonus", Object_type::bonus },
        { "flor", Object_type::flor }
    };

    std::string type_name;
    stream >> type_name;

    auto it = types.find(type_name);

    if (it != types.end())
    {
        type = it->second;
    }
    else
    {
        std::stringstream ss;
        ss << "expected one of: ";
        std::for_each(types.begin(), types.end(),
                      [&ss](auto& kv) { ss << kv.first << ", "; });
        ss << " but got: " << type_name;
        throw std::runtime_error(ss.str());
    }

    return stream;
}

std::stringstream filter_comments(std::string_view file)
{
    std::stringstream out;
    std::string       line;
    std::ifstream     in(file.data(), std::ios_base::binary);

    if (!in)
    {
        throw std::runtime_error(std::string("can't open file: ") +
                                 file.data());
    }

    while (std::getline(in, line))
    {
        size_t comment_pos = line.find("//");
        if (comment_pos != std::string::npos)
        {
            line = line.substr(0, comment_pos);
        }
        if (!line.empty())
        {
            out << line << '\n';
        }
    }

    return out;
}

#endif // GAME_OBJECT_H
