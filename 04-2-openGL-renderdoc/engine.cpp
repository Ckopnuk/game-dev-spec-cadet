#include "engine.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <chrono>
#include <exception>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <vector>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>

PFNGLCREATESHADERPROC            glCreateShader            = nullptr;
PFNGLSHADERSOURCEPROC            glShaderSource            = nullptr;
PFNGLCOMPILESHADERPROC           glCompileShader           = nullptr;
PFNGLGETSHADERIVPROC             glGetShaderiv             = nullptr;
PFNGLGETSHADERINFOLOGPROC        glGetShaderInfoLog        = nullptr;
PFNGLDELETESHADERPROC            glDeleteShader            = nullptr;
PFNGLCREATEPROGRAMPROC           glCreateProgram           = nullptr;
PFNGLATTACHSHADERPROC            glAttachShader            = nullptr;
PFNGLBINDATTRIBLOCATIONPROC      glBindAttribLocation      = nullptr;
PFNGLLINKPROGRAMPROC             glLinkProgram             = nullptr;
PFNGLGETPROGRAMIVPROC            glGetProgramiv            = nullptr;
PFNGLGETPROGRAMINFOLOGPROC       glGetProgramInfoLog       = nullptr;
PFNGLDELETEPROGRAMPROC           glDeleteProgram           = nullptr;
PFNGLUSEPROGRAMPROC              glUseProgram              = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC     glVertexAttribPointer     = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
PFNGLVALIDATEPROGRAMPROC         glValidateProgram         = nullptr;

// RENDERDOC
PFNGLBINDBUFFERPROC      glBindBuffer      = nullptr;
PFNGLGENBUFFERSPROC      glGenBuffers      = nullptr;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
PFNGLBUFFERDATAPROC      glBufferData      = nullptr;
// RENDERDOC

template <typename T>
static void load_gl_function(const char* funcName, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(funcName);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(std::string("can't load gl function") +
                                 funcName);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#define CKOM_GL_CHECK()                                                        \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

namespace Ckom
{
// static void APIENTRY
// callbackOpenGLDebug(GLenum source,
//                    GLenum type,
//                    GLuint id,
//                    GLenum severity,
//                    GLsizei length,
//                    const GLchar* message,
//                    [[maybe_unused]] const void* userParam);

static std::array<std::string_view, 21> eventNames = {
    { "left_pressed",
      "left_released",
      "right_pressed",
      "right_released",
      "up_pressed",
      "up_released",
      "down_pressed",
      "down_released",
      "select_pressed",
      "select_released",
      "start_pressed",
      "start_released",
      "button_triangle_pressed",
      "button_triangle_released",
      "button_square_pressed",
      "button_square_released",
      "button_cross_pressed",
      "button_cross_released",
      "button_circle_pressed",
      "button_circle_released",
      "turn_off" }
};

std::ostream& operator<<(std::ostream& fout, Event event)
{
    std::uint32_t value = static_cast<std::uint32_t>(event);
    std::uint32_t min   = static_cast<std::uint32_t>(Event::left_pressed);
    std::uint32_t max   = static_cast<std::uint32_t>(Event::turn_off);
    if (value >= min && value <= max)
    {
        fout << eventNames[value];
        return fout;
    }
    else
    {
        throw std::runtime_error("too big event value");
    }
}

static std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

std::istream& operator>>(std::istream& is, Vertex& v)
{
    is >> v.x;
    is >> v.y;
    is >> v.z;

    is >> v.r;
    is >> v.g;
    is >> v.b;

    return is;
}

std::istream& operator>>(std::istream& is, Triangle& tr)
{
    is >> tr.v[0];
    is >> tr.v[1];
    is >> tr.v[2];
    return is;
}

struct myBind
{
    myBind(SDL_KeyCode k, std::string_view str, Event press, Event releas)
        : keyCode(k)
        , keyName(str)
        , eventPress(press)
        , eventReleas(releas)
    {
    }

    SDL_KeyCode      keyCode;
    std::string_view keyName;
    Event            eventPress;
    Event            eventReleas;
};

const std::array<myBind, 10> keysArr{
    { { SDLK_w, "up", Event::up_pressed, Event::up_released },
      { SDLK_a, "left", Event::left_pressed, Event::left_released },
      { SDLK_s, "down", Event::down_pressed, Event::down_released },
      { SDLK_d, "right", Event::right_pressed, Event::right_released },
      { SDLK_i, "triangle", Event::button_triangle_pressed,
        Event::button_triangle_released },
      { SDLK_j, "square", Event::button_square_pressed,
        Event::button_square_released },
      { SDLK_k, "cross", Event::button_cross_pressed,
        Event::button_cross_released },
      { SDLK_l, "circle", Event::button_circle_pressed,
        Event::button_circle_released },
      { SDLK_p, "statr", Event::start_pressed, Event::start_released },
      { SDLK_ESCAPE, "select", Event::select_pressed, Event::select_released } }
};

static bool checkInput(const SDL_Event& ev, const myBind*& result)
{
    const auto it = std::find_if(
        std::begin(keysArr), std::end(keysArr),
        [&](const myBind& bind) { return bind.keyCode == ev.key.keysym.sym; });

    if (it != end(keysArr))
    {
        result = &(*it);
        return true;
    }
    return false;
}

class EngineImpl final : public Engine
{
public:
    std::string initialize(std::string_view /*conf*/) override final;
    bool        readInput(Event& ev) override final;
    void        renderTriangle(const Triangle& tr) override final;
    void        swapBuffer() override final;
    void        uninitialize() final;

private:
    SDL_Window*   window     = nullptr;
    SDL_GLContext gl_context = nullptr;
    GLuint        program_id = 0;
};

static bool alreadyExist = false;

Engine* create_engine()
{
    if (alreadyExist)
    {
        throw std::runtime_error("is already exist");
    }
    Engine* result = new EngineImpl;
    alreadyExist   = true;
    return result;
}

void destroy_engine(Engine* eng)
{
    if (alreadyExist == false)
    {
        throw std::runtime_error("engine not created!!!");
    }
    if (nullptr == eng)
    {
        throw std::runtime_error("eng iss nullptr!!!");
    }
    delete eng;
}

Engine::~Engine() {}

std::string EngineImpl::initialize(std::string_view /*conf*/)
{
    std::stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version link     = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(link.major, link.minor, link.patch))
    {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << ' ' << link << std::endl;
    }

    const int initResult = SDL_Init(SDL_INIT_EVERYTHING);
    if (initResult != 0)
    {
        const char* errMsg = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << errMsg << std::endl;
        return serr.str();
    }

    //  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    window =
        SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 640, 480, ::SDL_WINDOW_OPENGL);

    if (window == nullptr)
    {
        const char* errMsg = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << errMsg << std::endl;
        SDL_Quit();
        return serr.str();
    }
    // for render
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    // for renfer
    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        std::string msg("Can't create openGL context: ");
        msg += SDL_GetError();
        serr << msg << std::endl;
        return serr.str();
    }

    int glMajor = 0;
    int result  = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &glMajor);
    assert(result == 0);
    int glMinor = 0;
    result      = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &glMinor);
    assert(result == 0);

    std::cout << "GL_context: " << glMajor << '.' << glMinor << std::endl;

    if (glMajor <= 2 && glMinor < 1)
    {
        serr << "Corrent openGL context version: " << glMajor << ':' << glMinor
             << '\n'
             << "Need openGl version at least 2:1\n"
             << std::flush;
        return serr.str();
    }

    try
    {
        load_gl_function("glCreateShader", glCreateShader);
        load_gl_function("glShaderSource", glShaderSource);
        load_gl_function("glCompileShader", glCompileShader);
        load_gl_function("glGetShaderiv", glGetShaderiv);
        load_gl_function("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_function("glDeleteShader", glDeleteShader);
        load_gl_function("glCreateProgram", glCreateProgram);
        load_gl_function("glAttachShader", glAttachShader);
        load_gl_function("glBindAttribLocation", glBindAttribLocation);
        load_gl_function("glLinkProgram", glLinkProgram);
        load_gl_function("glGetProgramiv", glGetProgramiv);
        load_gl_function("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_function("glDeleteProgram", glDeleteProgram);
        load_gl_function("glUseProgram", glUseProgram);
        load_gl_function("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_function("glEnableVertexAttribArray",
                         glEnableVertexAttribArray);
        load_gl_function("glValidateProgram", glValidateProgram);
        load_gl_function("glBindBuffer", glBindBuffer);
        load_gl_function("glGenBuffers", glGenBuffers);
        load_gl_function("glGenVertexArrays", glGenVertexArrays);
        load_gl_function("glBindVertexArray", glBindVertexArray);
        load_gl_function("glBufferData", glBufferData);
    }
    catch (std::exception& ex)
    {
        return ex.what();
    }

    GLuint vertex_buffer = 0;
    glGenBuffers(1, &vertex_buffer);
    CKOM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    CKOM_GL_CHECK()
    GLuint vertex_array_obj = 0;
    glGenVertexArrays(1, &vertex_array_obj);
    CKOM_GL_CHECK()
    glBindVertexArray(vertex_array_obj);
    CKOM_GL_CHECK()

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    CKOM_GL_CHECK()
    std::string_view vertex_shader_source = R"(#version 330 core
                                    layout (location = 0) in vec3 a_position;
                                    layout (location = 1) in vec3 a_color;
                                    out vec4 v_position;
                                    out vec3 v_color;
                                    void main()
                                    {
                                        v_position = vec4(a_position, 1.0);
                                        v_color = a_color;
                                        gl_Position = v_position;
                                    }
                                    )";

    const char* source = vertex_shader_source.data();
    glShaderSource(vertex_shader, 1, &source, nullptr);
    CKOM_GL_CHECK()

    glCompileShader(vertex_shader);
    CKOM_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compiled_status);
    CKOM_GL_CHECK()
    if (compiled_status == 0)
    {
        GLint info_len = 0;
        glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &info_len);
        CKOM_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(vertex_shader, info_len, nullptr, info_chars.data());
        CKOM_GL_CHECK()
        glDeleteShader(vertex_shader);
        CKOM_GL_CHECK()

        std::string shader_type_name = "Vertex";
        serr << "Error compiling shader(Vertex)\n"
             << vertex_shader_source << '\n'
             << info_chars.data();
        return serr.str();
    }

    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    CKOM_GL_CHECK()
    std::string_view fragment_shader_source = R"(#version 330 core
                                      in vec4 v_position;
                                      in vec3 v_color;
                                      out vec4 FragColor;
                                      void main()
                                      {
                                          FragColor = vec4(v_color, 1.0);
                                          /*
                                          if (v_position.z >= 0.0)
                                          {
                                            float light_green = 0.5 + v_position.z / 2.0;
                                            FragColor = vec4(0.0, light_green, 0.0, 1.0);
                                          } else
                                          {
                                            float dark_green = 0.5 - (v_position.z / -2.0);
                                            FragColor = vec4(0.0, dark_green, 0.0, 1.0);
                                          }
                                          */
                                      }
                                      )";
    source                                  = fragment_shader_source.data();
    glShaderSource(fragment_shader, 1, &source, nullptr);
    CKOM_GL_CHECK()

    glCompileShader(fragment_shader);
    CKOM_GL_CHECK()

    compiled_status = 0;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compiled_status);
    CKOM_GL_CHECK()
    if (compiled_status == 0)
    {
        GLint info_len = 0;
        glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &info_len);
        CKOM_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(fragment_shader, info_len, nullptr,
                           info_chars.data());
        CKOM_GL_CHECK()

        glDeleteShader(fragment_shader);
        CKOM_GL_CHECK()

        serr << "Error compiling shader(Fragment)\n"
             << vertex_shader_source << '\n'
             << info_chars.data();
        return serr.str();
    }

    program_id = glCreateProgram();
    CKOM_GL_CHECK()
    if (program_id == 0)
    {
        serr << "Filed to create glProgram";
        return serr.str();
    }

    glAttachShader(program_id, vertex_shader);
    CKOM_GL_CHECK()
    glAttachShader(program_id, fragment_shader);
    CKOM_GL_CHECK()

    glBindAttribLocation(program_id, 0, "a_position");
    CKOM_GL_CHECK()
    glBindAttribLocation(program_id, 1, "a_color");
    CKOM_GL_CHECK()

    glLinkProgram(program_id);
    CKOM_GL_CHECK()

    GLint link_stat = 0;
    glGetProgramiv(program_id, GL_LINK_STATUS, &link_stat);
    CKOM_GL_CHECK()
    if (link_stat == 0)
    {
        GLint info_len = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_len);
        CKOM_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(info_len));
        glGetProgramInfoLog(program_id, info_len, nullptr, infoLog.data());
        CKOM_GL_CHECK()
        serr << "Error linked program:\n" << infoLog.data();
        glDeleteProgram(program_id);
        CKOM_GL_CHECK()
        return serr.str();
    }

    glUseProgram(program_id);
    CKOM_GL_CHECK()

    glEnable(GL_DEPTH_TEST);
    CKOM_GL_CHECK()

    return "";
}
// to do

bool EngineImpl::readInput(Event& event)
{
    SDL_Event sdlEvent;

    if (SDL_PollEvent(&sdlEvent))
    {
        const myBind* binding = nullptr;
        if (sdlEvent.type == SDL_QUIT)
        {
            event = Event::turn_off;
            return true;
        }
        else if (sdlEvent.type == SDL_KEYDOWN)
        {
            if (checkInput(sdlEvent, binding))
            {
                event = binding->eventPress;
                return true;
            }
        }
        else if (sdlEvent.type == SDL_KEYUP)
        {
            if (checkInput(sdlEvent, binding))
            {
                event = binding->eventReleas;
                return true;
            }
        }
    }
    return false;
}

void EngineImpl::renderTriangle(const Triangle& tr)
{
    // RENDER DOC addition ////////////////////
    glBufferData(GL_ARRAY_BUFFER, sizeof(tr), &tr, GL_STATIC_DRAW);
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(0);

    GLintptr position_attr_offset = 0;

    CKOM_GL_CHECK()
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          reinterpret_cast<void*>(position_attr_offset));
    CKOM_GL_CHECK()
    glEnableVertexAttribArray(1);
    CKOM_GL_CHECK()

    GLintptr color_attr_offset = sizeof(float) * 3;

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
                          reinterpret_cast<void*>(color_attr_offset));
    CKOM_GL_CHECK()
    glValidateProgram(program_id);
    CKOM_GL_CHECK()
    // Check the validate status
    GLint validate_status = 0;
    glGetProgramiv(program_id, GL_VALIDATE_STATUS, &validate_status);
    CKOM_GL_CHECK()
    if (validate_status == GL_FALSE)
    {
        GLint infoLen = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &infoLen);
        CKOM_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id, infoLen, nullptr, infoLog.data());
        CKOM_GL_CHECK()
        std::cerr << "Error linking program:\n" << infoLog.data();
        throw std::runtime_error("error");
    }
    glDrawArrays(GL_TRIANGLES, 0, 3);
    CKOM_GL_CHECK()
}

void EngineImpl::swapBuffer()
{
    SDL_GL_SwapWindow(window);

    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    CKOM_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    CKOM_GL_CHECK()
}

void EngineImpl::uninitialize()
{
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

} // end namespace Ckom
