#include <SDL2/SDL_version.h>

#include <cstdlib>
#include <iostream>

std::ostream &operator<<(std::ostream &mout, const SDL_version vers) {
  mout << static_cast<int>(vers.major) << '.';
  mout << static_cast<int>(vers.minor) << '.';
  mout << static_cast<int>(vers.patch) << std::endl;
  return mout;
}

int main(int, char **) {
  using std::cout;
  using std::endl;
  SDL_version compiled = {0, 0, 0};
  SDL_version linked = {0, 0, 0};

  SDL_VERSION(&compiled);
  SDL_GetVersion(&linked);

  cout << "Compiled: " << compiled;
  cout << "Linced: " << linked << endl;

  return cout.fail();
}
