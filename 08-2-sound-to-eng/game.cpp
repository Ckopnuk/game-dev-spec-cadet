#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <numbers>
#include <sstream>
#include <string_view>
#include <vector>

#pragma GCC diagnostic push
// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wall"
#include "src/imgui/imgui.h"
#pragma GCC diagnostic pop

#include "enemy.h"
#include "engine.h"
#include "sprite.h"
#include "sprite_reader.h"

#include "ani2d.h"

#include <chrono>

int main(int /*argc*/, char* /*argv*/[])
{
    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
        Ckom::create_engine(), Ckom::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    Ckom::Texture* textureBug = engine->create_texture("bug.png");
    if (nullptr == textureBug)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }
    Ckom::Texture* textureEvil = engine->create_texture("enemy2.png");
    if (nullptr == textureEvil)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }
    Ckom::Texture* texture_evil_1 = engine->create_texture("enemy3.png");
    if (nullptr == texture_evil_1)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }
    Ckom::Texture* texture_evil_2 = engine->create_texture("enemy1.png");
    if (nullptr == texture_evil_2)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }
    Ckom::Texture* texture_bullet = engine->create_texture("bullet2.png");
    if (nullptr == texture_bullet)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }
    Ckom::Texture* textureCannon = engine->create_texture("cannon1.png");
    if (nullptr == textureCannon)
    {
        std::cerr << "failed load texture\n";
        return EXIT_FAILURE;
    }
    Ckom::Texture* fon_texture = engine->create_texture("floor.jpeg");
    if (nullptr == fon_texture)
    {
        std::cerr << "failed load earth texture!\n";
        return EXIT_FAILURE;
    }

    Ckom::Vertex_buffer* vertex_buf_b = nullptr;
    Ckom::Vertex_buffer* vert_buf_fon = nullptr;

    std::ifstream file("vert_tex_color.txt");
    if (!file)
    {
        std::cerr << "Can't load vert_tex_color.txt" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::array<Ckom::Triangle_pct, 2> triangles;
        file >> triangles[0] >> triangles[1];
        vertex_buf_b =
            engine->create_vertex_buffer(&triangles[0], triangles.size());
        if (vertex_buf_b == 0)
        {
            std::cerr << "can't create bug vertex bufer";
            return EXIT_FAILURE;
        }
    }
    file.close();
    file.open("vert_pos_color.txt");

    if (!file)
    {
        std::cerr << "Can't load vert_pos_color.txt!" << std::endl;
        return EXIT_FAILURE;
    }
    else
    {
        std::array<Ckom::Triangle_pct, 10000> trian;
        file >> trian[0] >> trian[1];
        int size_n = 1;
        int line   = 0;
        for (size_t j = 0; j < 10; ++j)
        {
            for (size_t i = 0; i < 18; ++i)
            {
                size_n++;
                trian[size_n] = trian[size_n - 2];
                trian[size_n].v[0].p.x += 0.2;
                trian[size_n].v[1].p.x += 0.2;
                trian[size_n].v[2].p.x += 0.2;
                line = i + 3;
            }
            if (j < 9)
            {
                size_n += 1;
                trian[size_n] = trian[size_n - line];
                trian[size_n].v[0].p.y -= 0.2;
                trian[size_n].v[1].p.y -= 0.2;
                trian[size_n].v[2].p.y -= 0.2;

                size_n += 1;
                trian[size_n] = trian[size_n - line];
                trian[size_n].v[0].p.y -= 0.2;
                trian[size_n].v[1].p.y -= 0.2;
                trian[size_n].v[2].p.y -= 0.2;
            }
        }

        vert_buf_fon = engine->create_vertex_buffer(&trian[0], trian.size());
        if (vert_buf_fon == 0)
        {
            std::cerr << "can't create fon vertex bufer";
            return EXIT_FAILURE;
        }
    }

    //    Ckom::Sound_buffer* sound =
    //        engine->create_sound_buffer("t2_no_problemo.wav");

    bool continue_loop = true;

    Ckom::vec2          current_bug_pos(0.f, 0.f);
    Ckom::vec2          evilpos(-1.0, 0.0);
    float               current_bug_dir(0.f);
    float               current_cannon_dir(0.f);
    const float         pi    = std::numbers::pi_v<float>;
    const float         speed = 0.008f;
    std::vector<Enemy*> hive;
    std::vector<Bullet> bullets;
    //    std::vector<Bullet> bullets_bugs;
    int main_hp = 100;

    while (continue_loop)
    {
        Ckom::Event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continue_loop = false;
                    break;
                default:

                    break;
            }
        }

        if (main_hp <= 0)
        {
            continue_loop = false;
            std::cerr << "Game Over!!!" << std::endl;
        }

        if (engine->is_key_down(Ckom::Keys::down) &&
            engine->is_key_down(Ckom::Keys::left))
        {
            current_bug_dir = pi * 0.75;
            current_bug_pos.x += speed * -sin(current_bug_dir);
            current_bug_pos.y += speed * cos(current_bug_dir);
        }
        else if (engine->is_key_down(Ckom::Keys::down) &&
                 engine->is_key_down(Ckom::Keys::right))
        {
            current_bug_dir = -pi * 0.75;
            current_bug_pos.x += speed * -sin(current_bug_dir);
            current_bug_pos.y += speed * cos(current_bug_dir);
        }
        else if (engine->is_key_down(Ckom::Keys::up) &&
                 engine->is_key_down(Ckom::Keys::right))
        {
            current_bug_dir = -pi * 0.25;
            current_bug_pos.x += speed * -sin(current_bug_dir);
            current_bug_pos.y += speed * cos(current_bug_dir);
        }
        else if (engine->is_key_down(Ckom::Keys::up) &&
                 engine->is_key_down(Ckom::Keys::left))
        {
            current_bug_dir = pi * 0.25;
            current_bug_pos.x += speed * -sin(current_bug_dir);
            current_bug_pos.y += speed * cos(current_bug_dir);
        }

        else if (engine->is_key_down(Ckom::Keys::left))
        {
            current_bug_dir = pi * 0.5;
            current_bug_pos.x -= speed;
        }

        else if (engine->is_key_down(Ckom::Keys::up))
        {
            current_bug_dir = 0;
            current_bug_pos.y += speed;
        }

        else if (engine->is_key_down(Ckom::Keys::right))
        {
            current_bug_dir = -pi * 0.5f;
            current_bug_pos.x += speed;
        }

        else if (engine->is_key_down(Ckom::Keys::down))
        {
            current_bug_dir = pi;
            current_bug_pos.y -= speed;
        }

        if (engine->is_key_down(Ckom::Keys::left_a))
        {
            current_cannon_dir += pi * 0.02;
            //            sound->play(Ckom::Sound_buffer::properties::once);
        }
        else if (engine->is_key_down(Ckom::Keys::right_a))
        {
            current_cannon_dir -= pi * 0.02;
            //            sound->play(Ckom::Sound_buffer::properties::looped);
        }

        if (engine->is_key_down(Ckom::Keys::space))
        {
            Enemy* enemy = new Enemy_litle_bug(textureEvil);
            hive.push_back(enemy);
            if (hive.size() % 3 == 0)
            {
                Enemy* enemy_avarge = new Enemy_avarge_bug(texture_evil_1);
                hive.push_back(enemy_avarge);
            }
            else if (hive.size() % 5 == 0)
            {
                Enemy* enemy_big = new Enemy_big_boss_bug(texture_evil_2);
                hive.push_back(enemy_big);
            }
        }
        if (engine->is_key_down(Ckom::Keys::up_a))
        {
            Bullet bullet;
            bullet.texture   = texture_bullet;
            bullet.pos       = current_bug_pos;
            bullet.direction = current_cannon_dir;
            bullets.push_back(bullet);
        }

        Ckom::mat2x3 move   = Ckom::mat2x3::moove(current_bug_pos);
        Ckom::mat2x3 aspect = Ckom::mat2x3::scale(
            0.2, 0.2 * (engine->screen_size().x / engine->screen_size().y));
        Ckom::mat2x3 rot = Ckom::mat2x3::rotation(current_bug_dir);
        Ckom::mat2x3 m   = aspect * rot * move;

        Ckom::mat2x3 asp_for_cannon = Ckom::mat2x3::scale(
            0.07, 0.07 * (engine->screen_size().x / engine->screen_size().y));
        Ckom::mat2x3 rotate_cannon = Ckom::mat2x3::rotation(current_cannon_dir);
        Ckom::mat2x3 m_cannon      = asp_for_cannon * rotate_cannon * move;

        Ckom::mat2x3 scale_f = Ckom::mat2x3::scale(1, 1);
        Ckom::mat2x3 matr    = scale_f;

        evilpos.x += 0.0001;
        //        ImGui::NewFrame();
        engine->render(*vert_buf_fon, fon_texture, matr);
        engine->render(*vertex_buf_b, textureBug, m);
        //        ImGui::Render();
        if (!bullets.empty() && !hive.empty())
        {
            for (size_t i = 0; i < bullets.size(); ++i)
            {
                for (size_t j = 0; j < hive.size(); ++j)
                {
                    double radius = bullets[i].radius + hive[j]->get_radius();
                    double xa     = bullets[i].pos.x;
                    double ya     = bullets[i].pos.y;
                    double xb     = hive[j]->get_pos().x;
                    double yb     = hive[j]->get_pos().y;
                    double distance =
                        sqrt(pow((xb - xa), 2) + pow((yb - ya), 2));
                    if (radius >= distance)
                    {
                        delete hive[j];
                        hive.erase(hive.begin() + j);
                        bullets.erase(bullets.begin() + i);
                    }
                }
            }
        }

        if (!bullets.empty())
        {
            for (size_t i = 0; i < bullets.size(); ++i)
            {
                bullets[i].pos.x +=
                    (speed * bullets[i].speed) * cos(bullets[i].direction);
                bullets[i].pos.y -=
                    (speed * bullets[i].speed) * -sin(bullets[i].direction);
                Ckom::mat2x3 move_b = Ckom::mat2x3::moove(bullets[i].pos);
                Ckom::mat2x3 asp =
                    Ckom::mat2x3::scale(0.08, 0.08 * (engine->screen_size().x /
                                                      engine->screen_size().y));
                Ckom::mat2x3 rotate_b =
                    Ckom::mat2x3::rotation(bullets[i].direction);
                Ckom::mat2x3 result_m = asp * rotate_b * move_b;
                engine->render(*vertex_buf_b, bullets[i].texture, result_m);
            }
        }

        engine->render(*vertex_buf_b, textureCannon, m_cannon);

        if (!hive.empty())
        {
            for (size_t i = 0; i < hive.size(); ++i)
            {
                if (hive[i]->get_pos().x >= 1)
                {
                    main_hp -= hive[i]->get_dmg();
                    delete hive[i];
                    hive.erase(hive.begin() + (i));
                }
                else
                {
                    hive[i]->set_move(speed * hive[i]->get_speed());
                    if (hive[i]->bullets.size() <= 1)
                    {
                        hive[i]->fire(texture_bullet, hive[i]->get_pos(), pi);
                    }
                    engine->render(*vertex_buf_b, hive[i]->get_texture(),
                                   hive[i]->matrix());
                    if (!hive[i]->bullets.empty())
                    {
                        for (size_t j = 0; j < hive[i]->bullets.size(); ++j)
                        {
                            hive[i]->bullets[j].pos.x +=
                                speed * hive[i]->bullets[j].speed;

                            Ckom::mat2x3 move_bullet =
                                Ckom::mat2x3::moove(hive[i]->bullets[j].pos);
                            Ckom::mat2x3 rotate_bullet = Ckom::mat2x3::rotation(
                                hive[i]->bullets[j].direction);
                            Ckom::mat2x3 asp = Ckom::mat2x3::scale(
                                0.08, 0.08 * (engine->screen_size().x /
                                              engine->screen_size().y));
                            Ckom::mat2x3 res =
                                asp * rotate_bullet * move_bullet;
                            engine->render(*vertex_buf_b,
                                           hive[i]->bullets[j].texture, res);
                        }
                    }
                }
            }
        }

        //        if (!bullets_bugs.empty())
        //        {
        //            for (size_t i = 0; i < bullets_bugs.size(); ++i)
        //            {
        //                bullets_bugs[i].pos.x += speed *
        //                bullets_bugs[i].speed;

        //                Ckom::mat2x3 move_b =
        //                Ckom::mat2x3::moove(bullets_bugs[i].pos); Ckom::mat2x3
        //                asp =
        //                    Ckom::mat2x3::scale(0.08, 0.08 *
        //                    (engine->screen_size().x /
        //                                                      engine->screen_size().y));
        //                Ckom::mat2x3 rotate_b = Ckom::mat2x3::rotation(
        //                    bullets_bugs[i].direction - pi * 0.5);
        //                Ckom::mat2x3 result_m = asp * rotate_b * move_b;
        //                engine->render(*vertex_buf_b, bullets_bugs[i].texture,
        //                               result_m);

        //                if (bullets_bugs[i].pos.x >= 1)
        //                {
        //                    bullets_bugs.erase(bullets_bugs.begin() + i);
        //                }
        //            }
        //        }
        std::cout << "HP is: " << main_hp << std::endl;
        engine->swap_buffers();
    }

    for (size_t i = 0; i < hive.size(); ++i)
    {
        delete hive[i];
    }
    hive.clear();
    engine->uninitialize();

    return EXIT_SUCCESS;
}

// class timer
//{
// public:
//    timer()
//        : m_beg(clock_t::now())
//    {
//    }

//    void reset() { m_beg = clock_t::now(); }

//    float elapsed() const
//    {
//        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg)
//            .count();
//    }

// private:
//    // Type aliases to make accessing nested type easier
//    using clock_t  = std::chrono::high_resolution_clock;
//    using second_t = std::chrono::duration<float, std::ratio<1>>;

//    std::chrono::time_point<clock_t> m_beg;
//};
