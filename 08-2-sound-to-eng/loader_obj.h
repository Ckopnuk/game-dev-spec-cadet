#ifndef LOADER_OBJ_H
#define LOADER_OBJ_H

#include <string>
#include <vector>

class Loader_obj final
{
public:
    explicit Loader_obj(std::istream& byte_stream) noexcept(false);

    struct Vertex
    {
        float x;
        float y;
        float z;
        float w;
    };

    const std::vector<Vertex>& vertexes() const;

    struct Tex_coord
    {
        float u;
        float v;
        float w;
    };

    const std::vector<Tex_coord>& tex_coords() const;

    struct Normal
    {
        float x;
        float y;
        float z;
    };

    const std::vector<Normal>& normals() const;

    enum class Poin_type
    {
        only_vertex_index,
        vertex_and_texture_indexes,
        vertex_and_normal_indexes,
        vertex_texture_normal_indexes
    };

    struct only_v
    {
        int v;
    };

    struct v_vt
    {
        int v;
        int vt;
    };

    struct v_vn
    {
        int v;
        int vn;
    };

    struct v_vt_vn
    {
        int v;
        int vt;
        int vn;
    };

    struct Point
    {
        union
        {
            only_v  v;
            v_vt    vt;
            v_vn    vn;
            v_vt_vn vtn;
        };
        Poin_type type;
    };

    struct Face
    {
        Point p0;
        Point p1;
        Point p2;
    };

    const std::vector<Face>& faces() const;

    const std::string& name() const;

private:
    std::string            obj_name;
    std::vector<Vertex>    vertexes_;
    std::vector<Tex_coord> texture_coords;
    std::vector<Normal>    normals_;
    std::vector<Face>      faces_;
};

inline const std::vector<Loader_obj::Vertex>& Loader_obj::vertexes() const
{
    return vertexes_;
}

inline const std::vector<Loader_obj::Tex_coord>& Loader_obj::tex_coords() const
{
    return texture_coords;
}

inline const std::vector<Loader_obj::Normal>& Loader_obj::normals() const
{
    return normals_;
}

inline const std::vector<Loader_obj::Face>& Loader_obj::faces() const
{
    return faces_;
}

inline const std::string& Loader_obj::name() const
{
    return obj_name;
}

#endif // LOADER_OBJ_H
