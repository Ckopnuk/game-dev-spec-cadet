#include "loader_obj.h"

#include <algorithm>
//#include <charconv>
//#include <cstdlib>
//#include <iostream>
//#include <iterator>
//#include <string>

std::vector<std::string_view> split_line_into_words(std::string_view line,
                                                    const char       delimetr)
{
    if (line.empty())
    {
        return {};
    }

    size_t count_spaces =
        std::count(std::begin(line), std::end(line), delimetr);

    std::vector<size_t> start_next_word;
    start_next_word.reserve(count_spaces + 1);
    start_next_word.push_back(0); // first index

    size_t i = 0;

    auto copy_space_index = [&start_next_word, &i, delimetr](char value) {
        ++i;
        if (value == delimetr)
        {
            start_next_word.push_back(i);
        }
    };

    std::for_each(std::begin(line), std::end(line), copy_space_index);

    std::vector<std::string_view> result;
    result.reserve(count_spaces + 1);

    std::transform(std::begin(start_next_word), --std::end(start_next_word),
                   ++std::begin(start_next_word), std::back_inserter(result),
                   [&line](size_t first_indx, size_t last_indx) {
                       return line.substr(first_indx, --last_indx - first_indx);
                   });

    auto last_word = line.substr(start_next_word.back());
    result.push_back(last_word);

    return result;
}
