#include "enemy.h"
#include <math.h>
#include <numbers>

const float pi = std::numbers::pi_v<float>;

Enemy_litle_bug::Enemy_litle_bug(Ckom::Texture* tex)
{
    texture = tex;
    pos     = { -1, static_cast<float>(std::cos(std::rand())) };
}

Enemy_litle_bug::~Enemy_litle_bug()
{
    std::cout << "enemy destroy" << std::endl;
}

Ckom::Texture* Enemy_litle_bug::get_texture()
{
    return texture;
}

void Enemy_litle_bug::set_move(float x)
{
    pos.x += x;
}

Ckom::vec2 Enemy_litle_bug::get_pos()
{
    return pos;
}

Ckom::mat2x3 Enemy_litle_bug::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.2, 0.2 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(-pi * 0.5f);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

float Enemy_litle_bug::get_speed()
{
    return speed;
}

float Enemy_litle_bug::get_radius()
{
    return radius;
}

int Enemy_litle_bug::get_dmg()
{
    return dmg;
}

void Enemy_litle_bug::fire(Ckom::Texture* tex, const Ckom::vec2 position,
                           const float dir)
{
    //    Bullet bullet;
    //    bullet.texture   = tex;
    //    bullet.pos       = position;
    //    bullet.direction = dir;
    //    bullet.speed     = 1.2f;
    //    bullets.push_back(bullet);
}

Enemy::Enemy() {}

Enemy::~Enemy() {}

///////////////////////// avarge /////////////////////////////////

Enemy_avarge_bug::Enemy_avarge_bug(Ckom::Texture* tex)
{
    texture = tex;
    pos     = { -1, static_cast<float>(std::cos(std::rand())) };
}

Enemy_avarge_bug::~Enemy_avarge_bug()
{
    std::cerr << "avarge destroy" << std::endl;
}

Ckom::Texture* Enemy_avarge_bug::get_texture()
{
    return texture;
}

Ckom::vec2 Enemy_avarge_bug::get_pos()
{
    return pos;
}

Ckom::mat2x3 Enemy_avarge_bug::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.2, 0.2 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(-pi * 0.5f);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

float Enemy_avarge_bug::get_speed()
{
    return speed;
}

float Enemy_avarge_bug::get_radius()
{
    return radius;
}

int Enemy_avarge_bug::get_dmg()
{
    return dmg;
}

void Enemy_avarge_bug::set_move(float x)
{
    pos.x += x;
}

void Enemy_avarge_bug::fire(Ckom::Texture* tex, const Ckom::vec2 position,
                            const float dir)
{
    Bullet bullet;
    bullet.texture   = tex;
    bullet.pos       = position;
    bullet.direction = dir;
    bullet.speed     = 1.2f;
    bullets.push_back(bullet);
}

/////////////////////////////// big bos /////////////////////////////////

Enemy_big_boss_bug::Enemy_big_boss_bug(Ckom::Texture* tex)
{
    texture = tex;
    pos     = { -1, static_cast<float>(std::cos(std::rand())) };
}

Enemy_big_boss_bug::~Enemy_big_boss_bug()
{
    std::cerr << "BIG BOS DESTRUCTOR" << std::endl;
}

Ckom::Texture* Enemy_big_boss_bug::get_texture()
{
    return texture;
}

Ckom::vec2 Enemy_big_boss_bug::get_pos()
{
    return pos;
}

Ckom::mat2x3 Enemy_big_boss_bug::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.2, 0.2 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(-pi * 0.5f);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

float Enemy_big_boss_bug::get_speed()
{
    return speed;
}

float Enemy_big_boss_bug::get_radius()
{
    return radius;
}

int Enemy_big_boss_bug::get_dmg()
{
    return dmg;
}

void Enemy_big_boss_bug::set_move(float x)
{
    pos.x += x;
}

void Enemy_big_boss_bug::fire(Ckom::Texture* tex, const Ckom::vec2 position,
                              const float dir)
{
    Bullet bullet;
    bullet.texture   = tex;
    bullet.pos       = position;
    bullet.direction = dir;
    bullets.push_back(bullet);
}
