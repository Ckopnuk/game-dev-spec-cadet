#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SDL2/SDL.h>
#include <iostream>
#include <vector>

#include "my_math.h"

// void key_callback(GLFWwindow* window, int key, int scancode, int action,
//                  int mode);

int main()
{
    //    glfwInit();

    //    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    //    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    //    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    //    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    //    std::cout << "hello" << std::endl;

    //    GLFWwindow* window = glfwCreateWindow(800, 600, "Test", nullptr,
    //    nullptr); if (window == nullptr)
    //    {
    //        std::cerr << "Failed create window" << std::endl;
    //        glfwTerminate();
    //        return EXIT_FAILURE;
    //    }

    //    glfwMakeContextCurrent(window);

    //    glewExperimental = GL_TRUE;
    //    if (glewInit() != GLEW_OK)
    //    {
    //        std::cerr << "can't initialize GLEW" << std::endl;
    //        return EXIT_FAILURE;
    //    }

    //    int width, height;
    //    glfwGetFramebufferSize(window, &width, &height);

    //    glViewport(0, 0, width, height);

    //    glfwSetKeyCallback(window, key_callback);
    //    glClearColor(0.3, 0.5, 0.2, 1.0);

    //    const GLchar* vertex_shader_src =
    //        "#version 330 core\n"
    //        "layout (location = 0) in vec2 position;\n"
    //        "layout (location = 1) in vec3 color;\n"
    //        "out vec3 ourColor;\n"

    //        "void main()\n"
    //        "{\n"
    //        "gl_Position = vec4(position.x, position.y, 0.0f, 1.0f);\n"
    //        "ourColor = color;\n"
    //        "}\0";

    //    GLuint vertex_shader;
    //    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    //    glShaderSource(vertex_shader, 1, &vertex_shader_src, NULL);
    //    glCompileShader(vertex_shader);
    //    GLint  success;
    //    GLchar infolog[512];
    //    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    //    if (!success)
    //    {
    //        glGetShaderInfoLog(vertex_shader, 512, NULL, infolog);
    //        std::cerr << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n"
    //                  << infolog << std::endl;
    //    }

    //    const GLchar* fragment_shader_src = "#version 330 core\n"
    //                                        "in vec3 ourColor;\n"
    //                                        "out vec4 color;\n"

    //                                        "void main()\n"
    //                                        "{\n"
    //                                        "color = vec4(ourColor, 1.0f);\n"
    //                                        "}\n\0";

    //    GLuint fragment_shader;
    //    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    //    glShaderSource(fragment_shader, 1, &fragment_shader_src, NULL);
    //    glCompileShader(fragment_shader);

    //    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    //    if (!success)
    //    {
    //        glGetShaderInfoLog(fragment_shader, 512, NULL, infolog);
    //        std::cerr << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n"
    //                  << infolog << std::endl;
    //    }

    //    GLuint shader_program = glCreateProgram();

    //    glAttachShader(shader_program, vertex_shader);
    //    glAttachShader(shader_program, fragment_shader);
    //    glLinkProgram(shader_program);

    //    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    //    if (!success)
    //    {
    //        glGetProgramInfoLog(shader_program, 512, NULL, infolog);
    //        std::cerr << "ERROR::SHADER::PROGRAM::LINK_FAILED\n"
    //                  << infolog << std::endl;
    //    }

    //    glDeleteShader(vertex_shader);
    //    glDeleteShader(fragment_shader);

    //    GLfloat vertices[] = {
    //        -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, // Left
    //        0.5f,  -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, // Right
    //        0.0f,  0.5f,  0.0f, 1.0f, 0.0f, 0.0f  // Top

    //    };

    //    GLfloat vertices_01[] = {
    //        0.f,  0.2f,  0.0f, 1.0f, 1.0f, 1.0f, // Left
    //        1.f,  -0.5f, 0.0f, 1.0f, 1.0f, 1.0f, // Right
    //        0.6f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f  // Top
    //    };

    //    GLfloat vertices_square[] = {
    //        0.5f,  0.5f,  0.0f, 1.0f, 0.0f, 0.0f, // top right corner
    //        0.5f,  -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // lower right corner
    //        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, // lower left corner
    //        -0.5f, 0.5f,  0.0f, 1.0f, 1.0f, 1.0f  // top left corner
    //    };

    //    GLuint indexes[] = {
    //        0, 1, 3, // first triangle
    //        1, 2, 3  // second triangle
    //    };

    //    //    Ckom::Triangle_pos trian1, trian2;

    //    //    trian1.vertexes[0] = { vertices[0], vertices[1] };
    //    //    trian1.vertexes[1] = { vertices[6], vertices[7] };
    //    //    trian1.vertexes[2] = { vertices[12], vertices[13] };

    //    //    trian2.vertexes[0] = { vertices_01[0], vertices_01[1] };
    //    //    trian2.vertexes[1] = { vertices_01[6], vertices_01[7] };
    //    //    trian2.vertexes[2] = { vertices_01[12], vertices_01[13] };

    //    //    std::cout << intersection_triangle_check(trian1, trian2) <<
    //    std::endl;

    //    GLuint VBO, VAO, vbo, vao, EBO;

    //    glGenVertexArrays(1, &VAO);
    //    glGenBuffers(1, &VBO);
    //    glBindVertexArray(VAO);

    //    glGenBuffers(1, &EBO);

    //    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    //    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_square),
    //    vertices_square,
    //                 GL_STATIC_DRAW);
    //    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    //    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexes), indexes,
    //                 GL_STATIC_DRAW);

    //    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
    //                          (GLvoid*)0);
    //    glEnableVertexAttribArray(0);

    //    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
    //                          (GLvoid*)(3 * sizeof(GLfloat)));
    //    glEnableVertexAttribArray(1);

    //    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    //    // continue here
    //    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //        glBindVertexArray(0);
    //        glDisableVertexAttribArray(0);
    //        glDisableVertexAttribArray(1);

    //        glGenVertexArrays(1, &vao);
    //        glGenBuffers(1, &vbo);
    //        glBindVertexArray(vao);
    //        glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_01), vertices_01,
    //                     GL_STATIC_DRAW);
    //        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 6 *
    //        sizeof(GLfloat),
    //                              (GLvoid*)0);
    //        glEnableVertexAttribArray(0);
    //        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 *
    //        sizeof(GLfloat),
    //                              (GLvoid*)(3 * sizeof(GLfloat)));
    //        glEnableVertexAttribArray(1);
    //        glBindBuffer(GL_ARRAY_BUFFER, 0);
    //        glBindVertexArray(0);
    //        glDisableVertexAttribArray(0);
    //        glDisableVertexAttribArray(1);

    //        while (!glfwWindowShouldClose(window))
    //        {
    //            glfwPollEvents();

    //            glClear(GL_COLOR_BUFFER_BIT);

    //            glUseProgram(shader_program);
    //            glBindVertexArray(VAO);

    //            glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    //            glBindVertexArray(0);

    //            glBindVertexArray(vao);
    //            glDrawArrays(GL_TRIANGLES, 0, 3);
    //            glBindVertexArray(0);

    //            glfwSwapBuffers(window);
    //        }

    //        glDeleteVertexArrays(1, &VAO);
    //        glDeleteBuffers(1, &VBO);

    //        glfwTerminate();
    //        return EXIT_SUCCESS;

    Ckom::Engine*     engine = Ckom::create_engine();
    const std::string error  = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    bool continue_loop = true;

    while (continue_loop)
    {
        glfwPollEvents();
        engine->render();
        engine->swap_buffer();
    }
}

// void key_callback(GLFWwindow* window, int key, int scancode, int action,
//                  int mode)
//{
//    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
//        glfwSetWindowShouldClose(window, GL_TRUE);
//}
