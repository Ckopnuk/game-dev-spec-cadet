#include "engine.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SDL.h>
#include <SDL_opengl.h>

#include <algorithm>
#include <array>
#undef NDEBUG
#include <cassert>
#include <cmath>
#include <exception>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string_view>

template <typename T>
static void load_gl_function(const char* function_name, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(function_name);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(std::string("Can't load gl funcrion") +
                                 function_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action,
                  int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}

#define CKOM_GL_CHECK()                                                        \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

namespace Ckom
{

class Engine_impl final : public Engine
{
public:
    std::string initialize(std::string_view conf) override;
    void        swap_buffer() override;
    void        render() override;

private:
    GLuint        VBO, VAO, vbo, vao, EBO;
    GLuint        shader_program = 0;
    SDL_GLContext gl_context     = nullptr;
    SDL_Window*   sld_window     = nullptr;
};

GLFWwindow* window = nullptr;

std::string Engine_impl::initialize(std::string_view conf)
{
    std::stringstream serr;

    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    window = glfwCreateWindow(800, 600, "Test", nullptr, nullptr);
    if (window == nullptr)
    {
        serr << "Failed crate window" << std::endl;
        glfwTerminate();
    }

    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        serr << "Can't init GLEW" << std::endl;
    }

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    glViewport(0, 0, width, height);
    CKOM_GL_CHECK()

    glfwSetKeyCallback(window, key_callback);

    glClearColor(0.f, 0.0f, 0.f, 0.f);
    CKOM_GL_CHECK()

    const GLchar* vertex_shader_src =
        "#version 330 core\n"
        "layout (location = 0) in vec2 position;\n"
        "layout (location = 1) in vec3 color;\n"
        "out vec3 ourColor;\n"

        "void main()\n"
        "{\n"
        "gl_Position = vec4(position.x, position.y, 0.0f, 1.0f);\n"
        "ourColor = color;\n"
        "}\0";

    GLuint vertex_shader;
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_src, NULL);
    CKOM_GL_CHECK()
    glCompileShader(vertex_shader);
    CKOM_GL_CHECK()

    GLint  success;
    GLchar infolog[512];
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertex_shader, 512, NULL, infolog);
        serr << "ERROR::SHADER::VERTEX::COMPILE_FAILED\n"
             << infolog << std::endl;
    }

    const GLchar* fragment_shader_src = "#version 330 core\n"
                                        "in vec3 ourColor;\n"
                                        "out vec4 color;\n"

                                        "void main()\n"
                                        "{\n"
                                        "color = vec4(ourColor, 1.0f);\n"
                                        "}\n\0";

    GLuint fragment_shader;
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_src, NULL);
    glCompileShader(fragment_shader);

    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragment_shader, 512, NULL, infolog);
        serr << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n"
             << infolog << std::endl;
    }

    shader_program = glCreateProgram();

    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);

    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shader_program, 512, NULL, infolog);
        serr << "ERROR::SHADER::PROGRAM::LINK_FAILED\n" << infolog << std::endl;
    }

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    GLfloat vertices[] = {
        -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, // Left
        0.5f,  -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, // Right
        0.0f,  0.5f,  0.0f, 1.0f, 0.0f, 0.0f  // Top

    };

    GLfloat vertices_01[] = {
        0.f,  0.2f,  0.0f, 1.0f, 1.0f, 1.0f, // Left
        1.f,  -0.5f, 0.0f, 1.0f, 1.0f, 1.0f, // Right
        0.6f, -0.5f, 0.0f, 1.0f, 1.0f, 1.0f  // Top
    };

    GLfloat vertices_square[] = {
        0.5f,  0.5f,  0.0f, 1.0f, 0.0f, 0.0f, // top right corner
        0.5f,  -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // lower right corner
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, // lower left corner
        -0.5f, 0.5f,  0.0f, 1.0f, 1.0f, 1.0f  // top left corner
    };

    GLuint indexes[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &EBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_square), vertices_square,
                 GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexes), indexes,
                 GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                          (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat),
                          (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);

    glClear(GL_COLOR_BUFFER_BIT);

    //    glUseProgram(shader_program);

    //    glBindVertexArray(VAO);

    //    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    //    glfwSwapBuffers(window);

    return "";
}

void Engine_impl::swap_buffer()
{
    glfwSwapBuffers(window);
}

void Engine_impl::render()
{
    glUseProgram(shader_program);
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

static bool    already_exist = false;
static Engine* g_engine      = nullptr;

Engine* create_engine()
{
    if (already_exist)
    {
        throw std::runtime_error("Is already exist");
    }
    Engine* result = new Engine_impl;
    g_engine       = result;
    already_exist  = true;
    return result;
}

void destroy_engine(Engine* eng)
{
    if (already_exist == false)
    {
        throw std::runtime_error("engine not created!!!");
    }

    if (nullptr == eng)
    {
        throw std::runtime_error("engine is nullptr!!!");
    }
    delete eng;
    already_exist = false;
}

} // namespace Ckom
