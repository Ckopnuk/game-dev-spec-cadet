#include "my_math.h"

// bool intersection_check(float x1, float y1, float x2, float y2, float x3,
//                        float y3, float x4, float y4)
//{
//    float ua, ub, numerator_a, numerator_b, denominator;
//    denominator = (y4 - y3) * (x1 - x2) - (x4 - x3) * (y1 - y2);

//    if (denominator == 0)
//    {
//        if ((x1 * y2 - x2 * y1) * (x4 - x3) - (x3 * y4 - x4 * y3) * (x2 - x1)
//        ==
//                0 &&
//            (x1 * y2 - x2 * y1) * (y4 - y3) - (x3 * y4 - x4 * y3) * (y2 - y1)
//            ==
//                0)
//        {
//            return true;
//        }
//        else
//            return false;
//    }
//    else
//    {
//        numerator_a = (x4 - x2) * (y4 - y3) - (x4 - x3) * (y4 - y2);
//        numerator_b = (x1 - x2) * (y4 - y2) - (x4 - x2) * (y1 - y2);

//        ua = numerator_a / denominator;
//        ub = numerator_b / denominator;

//        return (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1 ? true : false);
//    }
//}

// bool line_crossing(Ckom::segment line1, Ckom::segment line2)
//{
//    return intersection_check(line1.a.x, line1.a.y, line1.b.x, line1.b.y,
//                              line2.a.x, line2.a.y, line2.b.x, line2.b.y);
//}

// bool intersection_triangle_check(Ckom::Triangle_pos trian1,
//                                 Ckom::Triangle_pos trian2)
//{
//    int n = 3; // point count
//    for (int i = 0; i < n; ++i)
//    {
//        int i1 = i + 1;
//        if (i1 >= n)
//        {
//            i1 = 0;
//        }

//        Ckom::segment line1, line2;
//        line1.a = trian1.vertexes[i];
//        line1.b = trian1.vertexes[i1];

//        line2.a = trian2.vertexes[i];
//        line2.b = trian2.vertexes[i1];

//        bool result = line_crossing(line1, line2);
//        if (result)
//        {
//            return true;
//        }
//        else
//        {
//            if (is_inside(trian1.vertexes[i], trian2))
//            {
//                return true;
//            }
//            else if (is_inside(trian2.vertexes[i], trian1))
//            {
//                return true;
//            }
//        }
//    }

//    return false;
//}

// bool is_inside(Ckom::vec2 point, Ckom::Triangle_pos trian)
//{
//    int   i1, i2, n;
//    float S, S1, S2, S3;
//    n = 3; // point count
//    for (int i = 0; i < n; ++i)
//    {
//        i1 = (i < n - 1) ? (i + 1) : 0;

//        i2 = i1 + 1;
//        if (i2 >= n)
//        {
//            i2 = 0;
//        }
//        if (i2 == (i < n - 1 ? i + 1 : 0))
//        {
//            break;
//        }

//        S  = std::abs(trian.vertexes[i1].x *
//                         (trian.vertexes[i2].y - trian.vertexes[i].y) +
//                     trian.vertexes[i2].x *
//                         (trian.vertexes[i].y - trian.vertexes[i1].y) +
//                     trian.vertexes[i].x *
//                         (trian.vertexes[i1].y - trian.vertexes[i2].y));
//        S1 = std::abs(trian.vertexes[i1].x * (trian.vertexes[i2].y - point.y)
//        +
//                      trian.vertexes[i2].x * (point.y - trian.vertexes[i1].y)
//                      + point.x * (trian.vertexes[i1].y -
//                      trian.vertexes[i2].y));
//        S2 = std::abs(trian.vertexes[i].x * (trian.vertexes[i2].y - point.y) +
//                      trian.vertexes[i2].x * (point.y - trian.vertexes[i].y) +
//                      point.x * (trian.vertexes[i].y - trian.vertexes[i2].y));
//        S3 = std::abs(trian.vertexes[i1].x * (trian.vertexes[i].y - point.y) +
//                      trian.vertexes[i].x * (point.y - trian.vertexes[i1].y) +
//                      point.x * (trian.vertexes[i1].y - trian.vertexes[i].y));
//        if (S == S1 + S2 + S3)
//        {
//            return true;
//        }
//        i1++;
//        if (i1 >= n)
//        {
//            i1 = 0;
//        }
//    }
//    return false;
//}
