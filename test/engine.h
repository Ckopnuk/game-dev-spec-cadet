#ifndef ENGINE_H
#define ENGINE_H

//#include <GLFW/glfw3.h>
#include <string>
#include <string_view>

namespace Ckom
{

struct point
{
    point();
    point(float x_, float y_);

    float x = 0.0f;
    float y = 0.0f;
};

class Engine
{
public:
    virtual ~Engine() {}
    virtual std::string initialize(std::string_view conf) = 0;
    virtual void        swap_buffer()                     = 0;

    virtual void render() = 0;
};

Engine* create_engine();
void    destroy_engine(Engine* eng);

} // namespace Ckom

#endif // ENGINE_H
