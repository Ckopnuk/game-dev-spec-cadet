#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.h"

int main(int, char*[])
{
    std::unique_ptr<Ckom::Engine, void (*)(Ckom::Engine*)> engine(
        Ckom::create_engine(), Ckom::destroy_engine);

    const std::string error = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    bool continueLoop = true;
    while (continueLoop)
    {
        Ckom::Event event;

        while (engine->readInput(event))
        {

            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continueLoop = false;
                    break;
                default:
                    break;
            }
        }

        std::ifstream file("vert_and_tex_coord.txt", std::ios_base::binary);
        assert(!!file);

        if (!file)
        {
            std::cerr << "Can't open file vert_and_tex_coord.txt" << std::endl;
        }

        Ckom::Triangle tr1;
        Ckom::Triangle tr2;

        file >> tr1 >> tr2;

        engine->renderTriangle(tr1);
        engine->renderTriangle(tr2);

        engine->swapBuffer();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}
