#include "engine.h"
#include "picopng.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <tuple>
#include <vector>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>

static PFNGLCREATESHADERPROC        glCreateShader        = nullptr;
static PFNGLSHADERSOURCEPROC        glShaderSource        = nullptr;
static PFNGLCOMPILESHADERPROC       glCompileShader       = nullptr;
static PFNGLGETSHADERIVPROC         glGetShaderiv         = nullptr;
static PFNGLGETSHADERINFOLOGPROC    glGetShaderInfoLog    = nullptr;
static PFNGLDELETESHADERPROC        glDeleteShader        = nullptr;
static PFNGLCREATEPROGRAMPROC       glCreateProgram       = nullptr;
static PFNGLATTACHSHADERPROC        glAttachShader        = nullptr;
static PFNGLBINDATTRIBLOCATIONPROC  glBindAttribLocation  = nullptr;
static PFNGLLINKPROGRAMPROC         glLinkProgram         = nullptr;
static PFNGLGETPROGRAMIVPROC        glGetProgramiv        = nullptr;
static PFNGLGETPROGRAMINFOLOGPROC   glGetProgramInfoLog   = nullptr;
static PFNGLDELETEPROGRAMPROC       glDeleteProgram       = nullptr;
static PFNGLUSEPROGRAMPROC          glUseProgram          = nullptr;
static PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;

static PFNGLENABLEVERTEXATTRIBARRAYPROC  glEnableVertexAttribArray  = nullptr;
static PFNGLVALIDATEPROGRAMPROC          glValidateProgram          = nullptr;
static PFNGLGETUNIFORMLOCATIONPROC       glGetUniformLocation       = nullptr;
static PFNGLUNIFORM1FPROC                glUniform1f                = nullptr;
static PFNGLUNIFORM1IPROC                glUniform1i                = nullptr;
static PFNGLDELETEBUFFERSPROC            glDeleteBuffers            = nullptr;
static PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = nullptr;
static PFNGLACTIVETEXTUREPROC            glActiveTextureMY          = nullptr;
static PFNGLUNIFORM4FVPROC               glUniform4fv               = nullptr;
// RENDERDOC
static PFNGLBINDBUFFERPROC      glBindBuffer      = nullptr;
static PFNGLGENBUFFERSPROC      glGenBuffers      = nullptr;
static PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
static PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
static PFNGLBUFFERDATAPROC      glBufferData      = nullptr;
// RENDERDOC

template <typename T>
static void load_gl_function(const char* funcName, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(funcName);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(std::string("can't load gl function") +
                                 funcName);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#define CKOM_GL_CHECK()                                                        \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

namespace Ckom
{
#pragma pack(push, 1)
class Texture_gl_es20 final : public Texture
{
public:
    explicit Texture_gl_es20(std::string_view path);
    ~Texture_gl_es20() override;

    void bind() const
    {
        glBindTexture(GL_TEXTURE_2D, tex_hendl);
        CKOM_GL_CHECK()
    }

    std::uint32_t get_width() const override { return width; }
    std::uint32_t get_height() const override { return height; }

private:
    std::string   file_path;
    GLuint        tex_hendl = 0;
    std::uint32_t width     = 0;
    std::uint32_t height    = 0;
};
#pragma pack(pop)

class Shader_gl_es20
{
public:
    Shader_gl_es20(std::string_view vertex_src, std::string_view fragment_src,
                   const std::vector<std::tuple<GLuint, const GLchar*>>& atrib)
    {
        vert_shader = compiled_shader(GL_VERTEX_SHADER, vertex_src);
        frag_shader = compiled_shader(GL_FRAGMENT_SHADER, fragment_src);
        if (vert_shader == 0 || frag_shader == 0)
        {
            throw std::runtime_error("can't compile shader");
        }
        program_id = link_program(atrib);
        if (program_id == 0)
        {
            throw std::runtime_error("can't link program id");
        }
    }

    void use() const
    {
        glUseProgram(program_id);
        CKOM_GL_CHECK()
    }
    void set_uniform(std::string_view uniform_name, Texture_gl_es20* texture)
    {
        assert(texture != nullptr);
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        CKOM_GL_CHECK()
        if (location == -1)
        {
            std::cerr << "Can't get uniform location from shader\n";
            std::runtime_error("cant get uniform location");
        }

        unsigned int texture_unit = 0;
        glActiveTextureMY(GL_TEXTURE0 + texture_unit);
        CKOM_GL_CHECK()

        texture->bind();

        glUniform1i(location, static_cast<int>(0 + texture_unit));
        CKOM_GL_CHECK()
    }
    void set_uniform(std::string_view uniform_name, const Color& clr)
    {
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        CKOM_GL_CHECK()
        if (location == -1)
        {
            std::cerr << "Can't get uniform location from shader\n";
            std::runtime_error("cant get uniform location");
        }
        float values[4] = { clr.get_r(), clr.get_g(), clr.get_b(),
                            clr.get_a() };
        glUniform4fv(location, 1, &values[0]);
        CKOM_GL_CHECK()
    }

private:
    GLuint compiled_shader(GLenum shader_type, std::string_view src)
    {
        GLuint shader_id = glCreateShader(shader_type);
        CKOM_GL_CHECK()
        std::string_view vertex_shader_src = src;
        const char*      source            = vertex_shader_src.data();
        glShaderSource(shader_id, 1, &source, nullptr);
        CKOM_GL_CHECK()

        glCompileShader(shader_id);
        CKOM_GL_CHECK()

        GLint compiled_status = 0;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
        CKOM_GL_CHECK()
        if (compiled_status == 0)
        {
            GLint info_len = 0;
            glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
            CKOM_GL_CHECK()
            std::vector<char> info_chars(static_cast<size_t>(info_len));
            glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
            CKOM_GL_CHECK()
            glDeleteShader(shader_id);
            CKOM_GL_CHECK()

            std::string shader_type_name =
                shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";
            std::cerr << "Error compiling shader(vertex)\n"
                      << vertex_shader_src << "\n"
                      << info_chars.data();
            return 0;
        }
        return shader_id;
    }
    GLuint link_program(
        const std::vector<std::tuple<GLuint, const GLchar*>>& atrib)
    {
        GLuint program_id_ = glCreateProgram();
        CKOM_GL_CHECK()
        if (program_id_ == 0)
        {
            std::cerr << "failed create program ";
            throw std::runtime_error("can't link shader");
        }

        glAttachShader(program_id_, vert_shader);
        CKOM_GL_CHECK()
        glAttachShader(program_id_, frag_shader);
        CKOM_GL_CHECK()

        for (const auto& attr : atrib)
        {
            GLuint        loc  = std::get<0>(attr);
            const GLchar* name = std::get<1>(attr);
            glBindAttribLocation(program_id_, loc, name);
            CKOM_GL_CHECK()
        }

        glLinkProgram(program_id_);
        CKOM_GL_CHECK()

        GLint linked_status = 0;
        glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
        CKOM_GL_CHECK()
        if (linked_status == 0)
        {
            GLint info_len = 0;
            glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &info_len);
            CKOM_GL_CHECK()
            std::vector<char> info_log(static_cast<size_t>(info_len));
            glGetProgramInfoLog(program_id_, info_len, nullptr,
                                info_log.data());
            CKOM_GL_CHECK()
            std::cerr << "Error linked program:\n" << info_log.data();
            glDeleteProgram(program_id_);
            CKOM_GL_CHECK()
            return 0;
        }
        return program_id_;
    }

    GLuint vert_shader = 0;
    GLuint frag_shader = 0;
    GLuint program_id  = 0;
};

Triangle_p::Triangle_p()
    : v{ Vertex_p(), Vertex_p(), Vertex_p() }
{
}

Triangle_pc::Triangle_pc()
    : v{ Vertex_pc(), Vertex_pc(), Vertex_pc() }
{
}

Triangle_pct::Triangle_pct()
    : v{ Vertex_pct(), Vertex_pct(), Vertex_pct() }
{
}

static std::array<std::string_view, 21> eventNames = {
    { "left_pressed",
      "left_released",
      "right_pressed",
      "right_released",
      "up_pressed",
      "up_released",
      "down_pressed",
      "down_released",
      "select_pressed",
      "select_released",
      "start_pressed",
      "start_released",
      "button_triangle_pressed",
      "button_triangle_released",
      "button_square_pressed",
      "button_square_released",
      "button_cross_pressed",
      "button_cross_released",
      "button_circle_pressed",
      "button_circle_released",
      "turn_off" }
};

std::ostream& operator<<(std::ostream& fout, Event event)
{
    std::uint32_t value = static_cast<std::uint32_t>(event);
    std::uint32_t min   = static_cast<std::uint32_t>(Event::left_pressed);
    std::uint32_t max   = static_cast<std::uint32_t>(Event::turn_off);
    if (value >= min && value <= max)
    {
        fout << eventNames[value];
        return fout;
    }
    else
    {
        throw std::runtime_error("too big event value");
    }
}

static std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

std::istream& operator>>(std::istream& is, Texture_position& tx)
{
    is >> tx.u;
    is >> tx.v;
    return is;
}

std::istream& operator>>(std::istream& is, Color& clr)
{
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
    float a = 0.f;

    is >> r;
    is >> g;
    is >> b;
    is >> a;

    clr = Color(r, g, b, a);
    return is;
}

std::istream& operator>>(std::istream& is, Vertex_p& v)
{
    is >> v.p.x;
    is >> v.p.y;

    return is;
}

std::istream& operator>>(std::istream& is, Vertex_pc& v)
{
    is >> v.p.x;
    is >> v.p.y;
    is >> v.c;
    return is;
}

std::istream& operator>>(std::istream& is, Vertex_pct& v)
{
    is >> v.p.x;
    is >> v.p.y;
    is >> v.tx;
    is >> v.c;
    return is;
}

std::istream& operator>>(std::istream& is, Triangle_p& tr)
{
    is >> tr.v[0];
    is >> tr.v[1];
    is >> tr.v[2];
    return is;
}

std::istream& operator>>(std::istream& is, Triangle_pc& tr)
{
    is >> tr.v[0];
    is >> tr.v[1];
    is >> tr.v[2];
    return is;
}

std::istream& operator>>(std::istream& is, Triangle_pct& tr)
{
    is >> tr.v[0];
    is >> tr.v[1];
    is >> tr.v[2];
    return is;
}

#pragma pack(push, 1)
struct myBind
{
    myBind(SDL_KeyCode k, std::string_view str, Event press, Event releas)
        : keyCode(k)
        , keyName(str)
        , eventPress(press)
        , eventReleas(releas)
    {
    }

    SDL_KeyCode      keyCode;
    std::string_view keyName;
    Event            eventPress;
    Event            eventReleas;
};
#pragma pack(pop)

const std::array<myBind, 10> keysArr{
    { { SDLK_w, "up", Event::up_pressed, Event::up_released },
      { SDLK_a, "left", Event::left_pressed, Event::left_released },
      { SDLK_s, "down", Event::down_pressed, Event::down_released },
      { SDLK_d, "right", Event::right_pressed, Event::right_released },
      { SDLK_i, "triangle", Event::button_triangle_pressed,
        Event::button_triangle_released },
      { SDLK_j, "square", Event::button_square_pressed,
        Event::button_square_released },
      { SDLK_k, "cross", Event::button_cross_pressed,
        Event::button_cross_released },
      { SDLK_l, "circle", Event::button_circle_pressed,
        Event::button_circle_released },
      { SDLK_p, "statr", Event::start_pressed, Event::start_released },
      { SDLK_ESCAPE, "select", Event::select_pressed, Event::select_released } }
};

static bool checkInput(const SDL_Event& ev, const myBind*& result)
{
    const auto it = std::find_if(
        std::begin(keysArr), std::end(keysArr),
        [&](const myBind& bind) { return bind.keyCode == ev.key.keysym.sym; });

    if (it != end(keysArr))
    {
        result = &(*it);
        return true;
    }
    return false;
}

class EngineImpl final : public Engine
{
public:
    std::string initialize(std::string_view /*conf*/) override final;

    bool  read_input(Event& ev) override final;
    float get_time() override final;

    Texture* create_texture(std::string_view path) override final
    {
        return new Texture_gl_es20(path);
    }
    void destroy_texture(Texture* texture) override final { delete texture; }

    void render(const Triangle_p& tr, const Color& col) override final
    {
        shader00->use();
        shader00->set_uniform("u_color", col);
        // vertex coordinates
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_p),
                              &tr.v[0].p.x);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(0);
        CKOM_GL_CHECK()

        // texture coordinates
        // glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(v0),
        // &t.v[0].tx);
        // OM_GL_CHECK();
        // glEnableVertexAttribArray(1);
        // OM_GL_CHECK();

        glDrawArrays(GL_TRIANGLES, 0, 3);
        CKOM_GL_CHECK()
    }
    void render(const Triangle_pc& tr) override final
    {
        shader01->use();
        // positions
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v[0].p);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(0);
        CKOM_GL_CHECK()
        // colors
        glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(tr.v[0]),
                              &tr.v[0].c);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(1);
        CKOM_GL_CHECK()

        glDrawArrays(GL_TRIANGLES, 0, 3);
        CKOM_GL_CHECK()

        glDisableVertexAttribArray(1);
        CKOM_GL_CHECK()
    }
    void render(const Triangle_pct& tr, Texture* tx) override final
    {
        shader02->use();
        Texture_gl_es20* texture = static_cast<Texture_gl_es20*>(tx);
        texture->bind();
        shader02->set_uniform("s_texture", texture);
        // positions
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v[0].p);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(0);
        CKOM_GL_CHECK()
        // colors
        glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(tr.v[0]),
                              &tr.v[0].c);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(1);
        CKOM_GL_CHECK()

        // texture coordinates
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(tr.v[0]),
                              &tr.v[0].tx);
        CKOM_GL_CHECK()
        glEnableVertexAttribArray(2);
        CKOM_GL_CHECK()

        glDrawArrays(GL_TRIANGLES, 0, 3);
        CKOM_GL_CHECK()

        glDisableVertexAttribArray(1);
        CKOM_GL_CHECK()
        glDisableVertexAttribArray(2);
        CKOM_GL_CHECK()
    }
    void swap_buffer() override final;
    void set_uniform(const char*, float) override final;
    void uninitialize() override final;

private:
    SDL_Window*   window     = nullptr;
    SDL_GLContext gl_context = nullptr;
    GLuint        program_id = 0;

    Shader_gl_es20* shader00 = nullptr;
    Shader_gl_es20* shader01 = nullptr;
    Shader_gl_es20* shader02 = nullptr;
};

static bool alreadyExist = false;

Engine* create_engine()
{
    if (alreadyExist)
    {
        throw std::runtime_error("is already exist");
    }
    Engine* result = new EngineImpl;
    alreadyExist   = true;
    return result;
}

void destroy_engine(Engine* eng)
{
    if (alreadyExist == false)
    {
        throw std::runtime_error("engine not created!!!");
    }
    if (nullptr == eng)
    {
        throw std::runtime_error("eng iss nullptr!!!");
    }
    delete eng;
}

Engine::~Engine() {}

std::string EngineImpl::initialize(std::string_view /*conf*/) // continue
{
    std::stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version link     = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(link.major, link.minor, link.patch))
    {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << ' ' << link << std::endl;
    }

    const int initResult = SDL_Init(SDL_INIT_EVERYTHING);
    CKOM_GL_CHECK()
    if (initResult != 0)
    {
        const char* errMsg = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << errMsg << std::endl;
        return serr.str();
    }

    // SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    window =
        SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, 640, 480, ::SDL_WINDOW_OPENGL);

    if (window == nullptr)
    {
        const char* errMsg = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << errMsg << std::endl;
        SDL_Quit();
        return serr.str();
    }
    // for render
    //    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    //    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    //    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
    //                        SDL_GL_CONTEXT_PROFILE_CORE);
    // for renfer
    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        std::string msg("Can't create openGL context: ");
        msg += SDL_GetError();
        serr << msg << std::endl;
        return serr.str();
    }

    int glMajor = 0;
    int result  = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &glMajor);
    assert(result == 0);
    int glMinor = 0;
    result      = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &glMinor);
    assert(result == 0);

    std::cout << "GL_context: " << glMajor << '.' << glMinor << std::endl;

    if (glMajor <= 2 && glMinor < 1)
    {
        serr << "Corrent openGL context version: " << glMajor << ':' << glMinor
             << '\n'
             << "Need openGl version at least 2:1\n"
             << std::flush;
        return serr.str();
    }

    try
    {
        load_gl_function("glCreateShader", glCreateShader);
        load_gl_function("glShaderSource", glShaderSource);
        load_gl_function("glCompileShader", glCompileShader);
        load_gl_function("glGetShaderiv", glGetShaderiv);
        load_gl_function("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_function("glDeleteShader", glDeleteShader);
        load_gl_function("glCreateProgram", glCreateProgram);
        load_gl_function("glAttachShader", glAttachShader);
        load_gl_function("glBindAttribLocation", glBindAttribLocation);
        load_gl_function("glLinkProgram", glLinkProgram);
        load_gl_function("glGetProgramiv", glGetProgramiv);
        load_gl_function("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_function("glDeleteProgram", glDeleteProgram);
        load_gl_function("glUseProgram", glUseProgram);
        load_gl_function("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_function("glEnableVertexAttribArray",
                         glEnableVertexAttribArray);
        load_gl_function("glValidateProgram", glValidateProgram);
        load_gl_function("glBindBuffer", glBindBuffer);
        load_gl_function("glGenBuffers", glGenBuffers);
        load_gl_function("glGenVertexArrays", glGenVertexArrays);
        load_gl_function("glBindVertexArray", glBindVertexArray);
        load_gl_function("glBufferData", glBufferData);
        load_gl_function("glGetUniformLocation", glGetUniformLocation);
        load_gl_function("glUniform1f", glUniform1f);
        load_gl_function("glDeleteBuffers", glDeleteBuffers);
        load_gl_function("glDisableVertexAttribArray",
                         glDisableVertexAttribArray);
        load_gl_function("glUniform1i", glUniform1i);
        load_gl_function("glActiveTexture", glActiveTextureMY);
        load_gl_function("glUniform4fv", glUniform4fv);
    }
    catch (std::exception& ex)
    {
        return ex.what();
    }

    shader00 = new Shader_gl_es20(R"(
                                  attribute vec2 a_position;
                                  void main()
                                  {
                                      gl_Position = vec4(a_position, 0.0, 1.0);
                                  }
                                  )",
                                  R"(
                                  uniform vec4 u_color;
                                  void main()
                                  {
                                      gl_FragColor = u_color;
                                  }
                                  )",
                                  { { 0, "a_position" } });

    shader00->use();
    shader00->set_uniform("u_color", Color(1.f, 0.f, 0.f, 1.f));

    shader01 = new Shader_gl_es20(
        R"(
                attribute vec2 a_position;
                attribute vec4 a_color;
                varying vec4 v_color;
                void main()
                {
                    v_color = a_color;
                    gl_Position = vec4(a_position, 0.0, 1.0);
                }
                )",
        R"(
                varying vec4 v_color;
                void main()
                {
                     gl_FragColor = v_color;
                }
                )",
        { { 0, "a_position" }, { 1, "a_color" } });

    shader01->use();

    shader02 = new Shader_gl_es20(
        R"(
                attribute vec2 a_position;
                attribute vec2 a_tex_coord;
                attribute vec4 a_color;
                varying vec4 v_color;
                varying vec2 v_tex_coord;
                void main()
                {
                    v_tex_coord = a_tex_coord;
                    v_color = a_color;
                    gl_Position = vec4(a_position, 0.0, 1.0);
                }
                )",
        R"(
                varying vec2 v_tex_coord;
                varying vec4 v_color;
                uniform sampler2D s_texture;
                void main()
                {
                     gl_FragColor = texture2D(s_texture, v_tex_coord) * v_color;
                }
                )",
        { { 0, "a_position" }, { 1, "a_color" }, { 2, "a_tex_coord" } });

    // turn on rendering with just created shader program
    shader02->use();

    glEnable(GL_BLEND);
    CKOM_GL_CHECK()
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    CKOM_GL_CHECK()

    glClearColor(0.f, 0.0, 0.f, 0.0f);
    CKOM_GL_CHECK()

    return "";
}

bool EngineImpl::read_input(Event& event)
{
    SDL_Event sdlEvent;

    if (SDL_PollEvent(&sdlEvent))
    {
        const myBind* binding = nullptr;
        if (sdlEvent.type == SDL_QUIT)
        {
            event = Event::turn_off;
            return true;
        }
        else if (sdlEvent.type == SDL_KEYDOWN)
        {
            if (checkInput(sdlEvent, binding))
            {
                event = binding->eventPress;
                return true;
            }
        }
        else if (sdlEvent.type == SDL_KEYUP)
        {
            if (checkInput(sdlEvent, binding))
            {
                event = binding->eventReleas;
                return true;
            }
        }
    }
    return false;
}

void EngineImpl::swap_buffer()
{
    SDL_GL_SwapWindow(window);

    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    CKOM_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    CKOM_GL_CHECK()
}

void EngineImpl::set_uniform(const char* name, float count)
{
    GLint uniLoc = glGetUniformLocation(program_id, name);
    glUniform1f(uniLoc, count);
}

void EngineImpl::uninitialize()
{
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

float EngineImpl::get_time()
{
    std::uint32_t ms_from_library_initialization = SDL_GetTicks();
    float         seconds = ms_from_library_initialization * 0.001f;
    return seconds;
}

Color::Color(uint32_t rgba_)
    : rgba(rgba_)
{
}

Color::Color(float r, float g, float b, float a)
{
    assert(r <= 1 && r >= 0);
    assert(g <= 1 && g >= 0);
    assert(b <= 1 && b >= 0);
    assert(a <= 1 && a >= 0);

    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);

    rgba = a_ << 24 | b_ << 16 | g_ << 8 | r_;
}

float Color::get_r() const
{
    std::uint32_t r_ = (rgba & 0x000000FF) >> 0;
    return r_ / 255.f;
}
float Color::get_g() const
{
    std::uint32_t g_ = (rgba & 0x0000FF00) >> 8;
    return g_ / 255.f;
}
float Color::get_b() const
{
    std::uint32_t b_ = (rgba & 0x00FF0000) >> 16;
    return b_ / 255.f;
}
float Color::get_a() const
{
    std::uint32_t a_ = (rgba & 0xFF000000) >> 24;
    return a_ / 255.f;
}

void Color::set_r(const float r)
{
    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    rgba &= 0xFFFFFF00;
    rgba |= (r_ << 0);
}
void Color::set_g(const float g)
{
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    rgba &= 0xFFFF00FF;
    rgba |= (g_ << 8);
}
void Color::set_b(const float b)
{
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    rgba &= 0xFF00FFFF;
    rgba |= (b_ << 16);
}
void Color::set_a(const float a)
{
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);
    rgba &= 0x00FFFFFF;
    rgba |= a_ << 24;
}

Texture_gl_es20::Texture_gl_es20(std::string_view path)
    : file_path(path)
{
    std::vector<unsigned char> png_file_in_memory;
    std::ifstream              ifs(path.data(), std::ios_base::binary);
    if (!ifs)
    {
        throw std::runtime_error("can't load texture");
    }
    ifs.seekg(0, std::ios_base::end);
    std::streamoff pos_in_file = ifs.tellg();
    png_file_in_memory.resize(static_cast<size_t>(pos_in_file));
    ifs.seekg(0, std::ios_base::beg);
    if (!ifs)
    {
        throw std::runtime_error("can't load texture");
    }

    ifs.read(reinterpret_cast<char*>(png_file_in_memory.data()), pos_in_file);
    if (!ifs.good())
    {
        throw std::runtime_error("can't load texture");
    }

    std::vector<unsigned char> image;
    unsigned long              w = 0;
    unsigned long              h = 0;
    int error = decodePNG(image, w, h, &png_file_in_memory[0],
                          png_file_in_memory.size(), false);

    // if there's an error, display it
    if (error != 0)
    {
        std::cerr << "error: " << error << std::endl;
        throw std::runtime_error("can't load texture");
    }

    glGenTextures(1, &tex_hendl);
    CKOM_GL_CHECK()
    glBindTexture(GL_TEXTURE_2D, tex_hendl);
    CKOM_GL_CHECK()

    GLint   mipmap_level = 0;
    GLint   border       = 0;
    GLsizei width_       = static_cast<GLsizei>(w);
    GLsizei height_      = static_cast<GLsizei>(h);
    glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, width_, height_, border,
                 GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
    CKOM_GL_CHECK()

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    CKOM_GL_CHECK()
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    CKOM_GL_CHECK()
}

Texture_gl_es20::~Texture_gl_es20()
{
    glDeleteTextures(1, &tex_hendl);
    CKOM_GL_CHECK()
}

} // end namespace Ckom
