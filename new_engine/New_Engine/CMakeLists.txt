cmake_minimum_required(VERSION 3.17)

project(New_Engine)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)



set(eng_version engine_00.00.01)

add_library(${eng_version} STATIC    engine.h
                                     engine.cpp)

find_package(sdl2 REQUIRED)
target_include_directories(${eng_version} PRIVATE ${SDL2_INCLUDE_DIRS})
target_link_libraries(${eng_version} PRIVATE ${SDL2_LIBRARIES})

if(WIN32)
    target_compile_definitions(${eng_version} PRIVATE "-DCKOM_DECLSPEC=__declspec(dllexport)")

elseif(UNIX)
    target_link_libraries(${eng_version} PRIVATE -lSDL2 -lGL)

elseif(MSVC)
target_link_libraries(${eng_version} PRIVATE SDL2::SDL2 SDL2::SDL2main opengl32)

endif(WIN32)

find_package(sdl2 REQUIRED)
target_include_directories(${eng_version} PRIVATE ${SDL2_INCLUDE_DIRS})
target_link_libraries(${eng_version} PRIVATE ${SDL2_LIBRARIES})

if (MINGW)
    target_link_libraries(${engine-version} PRIVATE
        -lmingw32
        -lSDL2main
        -lSDL2
        -mwindows
        -lopengl32
        )
endif(MINGW)

find_library(SDL2_LIB NAMES SDL2)

add_executable(${PROJECT_NAME} main.cpp)

target_link_libraries(${PROJECT_NAME} ${eng_version})


