#include <iostream>

#include "engine.h"

using namespace std;

int main()
{
    Ckom::Engine*     engine = Ckom::create_engine();
    const std::string error  = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    bool continue_loop = true;

    while (continue_loop)
    {

        engine->swap_buffers();
    }

    engine->uninitialize();
    Ckom::destroy_engine(engine);

    return EXIT_SUCCESS;
}
