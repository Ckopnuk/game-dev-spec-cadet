#include "engine.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#include <SDL2/SDL_syswm.h>

#include <array>
#include <cassert>
#include <iostream>
#include <sstream>

static PFNGLCREATESHADERPROC glCreateShader = nullptr;

template <typename T>
static void load_gl_function(const char* func_name, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (gl_pointer == nullptr)
    {
        throw std::runtime_error(std::string("CAN'T LOAD GL FUNCTION!!!") +
                                 func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#define CKOM_GL_CHECK()                                                        \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

namespace Ckom
{
// struct section

///// vec2 /////
vec2::vec2()
    : x(0.f)
    , y(1.f)
{
}

vec2::vec2(float x_set, float y_set)
    : x(x_set)
    , y(y_set)
{
}

float vec2::length() const
{
    return std::sqrt(x * x + y * y);
}

///// end vec2 /////

// bind keys section

struct my_Bind
{
    my_Bind(SDL_KeyCode k, std::string_view str, Event pressed, Event relessed,
            Keys ckom_k)
        : key_code(k)
        , key_name(str)
        , event_press(pressed)
        , event_released(relessed)
        , ck_keys(ckom_k)
    {
    }
    SDL_KeyCode      key_code;
    std::string_view key_name;
    Event            event_press;
    Event            event_released;
    Ckom::Keys       ck_keys;
};

const std::array<my_Bind, 5> keys_arr{
    { my_Bind{ SDLK_ESCAPE, "esc", Event::esc_pressed, Event::esc_released,
               Keys::esc },
      my_Bind{ SDLK_w, "up", Event::up_pressed, Event::up_released, Keys::up },
      my_Bind{ SDLK_s, "down", Event::down_pressed, Event::down_released,
               Keys::down },
      my_Bind{ SDLK_a, "left", Event::left_pressed, Event::left_released,
               Keys::left },
      my_Bind{ SDLK_d, "right", Event::right_pressed, Event::right_released,
               Keys::right } }
};

// end bind keys section

// class ENGINE section

class Engine_impl final : public Engine
{
public:
    std::string initialize(std::string_view) override;
    void        uninitialize() override;
    void        render() override;
    void        swap_buffers() override;

private:
    SDL_Window*   window     = nullptr;
    SDL_GLContext gl_context = nullptr;
};

/// DEFINITION SECTION ///

std::string Engine_impl::initialize(std::string_view /*conf*/)
{
    std::stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version link     = { 0, 0, 0 };

    SDL_VERSION(&compiled);
    SDL_GetVersion(&link);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(link.major, link.minor, link.patch))
    {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << &compiled << ' ' << &link << std::endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        const char* err_msg = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << err_msg << std::endl;
        return serr.str();
    }

    int width = 1024, height = 760;

    window = SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, width, height,
                              ::SDL_WINDOW_OPENGL);

    if (window == nullptr)
    {
        const char* err_msg = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << err_msg << std::endl;
        SDL_Quit();
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
        gl_context = SDL_GL_CreateContext(window);
        if (gl_context == nullptr)
        {
            std::string msg("Can't create sdl_gl_context: ");
            msg += SDL_GetError();
            serr << msg << std::endl;
            return serr.str();
        }
    }

    int gl_major = 0, gl_minor = 0;
    int result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major);
    assert(result == 0);
    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor);
    assert(result == 0);

    std::cout << "SDL_Context: " << gl_major << '.' << gl_minor << std::endl;

    if (gl_major <= 2 && gl_minor < 0)
    {
        serr << "Corrent  openGL context versuion: " << gl_major << ':'
             << gl_minor << '\n'
             << "need openGL version at least 2.1\n"
             << std::flush;
        return serr.str();
    }

    //////// load section /////////
    // here we load GL function
    try
    {
        load_gl_function("glCreateShader", glCreateShader);
    }
    catch (std::exception& ex)
    {
        return ex.what();
    }

    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    glViewport(0, 0, w, h);

    glClearColor(0.f, 0.f, 0.f, 0.f);
    CKOM_GL_CHECK()

    return "";
}

void Engine_impl::uninitialize()
{
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Engine_impl::render() {}

void Engine_impl::swap_buffers()
{
    SDL_GL_SwapWindow(window);
    glClear(GL_COLOR_BUFFER_BIT);
    CKOM_GL_CHECK()
}

static bool    already_exist = false;
static Engine* g_engine      = nullptr;

Engine::~Engine() {}

Engine* create_engine()
{
    if (already_exist)
    {
        throw std::runtime_error("Engine already exist");
    }
    else
    {
        Engine* result = new Engine_impl;
        g_engine       = result;
        already_exist  = true;
        return result;
    }
}

void destroy_engine(Engine* eng)
{
    if (already_exist == false)
    {
        throw std::runtime_error("Engine not created!!!");
    }
    if (eng == nullptr)
    {
        throw std::runtime_error("eng is nullptr");
    }
    delete eng;
    already_exist = false;
}

} // namespace Ckom

// stopped an input work on binding keys!
