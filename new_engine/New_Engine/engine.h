#ifndef ENGINE_H
#define ENGINE_H

#include <string>
#include <string_view>

#ifndef CKOM_DECLSPEC
#define CKOM_DECLSPEC
#endif

namespace Ckom
{

enum class Event
{
    esc_pressed,
    esc_released,
    up_pressed,
    down_pressed,
    left_pressed,
    right_pressed,
    up_released,
    down_released,
    left_released,
    right_released
};

enum class Keys
{
    esc,
    up,
    down,
    left,
    right
};

// struct section
struct CKOM_DECLSPEC vec2
{
    vec2();
    ~vec2();
    vec2(const vec2&) = default;
    vec2(float x_set, float y_set);
    float length() const;
    float x = 0.0f;
    float y = 0.0f;
};

// class section

class CKOM_DECLSPEC Engine
{
public:
    virtual ~Engine();

    virtual std::string initialize(std::string_view /*conf*/) = 0;
    virtual void        uninitialize()                        = 0;
    virtual void        render()                              = 0;
    virtual void        swap_buffers()                        = 0;
};

CKOM_DECLSPEC Engine* create_engine();
CKOM_DECLSPEC void    destroy_engine(Engine* eng);

} // namespace Ckom
#endif // ENGINE_H
