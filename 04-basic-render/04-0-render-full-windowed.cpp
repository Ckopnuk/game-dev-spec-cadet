#include "04-triangle-interpolated-render.h"

#include <SDL.h>

#include <cmath>
#include <cstdlib>

#include <algorithm>
#include <iostream>

int main(int, char*[])
{
    using namespace std;

    if (0 != SDL_Init(SDL_INIT_EVERYTHING))
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Window* window = SDL_CreateWindow(
        "run time soft render", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        width, height, SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr)
    {
        cerr << SDL_GetError() << endl;
        return EXIT_FAILURE;
    }

    const Color black = { 0, 0, 0 };

    Canvas image;

    TriangleInterpolated interpolatedRender(image, width, height);

    struct Programm : GfxProgramm
    {
        double mouseX{};
        double mouseY{};
        double radius{};

        void setUniforms(const Uniforms& vin) override
        {
            mouseX = vin.f0;
            mouseY = vin.f1;
            radius = vin.f2;
        }

        Vertex vertexShader(const Vertex& vin) override
        {
            Vertex out = vin;

            double x = out.f0;
            double y = out.f1;

            double dx = x - mouseX;
            double dy = y - mouseY;

            if (dx * dx + dy * dy < radius * radius)
            {
                double len = std::sqrt(dx * dx + dy * dy);
                if (len > 0)
                {
                    double normDx = dx / len;
                    double normDy = dy / len;

                    double radiusPosX = mouseX + normDx * radius;
                    double radiusPosY = mouseY + normDy * radius;

                    x = (x + radiusPosX) / 2;
                    y = (y + radiusPosY) / 2;
                }
            }

            assert(!std::isnan(x));
            assert(!std::isnan(y));
            out.f0 = x;
            out.f1 = y;

            return out;
        }
        Color fragmentShader(const Vertex& vin) override
        {
            Color out;
            out.r = static_cast<uint8_t>(vin.f2 * 255);
            out.g = static_cast<uint8_t>(vin.f3 * 255);
            out.b = static_cast<uint8_t>(vin.f4 * 255);

            double x  = vin.f0;
            double y  = vin.f1;
            double dx = mouseX - x;
            double dy = mouseY - y;
            if (dx * dx + dy * dy < radius * radius)
            {
                double len        = std::sqrt(dx * dx + dy * dy);
                double greenToRed = (len / radius) - 0.35;
                greenToRed        = std::clamp(greenToRed, 0.0, 1.0);
                out.r             = (1 - greenToRed) * 255;
                out.g             = greenToRed * 255;
                out.b             = 0;
            }

            return out;
        }
    } programm01;

    std::vector<Vertex> triangleV;

    const size_t cellXcount = 20;
    const size_t cellYcount = 20;
    const double cellWidth  = static_cast<double>(width) / cellXcount;
    const double cellHeight = static_cast<double>(height) / cellYcount;

    for (size_t j = 0; j < cellYcount; ++j)
    {
        for (size_t i = 0; i < cellXcount; ++i)
        {
            double x = i * cellWidth;
            double y = j * cellHeight;
            double r = 0;
            double g = 1;
            double b = 0;
            double u = 0;
            double v = 0;
            triangleV.push_back({ x, y, r, g, b, u, v, 0 });
        }
    }

    std::vector<uint16_t> indexV;

    for (size_t j = 0; j < cellYcount - 1; ++j)
    {
        for (size_t i = 0; i < cellXcount - 1; ++i)
        {
            uint16_t v0 = j * cellXcount + i;
            uint16_t v1 = v0 + 1;
            uint16_t v2 = v0 + cellXcount;
            uint16_t v3 = v2 + 1;

            bool whatOnlyCellBorder = true;
            if (whatOnlyCellBorder)
            {
                indexV.insert(end(indexV), { v0, v1, v0 });
                indexV.insert(end(indexV), { v2, v0, v2 });
                indexV.insert(end(indexV), { v2, v3, v2 });
                indexV.insert(end(indexV), { v1, v3, v1 });
            }
            else
            {
                indexV.insert(end(indexV), { v0, v1, v2 });
                indexV.insert(end(indexV), { v2, v1, v3 });
            }
        }
    }

    void*     pixels = image.data();
    const int depth  = sizeof(Color) * 8;
    const int pitch  = width * sizeof(Color);
    const int rmask  = 0x000000ff;
    const int gmask  = 0x0000ff00;
    const int bmask  = 0x00ff0000;
    const int amask  = 0;

    interpolatedRender.setGfxProgramm(programm01);

    double mouseX{ 1000 };
    double mouseY{ 100 };
    double radius{ 40.0 };

    bool continueLoop = true;

    while (continueLoop)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                continueLoop = false;
                break;
            }
            else if (event.type == SDL_MOUSEMOTION)
            {
                mouseX = event.motion.x;
                mouseY = event.motion.y;
                std::cout << "mouse x" << mouseX << std::endl;
                std::cout << "mouse y" << mouseY << std::endl;
            }
            else if (event.type == SDL_MOUSEWHEEL)
            {
                radius *= event.wheel.y;
            }
        }

        interpolatedRender.clear(black);
        programm01.setUniforms(Uniforms{ mouseX, mouseY, radius });

        interpolatedRender.drawTriangles(triangleV, indexV);

        SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(
            pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);
        if (bitmapSurface == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }

        SDL_Texture* bitmapTex =
            SDL_CreateTextureFromSurface(renderer, bitmapSurface);
        if (bitmapTex == nullptr)
        {
            cerr << SDL_GetError() << endl;
            return EXIT_FAILURE;
        }

        SDL_FreeSurface(bitmapSurface);

        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bitmapTex, nullptr, nullptr);
        SDL_RenderPresent(renderer);

        SDL_DestroyTexture(bitmapTex);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
