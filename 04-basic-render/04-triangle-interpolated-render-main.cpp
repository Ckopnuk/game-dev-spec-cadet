#include "04-triangle-interpolated-render.h"

int
main(int, char*[])
{
  const Color black = { 20, 20, 20 };
  Canvas image;

  TriangleInterpolated interpolatedRender(image, width, height);

  struct Programm : GfxProgramm
  {
    void setUniforms(const Uniforms&) override{};
    Vertex vertexShader(const Vertex& vIn) override
    {
      Vertex out = vIn;

      double alpha = 3.14159 / 6;
      double x = out.f0;
      double y = out.f1;
      out.f0 = x * cos(alpha) - y * sin(alpha);
      out.f1 = x * sin(alpha) + y * cos(alpha);

      out.f0 *= 0.3;
      out.f1 *= 0.3;

      out.f0 += (width / 2);
      out.f1 += (height / 2);

      return out;
    }

    Color fragmentShader(const Vertex& vIn) override
    {
      Color out;
      out.r = static_cast<uint8_t>(vIn.f2 * 255);
      out.g = static_cast<uint8_t>(vIn.f3 * 255);
      out.b = static_cast<uint8_t>(vIn.f4 * 255);

      return out;
    }
  } program01;

  interpolatedRender.clear(black);
  interpolatedRender.setGfxProgramm(program01);

  std::vector<Vertex> triangle_v{ { 0, 0, 1, 0, 0, 0, 0, 0 },
                                  { 0, 239, 0, 1, 0, 0, 0, 0 },
                                  { 319, 0, 0, 0, 1, 0, 0, 0 } };

  std::vector<uint16_t> indexes_v{ 0, 1, 2 };

  interpolatedRender.drawTriangles(triangle_v, indexes_v);

  image.saveImage("05-interpolated.ppm");

  struct ProgrammTex : GfxProgramm
  {
    std::array<Color, bufferSize> texture;
    void setUniforms(const Uniforms&) override {}
    Vertex vertexShader(const Vertex& vIn) override
    {
      Vertex out = vIn;

      //      double alpha = 3.14159 / 6;
      //      double x = out.f0;
      //      double y = out.f1;
      //      out.f0 = x * cos(alpha) - y * sin(alpha);
      //      out.f1 = x * sin(alpha) + y * cos(alpha);

      //      out.f0 *= 0.3;
      //      out.f1 *= 0.3;

      //      out.f0 += (width / 2);
      //      out.f1 += (height / 2);

      return out;
    }

    Color fragmentShader(const Vertex& vIn) override
    {
      Color out;

      out.r = static_cast<uint8_t>(vIn.f2 * 255);
      out.g = static_cast<uint8_t>(vIn.f3 * 255);
      out.b = static_cast<uint8_t>(vIn.f4 * 255);

      Color colorFromTexture = simple2d(vIn.f5, vIn.f6);
      out.r += colorFromTexture.r;
      out.g += colorFromTexture.g;
      out.b += colorFromTexture.b;

      return out;
    }

    void setTexture(const std::array<Color, bufferSize> tex) { texture = tex; }

    Color simple2d(double u, double v)
    {
      uint32_t u_ = static_cast<uint32_t>(std::round(u));
      uint32_t v_ = static_cast<uint32_t>(std::round(v));

      Color color = texture.at(v_ * width + u_);
      return color;
    }
  } program02;

  program02.setTexture(image);
  interpolatedRender.setGfxProgramm(program02);
  interpolatedRender.clear(black);
  interpolatedRender.drawTriangles(triangle_v, indexes_v);

  image.saveImage("06-texture-triangle.ppm");

  return 0;
}
