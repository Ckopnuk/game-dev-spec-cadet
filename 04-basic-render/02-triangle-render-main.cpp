#include "02-triangle-render.h"

int
main(int, char*[])
{
  const Color blue = { 0, 0, 250 };
  const Color black = { 0, 0, 0 };

  Canvas image;

  TriangleRender triRend(image, width, height);
  triRend.clear(black);

  std::vector<Position> triangle;
  triangle.push_back(Position{ 0, 0 });
  triangle.push_back(Position{ width - 1, height - 1 });
  triangle.push_back(Position{ 0, height - 1 });

  triRend.DrawTriangle(triangle, 3, blue);

  image.saveImage("02-triangle.ppm");

  triRend.clear(black);

  size_t max_x = 10;
  size_t max_y = 10;

  std::vector<Position> triangles;

  for (size_t i = 0; i < max_x; ++i) {
    for (size_t j = 0; j < max_y; ++j) {
      int32_t stepX = (width - 1) / max_x;
      int32_t stepY = (height - 1) / max_y;

      Position p0{ 0 + static_cast<int32_t>(i) * stepX,
                   0 + static_cast<int32_t>(j) * stepY };
      Position p1{ p0.x + stepX, p0.y + stepY };
      Position p2{ p0.x, p0.y + stepY };
      Position p3{ p0.x + stepX, p0.y };

      triangles.push_back(p0);
      triangles.push_back(p1);
      triangles.push_back(p2);

      triangles.push_back(p0);
      triangles.push_back(p3);
      triangles.push_back(p2);
    }
  }

  triRend.DrawTriangle(triangles, triangles.size(), blue);

  image.saveImage("02-triangles.ppm");

  return 0;
}
