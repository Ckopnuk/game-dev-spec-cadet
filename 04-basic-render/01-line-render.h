#pragma once

#include "00-convas-basic.h"

struct LineRender : IRender
{
  LineRender(Canvas& buffer, size_t width, size_t height);
  void clear(Color color) override;
  void setPixel(Position pos, Color clr) override;
  pixels pixelPosition(Position start, Position end) override;
  void drawLine(Position start, Position end, Color color);

private:
  Canvas& buf;
  const size_t w;
  const size_t h;
};
