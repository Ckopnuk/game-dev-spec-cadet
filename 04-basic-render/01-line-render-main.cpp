#include "01-line-render.h"

int
main(int, char**)
{
  const Color black = { 0, 0, 0 };
  const Color white = { 250, 250, 250 };
  const Color red = { 250, 0, 0 };

  Canvas image;

  LineRender render(image, width, height);

  render.clear(black);
  render.drawLine(Position{ 0, 0 }, Position{ width, height }, white);

  render.drawLine(Position{ 0, height - 1 }, Position{ height - 1, 0 }, red);

  image.saveImage("01_line");
  return 0;
}
