#pragma once

#include "02-triangle-render.h"

struct triangleIndexRender : TriangleRender
{
  triangleIndexRender(Canvas& buffer, size_t width, size_t height);

  void drawTriangles(std::vector<Position>& vertex,
                     std::vector<uint8_t>& indexes,
                     Color color);
};
