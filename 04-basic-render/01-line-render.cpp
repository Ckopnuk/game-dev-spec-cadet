#include "01-line-render.h"

#include <algorithm>
#include <cassert>

LineRender::LineRender(Canvas& buffer, size_t width, size_t height)
  : buf(buffer)
  , w(width)
  , h(height)
{}

void
LineRender::clear(Color color)
{
  std::fill(begin(buf), end(buf), color);
}

void
LineRender::setPixel(Position pos, Color clr)
{
  const size_t i =
    static_cast<unsigned>(pos.y) * w + static_cast<unsigned>(pos.x);
  if (i >= buf.size()) {
    return;
  }
  Color& col = buf[i];
  col = clr;
}

pixels
LineRender::pixelPosition(Position start, Position end)
{
  pixels result;
  int x0 = start.x;
  int y0 = start.y;
  int x1 = end.x;
  int y1 = end.y;

  bool steep = abs(y1 - y0) > abs(x1 - x0);
  if (steep) {
    std::swap(x0, y0);
    std::swap(x1, y1);
  }
  if (x0 > x1) {
    std::swap(x0, x1);
    std::swap(y0, y1);
  }

  int dx = x1 - x0;
  int dy = abs(y1 - y0);

  int error = dx / 2;
  int ystep = (y0 < y1) ? 1 : -1;
  int y = y0;

  for (int x = x0; x <= x1; x++) {
    result.push_back(Position{ static_cast<int32_t>(steep ? y : x),
                               static_cast<int32_t>(steep ? x : y) });
    error -= dy;
    if (error < 0) {
      y += ystep;
      error += dx;
    }
  }
  return result;
}

void
LineRender::drawLine(Position start, Position end, Color color)
{
  pixels L = pixelPosition(start, end);
  std::for_each(
    begin(L), std::end(L), [&](auto& pos) { setPixel(pos, color); });
}
