#include "04-triangle-interpolated-render.h"

#include <algorithm>
#include <iostream>

double interpolate(const double f0, const double f1, const double t)
{
    assert(t >= 0.0);
    assert(t <= 1.0);
    return f0 + (f1 - f0) * t;
}

Vertex interpolate(const Vertex& v0, const Vertex& v1, const double t)
{
    return { interpolate(v0.f0, v1.f0, t), interpolate(v0.f1, v1.f1, t),
             interpolate(v0.f2, v1.f2, t), interpolate(v0.f3, v1.f3, t),
             interpolate(v0.f4, v1.f4, t), interpolate(v0.f5, v1.f5, t),
             interpolate(v0.f6, v1.f6, t), interpolate(v0.f7, v1.f7, t) };
}

TriangleInterpolated::TriangleInterpolated(Canvas& buffer, size_t width,
                                           size_t height)
    : triangleIndexRender(buffer, width, height)
{
}

void TriangleInterpolated::drawTriangles(std::vector<Vertex>&   vertexes,
                                         std::vector<uint16_t>& indexes)
{
    for (size_t ind = 0; ind < indexes.size(); ind += 3)
    {
        const uint16_t index0 = indexes.at(ind + 0);
        const uint16_t index1 = indexes.at(ind + 1);
        const uint16_t index2 = indexes.at(ind + 2);

        const Vertex& v0 = vertexes.at(index0);
        const Vertex& v1 = vertexes.at(index1);
        const Vertex& v2 = vertexes.at(index2);

        const Vertex v_0 = programm->vertexShader(v0);
        const Vertex v_1 = programm->vertexShader(v1);
        const Vertex v_2 = programm->vertexShader(v2);

        const std::vector<Vertex> interpolated{ restirazeTriangle(v_0, v_1,
                                                                  v_2) };

        for (const Vertex& interpolatedVertex : interpolated)
        {
            const Color    color = programm->fragmentShader(interpolatedVertex);
            const Position pos{
                static_cast<int32_t>(std::round(interpolatedVertex.f0)),
                static_cast<int32_t>(std::round(interpolatedVertex.f1))
            };
            setPixel(pos, color);
        }
    }
}

std::vector<Vertex> TriangleInterpolated::restirazeTriangle(const Vertex& v0,
                                                            const Vertex& v1,
                                                            const Vertex& v2)
{
    std::vector<Vertex> out;

    std::array<const Vertex*, 3> inVertexes{ &v0, &v1, &v2 };
    std::sort(begin(inVertexes), end(inVertexes),
              [](const Vertex* left, const Vertex* right) {
                  return left->f1 < right->f1;
              });

    const Vertex& top    = *inVertexes.at(0);
    const Vertex& middle = *inVertexes.at(1);
    const Vertex& bottom = *inVertexes.at(2);

    Position start{ static_cast<int32_t>(std::round(top.f0)),
                    static_cast<int32_t>(std::round(top.f1)) };
    Position end{ static_cast<int32_t>(std::round(bottom.f0)),
                  static_cast<int32_t>(std::round(bottom.f1)) };
    Position middlePos{ static_cast<int32_t>(std::round(middle.f0)),
                        static_cast<int32_t>(std::round(middle.f1)) };

    if (start == end)
    {
        Position delta       = start - middlePos;
        size_t   countPixels = 4 * (std::abs(delta.x) + std::abs(delta.y) + 1);
        for (size_t i = 0; i < countPixels; ++i)
        {
            double t    = static_cast<double>(i) / countPixels;
            Vertex vert = interpolate(top, middle, t);
            out.push_back(vert);
        }
        return out;
    }

    if (start == middlePos)
    {
        Position delta       = start - end;
        size_t   countPixels = 4 * (std::abs(delta.x) + std::abs(delta.y) + 1);
        for (size_t i = 0; i < countPixels; ++i)
        {
            double t    = static_cast<double>(i) / countPixels;
            Vertex vert = interpolate(top, bottom, t);
            out.push_back(vert);
        }
        return out;
    }

    if (end == middlePos)
    {
        Position delta       = start - middlePos;
        size_t   countPixels = 4 * (std::abs(delta.x) + std::abs(delta.y) + 1);
        for (size_t i = 0; i < countPixels; ++i)
        {
            double t    = static_cast<double>(i) / countPixels;
            Vertex vert = interpolate(top, middle, t);
            out.push_back(vert);
        }
        return out;
    }

    std::vector<Position> longestSideLine = pixelPosition(start, end);
    auto                  itMiddle        = std::find_if(
        begin(longestSideLine), std::end(longestSideLine),
        [&](const Position& pos) {
            return pos.y == static_cast<int32_t>(std::round(middle.f1));
        });
    assert(itMiddle != std::end(longestSideLine));
    Position secondMiddle = *itMiddle;

    double t{ 0 };
    double endStart = (end - start).length();
    if (endStart > 0)
    {
        double middleStart = (secondMiddle - start).length();
        t                  = middleStart / endStart;
    }
    else
    {
        std::vector<Position> line = pixelPosition(start, middlePos);
    }

    Vertex              secondMiddleVertrex = interpolate(top, bottom, t);
    std::vector<Vertex> topTriangle =
        rasterHorizontTriangle(top, middle, secondMiddleVertrex);
    std::vector<Vertex> bottomTriangle =
        rasterHorizontTriangle(bottom, middle, secondMiddleVertrex);

    out.insert(std::end(out), begin(topTriangle), std::end(topTriangle));
    out.insert(std::end(out), begin(bottomTriangle), std::end(bottomTriangle));
    return out;
}

std::vector<Vertex> TriangleInterpolated::rasterHorizontTriangle(
    const Vertex& single, const Vertex& left, const Vertex& right)
{
    std::vector<Vertex> out;

    size_t numOfHorLines =
        static_cast<size_t>(std::round(std::abs(single.f1 - left.f1)));
    if (numOfHorLines > 0)
    {
        for (size_t i = 0; i <= numOfHorLines; ++i)
        {
            double tVertical   = static_cast<double>(i) / numOfHorLines;
            Vertex leftVertex  = interpolate(left, single, tVertical);
            Vertex rightVertex = interpolate(right, single, tVertical);

            rasteOneHorizintalLine(leftVertex, rightVertex, out);
        }
    }
    else
    {
        rasteOneHorizintalLine(left, right, out);
    }
    return out;
}

void TriangleInterpolated::rasteOneHorizintalLine(const Vertex& leftVertex,
                                                  const Vertex& rightVertex,
                                                  std::vector<Vertex>& out)
{
    size_t numOfPixelsInLine = static_cast<size_t>(
        std::round(std::abs(leftVertex.f0 - rightVertex.f0)));
    if (numOfPixelsInLine > 0)
    {
        for (size_t i = 0; i < numOfPixelsInLine + 1; ++i)
        {
            double tPixel = static_cast<double>(i) / (numOfPixelsInLine + 1);
            Vertex pixel  = interpolate(leftVertex, rightVertex, tPixel);

            out.push_back(pixel);
        }
    }
    else
    {
        out.push_back(leftVertex);
    }
}
