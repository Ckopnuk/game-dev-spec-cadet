#include "04-triangle-interpolated-render.h"

#include <SDL.h>

//#include <cstdlib>
#include <iostream>

int main(int, char*[])
{

    if (0 != SDL_Init(SDL_INIT_EVERYTHING))
    {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Window* window = SDL_CreateWindow(
        "Runtime soft", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width,
        height, SDL_WINDOW_OPENGL);
    if (window == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Renderer* renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    if (renderer == nullptr)
    {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    const Color black = { 0, 0, 0 };

    Canvas image;

    TriangleInterpolated interpolatedRender(image, width, height);

    struct Programm : GfxProgramm
    {
        double mouseX{};
        double mouseY{};
        double radius{};

        void setUniforms(const Uniforms& unif) override
        {
            mouseX = unif.f0;
            mouseY = unif.f1;
            radius = unif.f2;
        }

        Vertex vertexShader(const Vertex& vIn) override
        {
            Vertex out = vIn;

            double x = out.f0;
            double y = out.f1;

            out.f0 = x;
            out.f1 = y;

            return out;
        }

        Color fragmentShader(const Vertex& vIn) override
        {
            Color out;
            out.r = static_cast<uint8_t>(vIn.f2 * 255);
            out.g = static_cast<uint8_t>(vIn.f3 * 255);
            out.b = static_cast<uint8_t>(vIn.f4 * 255);

            double x = vIn.f0;
            double y = vIn.f1;

            double dx = mouseX - x;
            double dy = mouseY - y;

            if (dx * dx + dy * dy < radius * radius)
            {
                double gray = 0.21 * out.r + 0.72 * out.g + 0.07 * out.b;
                out.r       = gray;
                out.g       = gray;
                out.b       = gray;
            }
            return out;
        }
    } programm01;

    std::vector<Vertex> triangleV = { { 0, 0, 1, 0, 0, 0, 0, 0 },
                                      { 0, 239, 0, 1, 0, 0, 239, 0 },
                                      { 319, 239, 0, 0, 1, 319, 239, 0 } };

    std::vector<uint16_t> indexV = { 0, 1, 2 };

    void*     pixels = image.data();
    const int depth  = sizeof(Color) * 8;
    const int pitch  = width * sizeof(Color);
    const int rmask  = 0x000000ff;
    const int gmask  = 0x0000ff00;
    const int bmask  = 0x00ff0000;
    const int amask  = 0;

    interpolatedRender.setGfxProgramm(programm01);

    double mouseX{};
    double mouseY{};
    double radius{ 20.0 };

    bool continueLoop = true;

    while (continueLoop)
    {
        SDL_Event sdlEvent;
        while (SDL_PollEvent(&sdlEvent))
        {
            if (sdlEvent.type == SDL_QUIT)
            {
                continueLoop = false;
                break;
            }
            else if (sdlEvent.type == SDL_MOUSEMOTION)
            {
                mouseX = sdlEvent.motion.x;
                mouseY = sdlEvent.motion.y;
            }
            else if (sdlEvent.type == SDL_MOUSEWHEEL)
            {
                radius *= sdlEvent.wheel.y;
            }
        }

        interpolatedRender.clear(black);
        programm01.setUniforms(Uniforms{ mouseX, mouseY, radius });
        interpolatedRender.drawTriangles(triangleV, indexV);

        SDL_Surface* bitmapSurface = SDL_CreateRGBSurfaceFrom(
            pixels, width, height, depth, pitch, rmask, gmask, bmask, amask);

        if (bitmapSurface == nullptr)
        {
            std::cerr << SDL_GetError() << std::endl;
            return EXIT_FAILURE;
        }

        SDL_Texture* bitmapText =
            SDL_CreateTextureFromSurface(renderer, bitmapSurface);

        if (bitmapText == nullptr)
        {
            std::cerr << SDL_GetError() << std::endl;
        }

        SDL_FreeSurface(bitmapSurface);
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bitmapText, nullptr, nullptr);
        SDL_RenderPresent(renderer);

        SDL_DestroyTexture(bitmapText);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();

    return EXIT_SUCCESS;
}
