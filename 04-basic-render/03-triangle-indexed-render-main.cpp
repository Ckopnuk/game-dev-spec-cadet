#include "03-triangle-indexed-render.h"

int
main(int, char*[])
{
  Color black = { 0, 0, 0 };
  Color green = { 3, 255, 0 };

  Canvas image;
  std::vector<Position> vertexBuffer;

  size_t maxX = 10;
  size_t maxY = 10;

  int32_t stepX = (width - 1) / maxX;
  int32_t stepY = (height - 1) / maxY;

  for (size_t i = 0; i <= maxY; ++i) {
    for (size_t j = 0; j <= maxX; ++j) {
      Position p{ static_cast<int32_t>(j) * stepX,
                  static_cast<int32_t>(i) * stepY };
      vertexBuffer.push_back(p);
    }
  }

  assert(vertexBuffer.size() == (maxX + 1) * (maxY + 1));

  std::vector<uint8_t> indexBuffer;

  for (size_t i = 0; i < maxX; ++i) {
    for (size_t j = 0; j < maxY; ++j) {
      uint8_t index0 = static_cast<uint8_t>(j * (maxY + 1) + i);
      uint8_t index1 = static_cast<uint8_t>(index0 + (maxX + 1) + 1);
      uint8_t index2 = index1 - 1;
      uint8_t index3 = index0 + 1;

      indexBuffer.push_back(index0);
      indexBuffer.push_back(index1);
      indexBuffer.push_back(index2);

      indexBuffer.push_back(index0);
      indexBuffer.push_back(index3);
      indexBuffer.push_back(index1);
    }
  }

  triangleIndexRender indexRender(image, width, height);
  indexRender.clear(black);

  indexRender.drawTriangles(vertexBuffer, indexBuffer, green);

  image.saveImage("04-triangle-indx.ppm");

  Canvas texImage;

  texImage.loadImage("04-triangle-indx.ppm");

  assert(image == texImage);

  return 0;
}
