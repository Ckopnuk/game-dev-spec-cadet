#pragma once

#include "01-line-render.h"

struct TriangleRender : LineRender
{
  TriangleRender(Canvas& buffer, size_t width, size_t height);

  virtual pixels PixelsPositionOnTriangle(Position p0,
                                          Position p1,
                                          Position p2);
  void DrawTriangle(std::vector<Position>& vertexes,
                    size_t numVert,
                    Color color);
};
