#pragma once

#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iomanip>
#include <vector>

constexpr size_t width = 360;
constexpr size_t height = 240;

#pragma pack(push, 1)
struct Color
{
  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;
  friend bool operator==(const Color& left, const Color& right);
};
#pragma pack(pop)

const size_t bufferSize = width * height;

class Canvas : public std::array<Color, bufferSize>
{
public:
  void saveImage(const std::string& fileName)
  {
    std::ofstream outFile;
    outFile.exceptions(std::ios_base::failbit);
    outFile.open(fileName, std::ios_base::binary);
    outFile << "P6\n" << width << ' ' << height << ' ' << 255 << '\n';
    std::streamsize bufSize =
      static_cast<std::streamsize>(sizeof(Color) * size());
    outFile.write(reinterpret_cast<const char*>(this), bufSize);
  }

  void loadImage(const std::string& fileName)
  {
    std::ifstream inFile;
    inFile.exceptions(std::ios_base::failbit);
    inFile.open(fileName, std::ios_base::binary);
    std::string header;
    size_t imageWidth = 0;
    size_t imageHeight = 0;
    std::string colorFormat;
    inFile >> header >> imageWidth >> imageHeight >> colorFormat >> std::ws;
    if (size() != imageHeight * imageWidth) {
      throw std::runtime_error("image size not much");
    }
    std::streamsize bufSize =
      static_cast<std::streamsize>(sizeof(Color) * size());
    inFile.read(reinterpret_cast<char*>(this), bufSize);
  }
};

struct Position
{
  double length() { return std::sqrt(x * x + y * y); }
  friend Position operator-(const Position left, const Position rigth);
  friend bool operator==(const Position left, const Position right);
  int32_t x = 0;
  int32_t y = 0;
};

using pixels = std::vector<Position>;

struct IRender
{
  virtual ~IRender();
  virtual void clear(Color) = 0;
  virtual void setPixel(Position, Color) = 0;
  virtual pixels pixelPosition(Position start, Position end) = 0;
};
