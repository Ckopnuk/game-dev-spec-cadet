#pragma once

#include "03-triangle-indexed-render.h"

struct Vertex
{
  double f0 = 0; // x
  double f1 = 0; // y
  double f2 = 0; // r
  double f3 = 0; // g
  double f4 = 0; // b
  double f5 = 0; // u (texture coordinate)
  double f6 = 0; // v (texture coordinate)
  double f7 = 0; //?
};

double
interpolate(const double f0, const double f1, const double t);

Vertex
interpolate(const Vertex& v0, const Vertex& v1, const double t);

struct Uniforms
{
  double f0 = 0;
  double f1 = 0;
  double f2 = 0;
  double f3 = 0;
  double f4 = 0;
  double f5 = 0;
  double f6 = 0;
  double f7 = 0;
};

struct GfxProgramm
{
  virtual ~GfxProgramm() = default;
  virtual void setUniforms(const Uniforms&) = 0;
  virtual Vertex vertexShader(const Vertex& vIn) = 0;
  virtual Color fragmentShader(const Vertex& vIn) = 0;
};

struct TriangleInterpolated : triangleIndexRender
{
  TriangleInterpolated(Canvas& buffer, size_t width, size_t height);
  void setGfxProgramm(GfxProgramm& prgm) { programm = &prgm; }
  void drawTriangles(std::vector<Vertex>& vertexes,
                     std::vector<uint16_t>& indexes);

private:
  std::vector<Vertex> restirazeTriangle(const Vertex& v0,
                                        const Vertex& v1,
                                        const Vertex& v2);
  std::vector<Vertex> rasterHorizontTriangle(const Vertex& single,
                                             const Vertex& left,
                                             const Vertex& right);
  void rasteOneHorizintalLine(const Vertex& leftVertex,
                              const Vertex& rightVertex,
                              std::vector<Vertex>& out);
  GfxProgramm* programm = nullptr;
};
