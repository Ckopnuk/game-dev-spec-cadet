#include "00-convas-basic.h"

#include <iostream>

int
main(int, char**)
{
  const Color red = { 255, 0, 0 };
  Canvas image;

  std::fill(std::begin(image), std::end(image), red);

  const char* fileName = "00-red-image.ppm";

  image.saveImage(fileName);

  Canvas image_loaded;
  image_loaded.loadImage(fileName);

  if (image != image_loaded) {
    std::cerr << "image != image_loaded\n";
    return 1;
  } else {
    std::cout << "image == image_loaded\n";
  }
  return 0;
}
