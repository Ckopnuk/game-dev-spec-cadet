#include "03-triangle-indexed-render.h"

triangleIndexRender::triangleIndexRender(Canvas& buffer,
                                         size_t width,
                                         size_t height)
  : TriangleRender(buffer, width, height)
{}

void
triangleIndexRender::drawTriangles(std::vector<Position>& vertex,
                                   std::vector<uint8_t>& indexes,
                                   Color color)
{
  pixels trianglePixelsEdge;

  for (size_t i = 0; i < indexes.size() / 3; ++i) {
    uint8_t index0 = indexes[i * 3 + 0];
    uint8_t index1 = indexes[i * 3 + 1];
    uint8_t index2 = indexes[i * 3 + 2];

    Position p0 = vertex.at(index0);
    Position p1 = vertex.at(index1);
    Position p2 = vertex.at(index2);

    for (auto pixelpos : PixelsPositionOnTriangle(p0, p1, p2)) {
      trianglePixelsEdge.push_back(pixelpos);
    }
  }
  for (auto pos : trianglePixelsEdge) {
    setPixel(pos, color);
  }
}
