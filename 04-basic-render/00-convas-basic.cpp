#include "00-convas-basic.h"

bool
operator==(const Color& left, const Color& right)
{
  return left.r == right.r && left.g == right.g && left.b == right.b;
}

constexpr size_t colorSize = sizeof(Color);

static_assert(3 == colorSize, "24 bit per pixel(r, g, b)");

Position
operator-(const Position left, const Position rigth)
{
  return { left.x - rigth.x, left.y - rigth.y };
}

bool
operator==(const Position left, const Position right)
{
  return left.x == right.x && left.y == right.y;
}

IRender::~IRender() {}
