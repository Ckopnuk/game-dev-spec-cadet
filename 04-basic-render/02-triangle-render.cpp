#include "02-triangle-render.h"

TriangleRender::TriangleRender(Canvas& buffer, size_t width, size_t height)
  : LineRender(buffer, width, height)
{}

pixels
TriangleRender::PixelsPositionOnTriangle(Position p0, Position p1, Position p2)
{
  using namespace std;
  pixels pixelsPos;

  for (auto [start, end] : { pair{ p0, p1 }, pair{ p1, p2 }, pair{ p2, p0 } }) {
    for (auto pos : LineRender::pixelPosition(start, end)) {
      pixelsPos.push_back(pos);
    }
  }
  return pixelsPos;
}

void
TriangleRender::DrawTriangle(std::vector<Position>& vertexes,
                             size_t numVert,
                             Color color)
{
  pixels trianglePixelsEdge;
  for (size_t i = 0; i < numVert / 3; ++i) {
    Position p0 = vertexes.at(i * 3 + 0);
    Position p1 = vertexes.at(i * 3 + 1);
    Position p2 = vertexes.at(i * 3 + 2);

    for (auto pixelPos : PixelsPositionOnTriangle(p0, p1, p2)) {
      trianglePixelsEdge.push_back(pixelPos);
    }
  }

  for (auto pos : trianglePixelsEdge) {
    setPixel(pos, color);
  }
}
